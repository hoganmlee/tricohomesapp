<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <false/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>loading_sprite.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>1</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>loading_sprite.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>loading_sprite.xml</filename>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>loading/tricoloader0001.png</filename>
            <filename>loading/tricoloader0002.png</filename>
            <filename>loading/tricoloader0003.png</filename>
            <filename>loading/tricoloader0004.png</filename>
            <filename>loading/tricoloader0005.png</filename>
            <filename>loading/tricoloader0006.png</filename>
            <filename>loading/tricoloader0007.png</filename>
            <filename>loading/tricoloader0008.png</filename>
            <filename>loading/tricoloader0009.png</filename>
            <filename>loading/tricoloader0010.png</filename>
            <filename>loading/tricoloader0011.png</filename>
            <filename>loading/tricoloader0012.png</filename>
            <filename>loading/tricoloader0013.png</filename>
            <filename>loading/tricoloader0014.png</filename>
            <filename>loading/tricoloader0015.png</filename>
            <filename>loading/tricoloader0016.png</filename>
            <filename>loading/tricoloader0017.png</filename>
            <filename>loading/tricoloader0018.png</filename>
            <filename>loading/tricoloader0019.png</filename>
            <filename>loading/tricoloader0020.png</filename>
            <filename>loading/tricoloader0021.png</filename>
            <filename>loading/tricoloader0022.png</filename>
            <filename>loading/tricoloader0023.png</filename>
            <filename>loading/tricoloader0024.png</filename>
            <filename>loading/tricoloader0025.png</filename>
            <filename>loading/tricoloader0026.png</filename>
            <filename>loading/tricoloader0027.png</filename>
            <filename>loading/tricoloader0028.png</filename>
            <filename>loading/tricoloader0029.png</filename>
            <filename>loading/tricoloader0030.png</filename>
            <filename>loading/tricoloader0031.png</filename>
            <filename>loading/tricoloader0032.png</filename>
            <filename>loading/tricoloader0033.png</filename>
            <filename>loading/tricoloader0034.png</filename>
            <filename>loading/tricoloader0035.png</filename>
            <filename>loading/tricoloader0036.png</filename>
            <filename>loading/tricoloader0037.png</filename>
            <filename>loading/tricoloader0038.png</filename>
            <filename>loading/tricoloader0039.png</filename>
            <filename>loading/tricoloader0040.png</filename>
            <filename>loading/tricoloader0041.png</filename>
            <filename>loading/tricoloader0042.png</filename>
            <filename>loading/tricoloader0043.png</filename>
            <filename>loading/tricoloader0044.png</filename>
            <filename>loading/tricoloader0045.png</filename>
            <filename>loading/tricoloader0046.png</filename>
            <filename>loading/tricoloader0047.png</filename>
            <filename>loading/tricoloader0048.png</filename>
            <filename>loading/tricoloader0049.png</filename>
            <filename>loading/tricoloader0050.png</filename>
            <filename>loading/tricoloader0051.png</filename>
            <filename>loading/tricoloader0052.png</filename>
            <filename>loading/tricoloader0053.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
