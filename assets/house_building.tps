<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <false/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>house_building.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>house_building.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>house_building.xml</filename>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>house_build/trico_loader_000.png</filename>
            <filename>house_build/trico_loader_001.png</filename>
            <filename>house_build/trico_loader_002.png</filename>
            <filename>house_build/trico_loader_003.png</filename>
            <filename>house_build/trico_loader_004.png</filename>
            <filename>house_build/trico_loader_005.png</filename>
            <filename>house_build/trico_loader_006.png</filename>
            <filename>house_build/trico_loader_007.png</filename>
            <filename>house_build/trico_loader_008.png</filename>
            <filename>house_build/trico_loader_009.png</filename>
            <filename>house_build/trico_loader_010.png</filename>
            <filename>house_build/trico_loader_011.png</filename>
            <filename>house_build/trico_loader_012.png</filename>
            <filename>house_build/trico_loader_013.png</filename>
            <filename>house_build/trico_loader_014.png</filename>
            <filename>house_build/trico_loader_015.png</filename>
            <filename>house_build/trico_loader_016.png</filename>
            <filename>house_build/trico_loader_017.png</filename>
            <filename>house_build/trico_loader_018.png</filename>
            <filename>house_build/trico_loader_019.png</filename>
            <filename>house_build/trico_loader_020.png</filename>
            <filename>house_build/trico_loader_021.png</filename>
            <filename>house_build/trico_loader_022.png</filename>
            <filename>house_build/trico_loader_023.png</filename>
            <filename>house_build/trico_loader_024.png</filename>
            <filename>house_build/trico_loader_025.png</filename>
            <filename>house_build/trico_loader_026.png</filename>
            <filename>house_build/trico_loader_027.png</filename>
            <filename>house_build/trico_loader_028.png</filename>
            <filename>house_build/trico_loader_029.png</filename>
            <filename>house_build/trico_loader_030.png</filename>
            <filename>house_build/trico_loader_031.png</filename>
            <filename>house_build/trico_loader_032.png</filename>
            <filename>house_build/trico_loader_033.png</filename>
            <filename>house_build/trico_loader_034.png</filename>
            <filename>house_build/trico_loader_035.png</filename>
            <filename>house_build/trico_loader_036.png</filename>
            <filename>house_build/trico_loader_037.png</filename>
            <filename>house_build/trico_loader_038.png</filename>
            <filename>house_build/trico_loader_039.png</filename>
            <filename>house_build/trico_loader_040.png</filename>
            <filename>house_build/trico_loader_041.png</filename>
            <filename>house_build/trico_loader_042.png</filename>
            <filename>house_build/trico_loader_043.png</filename>
            <filename>house_build/trico_loader_044.png</filename>
            <filename>house_build/trico_loader_045.png</filename>
            <filename>house_build/trico_loader_046.png</filename>
            <filename>house_build/trico_loader_047.png</filename>
            <filename>house_build/trico_loader_048.png</filename>
            <filename>house_build/trico_loader_049.png</filename>
            <filename>house_build/trico_loader_050.png</filename>
            <filename>house_build/trico_loader_051.png</filename>
            <filename>house_build/trico_loader_052.png</filename>
            <filename>house_build/trico_loader_053.png</filename>
            <filename>house_build/trico_loader_054.png</filename>
            <filename>house_build/trico_loader_055.png</filename>
            <filename>house_build/trico_loader_056.png</filename>
            <filename>house_build/trico_loader_057.png</filename>
            <filename>house_build/trico_loader_058.png</filename>
            <filename>house_build/trico_loader_059.png</filename>
            <filename>house_build/trico_loader_060.png</filename>
            <filename>house_build/trico_loader_061.png</filename>
            <filename>house_build/trico_loader_062.png</filename>
            <filename>house_build/trico_loader_063.png</filename>
            <filename>house_build/trico_loader_064.png</filename>
            <filename>house_build/trico_loader_065.png</filename>
            <filename>house_build/trico_loader_066.png</filename>
            <filename>house_build/trico_loader_067.png</filename>
            <filename>house_build/trico_loader_068.png</filename>
            <filename>house_build/trico_loader_069.png</filename>
            <filename>house_build/trico_loader_070.png</filename>
            <filename>house_build/trico_loader_071.png</filename>
            <filename>house_build/trico_loader_072.png</filename>
            <filename>house_build/trico_loader_073.png</filename>
            <filename>house_build/trico_loader_074.png</filename>
            <filename>house_build/trico_loader_075.png</filename>
            <filename>house_build/trico_loader_076.png</filename>
            <filename>house_build/trico_loader_077.png</filename>
            <filename>house_build/trico_loader_078.png</filename>
            <filename>house_build/trico_loader_079.png</filename>
            <filename>house_build/trico_loader_080.png</filename>
            <filename>house_build/trico_loader_081.png</filename>
            <filename>house_build/trico_loader_082.png</filename>
            <filename>house_build/trico_loader_083.png</filename>
            <filename>house_build/trico_loader_084.png</filename>
            <filename>house_build/trico_loader_085.png</filename>
            <filename>house_build/trico_loader_086.png</filename>
            <filename>house_build/trico_loader_087.png</filename>
            <filename>house_build/trico_loader_088.png</filename>
            <filename>house_build/trico_loader_089.png</filename>
            <filename>house_build/trico_loader_090.png</filename>
            <filename>house_build/trico_loader_091.png</filename>
            <filename>house_build/trico_loader_092.png</filename>
            <filename>house_build/trico_loader_093.png</filename>
            <filename>house_build/trico_loader_094.png</filename>
            <filename>house_build/trico_loader_095.png</filename>
            <filename>house_build/trico_loader_096.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
