<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <false/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>trico_spritesheet.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>trico_spritesheet.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>trico_spritesheet.xml</filename>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>images/checkbox_down.png</filename>
            <filename>images/checkbox_up.png</filename>
            <filename>images/trico_logo3.png</filename>
            <filename>images/trico_logo2.png</filename>
            <filename>images/icon_pdf.png</filename>
            <filename>images/trico_logo1.png</filename>
            <filename>images/button_round15_dark.png</filename>
            <filename>images/button_round6_red.png</filename>
            <filename>images/button_round6_dark.png</filename>
            <filename>images/button_round6_light.png</filename>
            <filename>images/button_round18_red.png</filename>
            <filename>images/button_round18_dark.png</filename>
            <filename>images/button_round18_light.png</filename>
            <filename>images/button_counter.png</filename>
            <filename>images/icon_carrot20_dark.png</filename>
            <filename>images/icon_carrot20_light.png</filename>
            <filename>images/icon_carrot20_white.png</filename>
            <filename>images/icon_carrot14_light.png</filename>
            <filename>images/icon_carrot14_dark.png</filename>
            <filename>images/input_login.png</filename>
            <filename>images/button_round6_gradient.png</filename>
            <filename>images/icon_carrot14_white.png</filename>
            <filename>images/scrollBar.png</filename>
            <filename>images/button_round15_light.png</filename>
            <filename>images/button_round6_white.png</filename>
            <filename>images/icon_album.png</filename>
            <filename>images/icon_checklist.png</filename>
            <filename>images/icon_offers.png</filename>
            <filename>images/icon_pdf_dark.png</filename>
            <filename>images/sideNavDropShadow.png</filename>
            <filename>images/button_underline.png</filename>
            <filename>images/icon_logout.png</filename>
            <filename>images/homeDropShadow.png</filename>
            <filename>images/bottomDropShadow.png</filename>
            <filename>images/fullScreen.png</filename>
            <filename>images/button_round6_downstate.png</filename>
            <filename>images/button_close.png</filename>
            <filename>images/invalid_password.png</filename>
            <filename>images/invalid_user.png</filename>
            <filename>images/serviceCheckDown.png</filename>
            <filename>images/serviceCheckUp.png</filename>
            <filename>images/button_round10_white.png</filename>
            <filename>images/wifi_signal.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
