 package com.sajakfarki.trico
{	
	 
//	import com.sajakfarki.openpdf.OpenPDFExtension;
//	import com.sajakfarki.trico.service.PDFService;
	 
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.IAlbumService;
	import com.sajakfarki.trico.service.IChecklistService;
	import com.sajakfarki.trico.service.IEmailService;
	import com.sajakfarki.trico.service.ILoginService;
	import com.sajakfarki.trico.service.IWishlistService;
	import com.sajakfarki.trico.service.soap.SOAPAdService;
	import com.sajakfarki.trico.service.soap.SOAPAlbumService;
	import com.sajakfarki.trico.service.soap.SOAPChecklistService;
	import com.sajakfarki.trico.service.soap.SOAPEmailService;
	import com.sajakfarki.trico.service.soap.SOAPLoginService;
	import com.sajakfarki.trico.service.soap.SOAPWishlistService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.signal.AdFullPageSignal;
	import com.sajakfarki.trico.signal.AdInfoSignal;
	import com.sajakfarki.trico.signal.EmailSentSignal;
	import com.sajakfarki.trico.signal.InvalidNameSignal;
	import com.sajakfarki.trico.signal.InvalidPasswordSignal;
	import com.sajakfarki.trico.signal.LoggedInSignal;
	import com.sajakfarki.trico.signal.LogoutSignal;
	import com.sajakfarki.trico.signal.UpdateAlbumSignal;
	import com.sajakfarki.trico.signal.UpdateChecklistSignal;
	import com.sajakfarki.trico.signal.UpdateWishlistSignal;
	import com.sajakfarki.trico.view.components.AdFullPageView;
	import com.sajakfarki.trico.view.components.AlbumView;
	import com.sajakfarki.trico.view.components.ChecklistView;
	import com.sajakfarki.trico.view.components.CommunityView;
	import com.sajakfarki.trico.view.components.HomeView;
	import com.sajakfarki.trico.view.components.OffersView;
	import com.sajakfarki.trico.view.components.ReferralView;
	import com.sajakfarki.trico.view.components.WelcomeView;
	import com.sajakfarki.trico.view.components.display.EquityForm;
	import com.sajakfarki.trico.view.components.display.ReferralForm;
	import com.sajakfarki.trico.view.components.pdfs.BuildingExperienceView;
	import com.sajakfarki.trico.view.components.pdfs.CustomerPortalView;
	import com.sajakfarki.trico.view.components.pdfs.InteriorSelectionView;
	import com.sajakfarki.trico.view.components.pdfs.ServiceView;
	import com.sajakfarki.trico.view.components.promotions.EquitySaverView;
	import com.sajakfarki.trico.view.components.promotions.TricoCentreView;
	import com.sajakfarki.trico.view.mediators.AlbumViewMediator;
	import com.sajakfarki.trico.view.mediators.ChecklistViewMediator;
	import com.sajakfarki.trico.view.mediators.CommunityViewMediator;
	import com.sajakfarki.trico.view.mediators.HomeViewMediator;
	import com.sajakfarki.trico.view.mediators.OffersViewMediator;
	import com.sajakfarki.trico.view.mediators.ReferralViewMediator;
	import com.sajakfarki.trico.view.mediators.ServiceViewMediator;
	import com.sajakfarki.trico.view.mediators.WelcomeViewMediator;
	import com.sajakfarki.trico.view.mediators.display.AdFullPageMediator;
	import com.sajakfarki.trico.view.mediators.pdfs.BuildingExperienceMediator;
	import com.sajakfarki.trico.view.mediators.pdfs.CustomerPortalMediator;
	import com.sajakfarki.trico.view.mediators.pdfs.InteriorSelectionMediator;
	import com.sajakfarki.trico.view.mediators.promotions.EquitySaverMediator;
	import com.sajakfarki.trico.view.mediators.promotions.TricoCentreMediator;
	
	import flash.filesystem.File;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.StarlingContext;
	
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class TricoContext extends StarlingContext
	{
		public static const TRICO_FOLDER:String = "trico";
		
		public static const CUSTOMER_PORTAL:String = "TC_10733_Cust_Portal.pdf";
		public static const INTERIOR_SELECTION:String = "TC-11720_Quick_Ref_MASTER.pdf";
		public static const BUILDING_EXPERIENCE:String = "TC-11323_Experience_MASTER.pdf";
		
		public static const CARING_HOME:String = "TC-11717_Caring_For_your_home_v2.pdf";
		public static const MOVING_CHECKLIST:String = "Combine_Moving_Checklist.pdf";
		public static const MOVING_TIMELINE:String = "TC-11717_Moving_What_to_Do_v2.pdf";
		
		
		private var _theme:TricoTheme;
		
		
		[Embed(source="/../assets/loading_app.png")]
		private static const LOADING_IMAGE:Class;
		
		
		private var _loading:Image;
		private var _navigator:TricoNavigator;
		
				
		public function TricoContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
		}
		
		override public function startup():void
		{
			//LOADING SPRITE
			_loading = new Image(Texture.fromBitmap(new LOADING_IMAGE()));
			_loading.x = contextView.stage.stageWidth/2 - _loading.width/2;
			_loading.y = contextView.stage.stageHeight/2 - _loading.height/2;
			contextView.addChild(_loading);
			
//			TricoNavigator.WIDTH = contextView.stage.stageWidth;
//			TricoNavigator.HEIGHT = contextView.stage.stageHeight;
			TricoNavigator.WIDTH = TricoHomesApp.WIDTH;
			TricoNavigator.HEIGHT = TricoHomesApp.HEIGHT;
			
			
			/***** DEBUG APP VARS *******************************/
			EquityForm.EMAIL_LIST = "hogan@sajakfarki.com	";
//			EquityForm.EMAIL_LIST = "equitysaver@tricohomes.com";
			
			ReferralForm.EMAIL_LIST = "hogan@sajakfarki.com";
//			ReferralForm.EMAIL_LIST = "referrals@tricohomes.com";
			
			
//			HomeView.BUCKET_CHECKLIST = "bucket_something.png";
//			HomeView.BUCKET_PARTNERS
//			HomeView.BUCKET_ALBUM
			
						
//			TricoNavigator.DURATION_FULLPAGE = 5000;
			TricoNavigator.DURATION_FULLPAGE = 100;
			
			/***** DEBUG APP VARS *******************************/
			
			
			//MODELS
			injector.mapSingleton(AppModel);
			
			
			//SERVICES
			injector.mapSingletonOf( ILoginService, SOAPLoginService );
			injector.mapSingletonOf( IAdService, SOAPAdService );
			injector.mapSingletonOf( IAlbumService, SOAPAlbumService );
			injector.mapSingletonOf( IChecklistService, SOAPChecklistService );
			injector.mapSingletonOf( IWishlistService, SOAPWishlistService );
			injector.mapSingletonOf( IEmailService, SOAPEmailService );
			
			
			//SIGNALS
			injector.mapSingleton(LoggedInSignal);
			injector.mapSingleton(LogoutSignal);
			injector.mapSingleton(InvalidPasswordSignal);
			injector.mapSingleton(InvalidNameSignal);

			injector.mapSingleton(UpdateAlbumSignal);
			injector.mapSingleton(UpdateChecklistSignal);
			injector.mapSingleton(UpdateWishlistSignal);
			
			injector.mapSingleton(EmailSentSignal);
			
			injector.mapSingleton(AdDisplaySignal);
			injector.mapSingleton(AdFullPageSignal);
			injector.mapSingleton(AdInfoSignal);
			
			
			//VIEWS
			mediatorMap.mapView(WelcomeView, WelcomeViewMediator);
			mediatorMap.mapView(HomeView, HomeViewMediator);
			mediatorMap.mapView(AlbumView, AlbumViewMediator);
			mediatorMap.mapView(ChecklistView, ChecklistViewMediator);
			mediatorMap.mapView(OffersView, OffersViewMediator);
			mediatorMap.mapView(CommunityView, CommunityViewMediator);
			
			//FORMS
			mediatorMap.mapView(ServiceView, ServiceViewMediator);
			mediatorMap.mapView(ReferralView, ReferralViewMediator);
			mediatorMap.mapView(EquitySaverView, EquitySaverMediator);
			
			//PDFS
			mediatorMap.mapView(BuildingExperienceView, BuildingExperienceMediator);
			mediatorMap.mapView(CustomerPortalView, CustomerPortalMediator);
			mediatorMap.mapView(InteriorSelectionView, InteriorSelectionMediator);
			
			//PROMOTIONS
//			mediatorMap.mapView(GlobalFestView, GlobalFestMediator);
			mediatorMap.mapView(TricoCentreView, TricoCentreMediator);
			mediatorMap.mapView(AdFullPageView, AdFullPageMediator);
			
			
			this.addEventListener(ContextEvent.STARTUP_COMPLETE, loadTheme);
			
			super.startup();
			
		}
		
		/********************************************************
		 * 
		 *   Load Theme
		 * 
		 ********************************************************/
		private function loadTheme(event:ContextEvent):void
		{
			_theme = new TricoTheme(contextView.stage);
			checkLogin();
		}
		
		/********************************************************
		 * 
		 *   Check Login
		 * 
		 ********************************************************/
		private function checkLogin():void
		{
			
			contextView.removeChild(_loading, true);
			
			_navigator = new TricoNavigator(contextView);
			injector.injectInto(_navigator);
			_navigator.startApp();
			
			trace("File.applicationDirectory.url = " , File.applicationDirectory.url);
			trace("File.applicationDirectory.name = " + File.applicationDirectory.name);
//			trace("File.applicationDirectory.getDirectoryListing() = " + File.applicationDirectory.getDirectoryListing());
//			trace(File.applicationDirectory.resolvePath("TC_10733_Cust_Portal.pdf").openWithDefaultApplication());
//			trace(File.applicationDirectory.resolvePath("TC_10733_Cust_Portal.pdf").url);
//			trace(File.documentsDirectory.nativePath);			
			
			if(!TricoHomesApp.DEBUG){
				
				var customerPortal:File = File.applicationDirectory.resolvePath(CUSTOMER_PORTAL);
				var buildingExperience:File = File.applicationDirectory.resolvePath(BUILDING_EXPERIENCE);
				var interiorSelection:File = File.applicationDirectory.resolvePath(INTERIOR_SELECTION);
				var caringHome:File =  File.applicationDirectory.resolvePath(CARING_HOME);
				var movingChecklist:File = File.applicationDirectory.resolvePath(MOVING_CHECKLIST);
				var movingTimeline:File = File.applicationDirectory.resolvePath(MOVING_TIMELINE);
				
				var tricoDirectory:File = File.documentsDirectory.resolvePath(TRICO_FOLDER); 
				
				trace("tricoDirectory.exists = " + tricoDirectory.exists);
				if(!tricoDirectory.exists){
					tricoDirectory.createDirectory();
				}
				
//				customerPortal.copyTo(File.documentsDirectory.resolvePath(TRICO_FOLDER+"/"+CUSTOMER_PORTAL), false);
				trace("tricoDirectory.resolvePath(CUSTOMER_PORTAL).exists = " + tricoDirectory.resolvePath(CUSTOMER_PORTAL).exists);
				
				if(!tricoDirectory.resolvePath(CUSTOMER_PORTAL).exists){
					customerPortal.copyTo(tricoDirectory.resolvePath(CUSTOMER_PORTAL));
				}
				if(!tricoDirectory.resolvePath(BUILDING_EXPERIENCE).exists){
					buildingExperience.copyTo(tricoDirectory.resolvePath(BUILDING_EXPERIENCE));
				}
				if(!tricoDirectory.resolvePath(INTERIOR_SELECTION).exists){
					interiorSelection.copyTo(tricoDirectory.resolvePath(INTERIOR_SELECTION));
				}
				if(!tricoDirectory.resolvePath(CARING_HOME).exists){
					caringHome.copyTo(tricoDirectory.resolvePath(CARING_HOME));
				}
				if(!tricoDirectory.resolvePath(MOVING_CHECKLIST).exists){
					movingChecklist.copyTo(tricoDirectory.resolvePath(MOVING_CHECKLIST));
				}
				if(!tricoDirectory.resolvePath(MOVING_TIMELINE).exists){
					movingTimeline.copyTo(tricoDirectory.resolvePath(MOVING_TIMELINE), false);
				}
				
			}
			
		}
		
	}
	
}