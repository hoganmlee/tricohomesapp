package com.sajakfarki.trico.model.vo
{
	import flash.utils.Dictionary;

	public class WishList
	{
		private var _groups:Vector.<WishListGroup>;
		
		//_groupDictionary[advertiserName] = WishListGroup
		private var _groupDictionary:Dictionary;
		
		//_userDictionary[adID] = WishListFinder
		private var _userDictionary:Dictionary;
		
		private var _total:int = 0;
		
		public function get total():int
		{
			return _total;
		}

		public function set total(value:int):void
		{
			_total = value;
		}

		public function get userDictionary():Dictionary
		{
			if(_userDictionary == null) _userDictionary = new Dictionary();
			return _userDictionary;
		}

		public function set userDictionary(value:Dictionary):void
		{
			_userDictionary = value;
		}

		public function get groupDictionary():Dictionary
		{
			if(_groupDictionary == null) _groupDictionary = new Dictionary();
			return _groupDictionary;
		}

		public function set groupDictionary(value:Dictionary):void
		{
			_groupDictionary = value;
		}

		public function get groups():Vector.<WishListGroup>
		{
			if(_groups == null) _groups = new Vector.<WishListGroup>();
			return _groups;
		}

		public function set groups(value:Vector.<WishListGroup>):void
		{
			_groups = value;
		}

	}
}