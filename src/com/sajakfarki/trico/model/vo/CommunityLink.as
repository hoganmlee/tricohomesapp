package com.sajakfarki.trico.model.vo
{
	public class CommunityLink
	{
		private var _id:String;
		private var _community:String;
		private var _heading:String;
		private var _description:String;
		private var _link:String;

		public function get link():String
		{
			return _link;
		}

		public function set link(value:String):void
		{
			_link = value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description = value;
		}

		public function get heading():String
		{
			return _heading;
		}

		public function set heading(value:String):void
		{
			_heading = value;
		}

		public function get community():String
		{
			return _community;
		}

		public function set community(value:String):void
		{
			_community = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

	}
}