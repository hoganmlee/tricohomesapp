package com.sajakfarki.trico.model.vo
{
	public class WishListFinder
	{
		private var _group:String;
		private var _adID:int;

		public function get adID():int
		{
			return _adID;
		}

		public function set adID(value:int):void
		{
			_adID = value;
		}

		public function get group():String
		{
			return _group;
		}

		public function set group(value:String):void
		{
			_group = value;
		}

	}
}