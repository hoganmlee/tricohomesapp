package com.sajakfarki.trico.model.vo
{
	public class AlbumItem
	{
		private var _imageURL:String;
		private var _thumbURL:String;
		private var _description:String;
		private var _date:String;
		
		public function AlbumItem()
		{
//			this.imageURL = imageURL;
//			this.thumbURL = thumbURL;
//			this.description = description;
//			this.date = date;
		}
		
		
		
		
		
		public function get date():String
		{
			return _date;
		}

		public function set date(value:String):void
		{
			_date = value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description = value;
		}

		public function get thumbURL():String
		{
			return _thumbURL;
		}

		public function set thumbURL(value:String):void
		{
			_thumbURL = value;
		}

		public function get imageURL():String
		{
			return _imageURL;
		}

		public function set imageURL(value:String):void
		{
			_imageURL = value;
		}

	}
}