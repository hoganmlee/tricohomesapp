package com.sajakfarki.trico.model.vo
{
	import flash.utils.Dictionary;

	public class CheckListGroup
	{
		private var _orderID:int;
		private var _groupID:int;
		private var _groupName:String;
		
		private var _items:Vector.<CheckListItem>;
		private var _itemDictionary:Dictionary;
		
		public function get itemDictionary():Dictionary
		{
			if(_itemDictionary == null )  _itemDictionary = new Dictionary();
			return _itemDictionary;
		}

		public function set itemDictionary(value:Dictionary):void
		{
			_itemDictionary = value;
		}

		public function get items():Vector.<CheckListItem>
		{
			if(_items == null) _items = new Vector.<CheckListItem>();
			return _items;
		}

		public function set items(value:Vector.<CheckListItem>):void
		{
			_items = value;
		}

		public function get groupName():String
		{
			return _groupName;
		}

		public function set groupName(value:String):void
		{
			_groupName = value;
		}

		public function get groupID():int
		{
			return _groupID;
		}

		public function set groupID(value:int):void
		{
			_groupID = value;
		}

		public function get orderID():int
		{
			return _orderID;
		}

		public function set orderID(value:int):void
		{
			_orderID = value;
		}

	}
}