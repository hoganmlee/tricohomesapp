package com.sajakfarki.trico.model.vo
{
	import flash.utils.Dictionary;

	public class WishListGroup
	{
		private var _advertiserName:String;
		
		private var _adContactName:String;
		private var _adContactEmail:String;
		private var _adContactPhone:String;
		
		private var _ads:Vector.<DisplayAd>;
		private var _adsDictionary:Dictionary;
		
		public function get adContactPhone():String
		{
			return _adContactPhone;
		}
		
		public function set adContactPhone(value:String):void
		{
			_adContactPhone = value;
		}
		
		public function get adContactEmail():String
		{
			return _adContactEmail;
		}
		
		public function set adContactEmail(value:String):void
		{
			_adContactEmail = value;
		}
		
		public function get adContactName():String
		{
			return _adContactName;
		}
		
		public function set adContactName(value:String):void
		{
			_adContactName = value;
		}
		
		public function get adsDictionary():Dictionary
		{
			if(_adsDictionary == null )  _adsDictionary = new Dictionary();
			return _adsDictionary;
		}

		public function set adsDictionary(value:Dictionary):void
		{
			_adsDictionary = value;
		}

		public function get ads():Vector.<DisplayAd>
		{
			if(_ads == null) _ads = new Vector.<DisplayAd>();
			return _ads;
		}

		public function set ads(value:Vector.<DisplayAd>):void
		{
			_ads = value;
		}

		public function get advertiserName():String
		{
			return _advertiserName;
		}

		public function set advertiserName(value:String):void
		{
			_advertiserName = value;
		}

	}
}