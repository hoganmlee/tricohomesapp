package com.sajakfarki.trico.model.vo
{
	public class Advertiser
	{
		private var _advertiserID:int;
		private var _name:String;
		private var _memo:String;
		private var _contactName:String;
		private var _contactEmail:String;
		private var _contactPhone:String;
		private var _contactNotes:String;
		
		public function get memo():String
		{
			return _memo;
		}

		public function set memo(value:String):void
		{
			_memo = value;
		}

		public function get contactNotes():String
		{
			return _contactNotes;
		}

		public function set contactNotes(value:String):void
		{
			_contactNotes = value;
		}

		public function get contactPhone():String
		{
			return _contactPhone;
		}

		public function set contactPhone(value:String):void
		{
			_contactPhone = value;
		}

		public function get contactEmail():String
		{
			return _contactEmail;
		}

		public function set contactEmail(value:String):void
		{
			_contactEmail = value;
		}

		public function get contactName():String
		{
			return _contactName;
		}

		public function set contactName(value:String):void
		{
			_contactName = value;
		}

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get advertiserID():int
		{
			return _advertiserID;
		}

		public function set advertiserID(value:int):void
		{
			_advertiserID = value;
		}

	}
}