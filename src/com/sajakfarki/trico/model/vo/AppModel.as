package com.sajakfarki.trico.model.vo
{

	public class AppModel
	{
		
		private var _token:String;
		private var _tricoUser:TricoUser;
	
		
		private var _checklist:CheckList;
		private var _wishlist:WishList;
		
		
		
		private var _albumList:Vector.<AlbumItem>;
		
		
		
		private var _offerSelected:int;
		
		
		
		private var _communityLinks:Vector.<CommunityLink>;
		
		
		
		public function get communityLinks():Vector.<CommunityLink>
		{
			if(_communityLinks == null) _communityLinks = new Vector.<CommunityLink>();
			return _communityLinks;
		}

		public function set communityLinks(value:Vector.<CommunityLink>):void
		{
			_communityLinks = value;
		}

		public function get albumList():Vector.<AlbumItem>
		{
//			if(_albumList == null) _albumList = new Vector.<AlbumItem>();
			return _albumList;
		}

		public function set albumList(value:Vector.<AlbumItem>):void
		{
			_albumList = value;
		}

		public function get offerSelected():int
		{
			return _offerSelected;
		}

		public function set offerSelected(value:int):void
		{
			_offerSelected = value;
		}

		public function get wishlist():WishList
		{
			return _wishlist;
		}

		public function set wishlist(value:WishList):void
		{
			_wishlist = value;
		}

		public function get checklist():CheckList
		{
			return _checklist;
		}

		public function set checklist(value:CheckList):void
		{
			_checklist = value;
		}

		public function get tricoUser():TricoUser
		{
			return _tricoUser;
		}

		public function set tricoUser(value:TricoUser):void
		{
			_tricoUser = value;
		}

		public function get token():String
		{
			return _token;
		}

		public function set token(value:String):void
		{
			_token = value;
		}

	}
}