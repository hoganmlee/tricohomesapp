package com.sajakfarki.trico.model.vo
{
	public class CheckListItem
	{
		private var _itemID:int;
		private var _groupID:int;
		private var _itemName:String;
		private var _orderID:int;
		private var _description:String;
		private var _checked:Boolean;
		
		public function get checked():Boolean
		{
			return _checked;
		}

		public function set checked(value:Boolean):void
		{
			_checked = value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description = value;
		}

		public function get orderID():int
		{
			return _orderID;
		}

		public function set orderID(value:int):void
		{
			_orderID = value;
		}

		public function get itemName():String
		{
			return _itemName;
		}

		public function set itemName(value:String):void
		{
			_itemName = value;
		}

		public function get groupID():int
		{
			return _groupID;
		}

		public function set groupID(value:int):void
		{
			_groupID = value;
		}

		public function get itemID():int
		{
			return _itemID;
		}

		public function set itemID(value:int):void
		{
			_itemID = value;
		}

	}
}