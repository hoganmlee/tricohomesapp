package com.sajakfarki.trico.model.vo
{
	public class TricoUser
	{
		private var _id:String;
		private var _name:String;
		private var _community:String;
		private var _email:String;
		
		public function get email():String
		{
			return _email;
		}

		public function set email(value:String):void
		{
			_email = value;
		}

		public function get community():String
		{
			return _community;
		}

		public function set community(value:String):void
		{
			_community = value;
		}

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

	}
}