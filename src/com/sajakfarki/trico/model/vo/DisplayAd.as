package com.sajakfarki.trico.model.vo
{
	public class DisplayAd
	{
		public static const TYPE_AD_1:String = "AD_1";
		public static const TYPE_AD_2:String = "AD_2";
		public static const TYPE_AD_3:String = "AD_3";
		public static const TYPE_AD_FULLPAGE:String = "AD_4";
		
		private var _advertiserName:String;
		
		private var _adID:int;
		private var _media:String;
		private var _actionURL:String;
		
		private var _isExclusive:Boolean;
		private var _exclusiveTitle:String;
		private var _exclusiveDescription:String;

		public function get advertiserName():String
		{
			return _advertiserName;
		}

		public function set advertiserName(value:String):void
		{
			_advertiserName = value;
		}

		public function get exclusiveDescription():String
		{
			return _exclusiveDescription;
		}

		public function set exclusiveDescription(value:String):void
		{
			_exclusiveDescription = value;
		}

		public function get exclusiveTitle():String
		{
			return _exclusiveTitle;
		}

		public function set exclusiveTitle(value:String):void
		{
			_exclusiveTitle = value;
		}

		public function get isExclusive():Boolean
		{
			return _isExclusive;
		}

		public function set isExclusive(value:Boolean):void
		{
			_isExclusive = value;
		}

		public function get actionURL():String
		{
			return _actionURL;
		}

		public function set actionURL(value:String):void
		{
			_actionURL = value;
		}

		public function get media():String
		{
			return _media;
		}

		public function set media(value:String):void
		{
			_media = value;
		}

		public function get adID():int
		{
			return _adID;
		}

		public function set adID(value:int):void
		{
			_adID = value;
		}

	}
}