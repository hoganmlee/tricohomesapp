package com.sajakfarki.trico.model.vo
{
	import flash.utils.Dictionary;

	public class CheckList
	{
		private var _groups:Vector.<CheckListGroup>;
		private var _groupDictionary:Dictionary;
		
		private var _tally:int = 0;
		private var _total:int = 0;
		
		public function get total():int
		{
			return _total;
		}

		public function set total(value:int):void
		{
			_total = value;
		}

		public function get tally():int
		{
			return _tally;
		}

		public function set tally(value:int):void
		{
			_tally = value;
		}

		public function get groupDictionary():Dictionary
		{
			if(_groupDictionary == null) _groupDictionary = new Dictionary();
			return _groupDictionary;
		}

		public function set groupDictionary(value:Dictionary):void
		{
			_groupDictionary = value;
		}

		public function get groups():Vector.<CheckListGroup>
		{
			if(_groups == null) _groups = new Vector.<CheckListGroup>();
			return _groups;
		}

		public function set groups(value:Vector.<CheckListGroup>):void
		{
			_groups = value;
		}

	}
}