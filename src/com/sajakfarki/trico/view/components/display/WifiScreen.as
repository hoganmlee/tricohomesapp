package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class WifiScreen extends FeathersControl
	{
		private var _bg:Scale9Image;
		private var _img:Image;
		private var _title:TextField;
		private var _disable:Quad;
		
		public function WifiScreen()
		{
			super();
		}
		
		public function setBG(texture:Scale9Textures, icon:Texture):void
		{
			_bg = new Scale9Image(texture);
			_bg.width  = 376;
			_bg.height  = 216;
			addChild(_bg);
			
			_img = new Image(icon);
			addChild(_img);
			
			_title = new TextField(208, 100, "Wifi connection required", TricoTheme.FONT_HELL_45Light_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			this.addChild(_title);
			
		}
		
		override protected function initialize():void
		{
			_disable = new Quad(TricoHomesApp.WIDTH, TricoHomesApp.HEIGHT, 0x000000, true);
			_disable.alpha = 0.5;
			this.addChildAt(_disable, 0);
			
		}		
				
		override public function dispose():void
		{
			if(_img) _img.dispose();
			if(_bg) _bg.dispose();
			super.dispose();
		}
		
		public function set image(texture:Texture):void
		{
			_img = new Image(texture);
			this.addChild(_img);
			this.invalidate(INVALIDATION_FLAG_SIZE);
		}
		
		override protected function draw():void
		{
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(sizeInvalid)
			{
				this.setSizeInternal(_bg.width, _bg.height, false);
			}
			
			_disable.x = -_disable.width/2;
			_disable.y = -_disable.height/2;
			
			_bg.x = -_bg.width/2-2;
			_bg.y = -_bg.height/2-3;
//			_bg.alpha = 0.5;
			
			_img.x = -_img.width/2;
			_img.y = -_img.height/2-50;
			
			_title.x = -_title.width/2;
			//_title.y = 0;
			
		}
	}
}