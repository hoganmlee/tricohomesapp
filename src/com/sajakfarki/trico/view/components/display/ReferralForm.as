package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ReferralForm extends FeathersControl
	{
		public static var EMAIL_SUBJECT:String = "Hey, need some email subject copy for the referral section";
		public static var EMAIL_LIST:String = "hogan@sajakfarki.com";
		//   												referrals@tricohomes.com
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
		
		private var _nameTitle:TextField;
		private var _nameInput:UserTextInput;
		private var _phoneTitle:TextField;
		private var _phoneInput:UserTextInput;
		private var _emailTitle:TextField;
		private var _emailInput:UserTextInput;
		
		private var _sendButton:Button;
		
		private var _verify:TextField;
		
		private var _validEmail:Boolean;
		private var _validName:Boolean;
		private var _suggestedTitle:TextField;
		private var _suggestedInput:UserTextInput;
		private var _title:TextField;
		
		private var _bottomBar:Quad;
		
		public function ReferralForm()
		{
			super();
		}
		
		override protected function initialize():void
		{
			addBarRight(106);
			
			_title = new TextField(460, 300, "Who you would like to refer", TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			_title.y = 136;
			this.addChild(_title);
			
			_nameTitle = new TextField(300, 100, "NAME", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_nameTitle.vAlign = VAlign.TOP;
			_nameTitle.hAlign = HAlign.LEFT;
			_nameTitle.y = 186;
			this.addChild(_nameTitle);
			
			_phoneTitle = new TextField(300, 100, "PHONE NUMBER", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_phoneTitle.vAlign = VAlign.TOP;
			_phoneTitle.hAlign = HAlign.LEFT;
			_phoneTitle.y = 283;
			this.addChild(_phoneTitle);
			
			_emailTitle = new TextField(300, 100, "EMAIL", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_emailTitle.vAlign = VAlign.TOP;
			_emailTitle.hAlign = HAlign.LEFT;
			_emailTitle.y = 383;
			this.addChild(_emailTitle);
			
			_suggestedTitle = new TextField(300, 100, "SUGGESTED COMMUNITY", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_suggestedTitle.vAlign = VAlign.TOP;
			_suggestedTitle.hAlign = HAlign.LEFT;
			_suggestedTitle.y = 483;
			this.addChild(_suggestedTitle);
			
			_nameInput = new UserTextInput();
			_nameInput.setDimensions(475, 50);
			_nameInput.addEventListener(FeathersEventType.FOCUS_IN, resetName);
			_nameInput.y = 208;
			addChild(_nameInput); 
			
			_phoneInput = new UserTextInput();
			_phoneInput.setDimensions(475, 50);
			_phoneInput.y = 308;
			addChild(_phoneInput);
			
			_emailInput = new UserTextInput();
			_emailInput.setDimensions(475, 50);
			_emailInput.addEventListener(FeathersEventType.FOCUS_IN, resetEmail);
			_emailInput.y = 408;
			addChild(_emailInput);
			
			_suggestedInput = new UserTextInput();
			_suggestedInput.setDimensions(475, 50);
			_suggestedInput.y = 508;
			addChild(_suggestedInput);
			
			_verify = new TextField(475, 60, "sending...", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON); 
			_verify.vAlign = VAlign.CENTER;
			_verify.hAlign = HAlign.CENTER;
			_verify.y = 609;
			addChild(_verify);
			
			_sendButton= new Button();
			_sendButton.nameList.add("sendLong");
			//			_sendButton.width = 475;
			_sendButton.label = "Send";
			_sendButton.addEventListener(Event.TRIGGERED, sendListener);
			_sendButton.y = 609;
			addChild(_sendButton);
			
			_bottomBar = new Quad(475, 1, 0xD9D8D4);
			_bottomBar.y = 700;
			_bottomBar.x = 0;
			addChild(_bottomBar); 
			
		}
		
		private function resetEmail(e:Event):void
		{
			_emailInput.reset();
		}
		
		private function resetName(e:Event):void
		{
			_nameInput.reset();
		}
		
		private function isValidEmail(email:String):Boolean {
			var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test(email);
		}
		
		private function sendListener(event:Event):void
		{
			
			if(_nameInput.text == ""){
				trace(_nameTitle);
				_nameTitle.color = TricoTheme.COLOR_ERROR;
				_nameTitle.text = "NAME (ex. Firstname Lastname)";
				_nameInput.error();
				_validName = false;
			}else{
				_nameTitle.color = TricoTheme.COLOR_ACCENT_BUTTON;
				_nameTitle.text = "NAME";
				_validName = true;
			}
			
			if (!isValidEmail(_emailInput.text)) {
				_emailTitle.color = TricoTheme.COLOR_ERROR;
				_emailTitle.text = "Email (please enter a valid email address)";
				_emailInput.error();
				_validEmail = false;
			}else{
				_emailTitle.color = TricoTheme.COLOR_ACCENT_BUTTON;
				_emailTitle.text = "Email";
				_validEmail = true;
			}
			
			if(_validEmail && _validName) {
				
				_sendButton.visible = false;
				_verify.text = "Sending...";
							
				//send email
				sendEmail.dispatch(EMAIL_SUBJECT, EMAIL_LIST,
					"\nName: "+ _nameInput.text +"\n"+ 
					"Phone: "+ _phoneInput.text +"\n"+
					"Email: "+ _emailInput.text +"\n"+
					"Suggested Community: " + _suggestedInput.text +"\n", false);
				
			}
			
		}
		
		private function addBarRight(yPos:int):void
		{
			var bar:Quad = new Quad(475, 1, 0xD9D8D4);
			bar.y = yPos;
			bar.x = 0;
			addChild(bar); 
		}
		
		public function emailSent(value:Boolean):void
		{
			if(value){
				_title.text = "Thank you for your submission!";
				_bottomBar.y = 286;
				
				_nameTitle.visible = false;
				_phoneTitle.visible = false;
				_emailTitle.visible = false;
				_suggestedTitle.visible = false;
				_nameInput.visible = false;
				_phoneInput.visible = false;
				_emailInput.visible = false;
				_suggestedInput.visible = false;
				
				_verify.text = "";
				
				_sendButton.visible = true;
				_sendButton.label = "Submit Another?";
				_sendButton.removeEventListener(Event.TRIGGERED, sendListener);
				_sendButton.addEventListener(Event.TRIGGERED, anotherListener);
				_sendButton.y = 194;
				
			}else{
				_verify.text = "Service unavailable...";
			}
		}
		
		private function anotherListener(event:Event):void
		{
			_nameTitle.visible = true;
			_phoneTitle.visible = true;
			_emailTitle.visible = true;
			_suggestedTitle.visible = true;
			_nameInput.visible = true;
			_nameInput.text = "";
			_phoneInput.visible = true;
			_phoneInput.text = "";
			_emailInput.visible = true;
			_emailInput.text = "";
			_suggestedInput.visible = true;
			_suggestedInput.text = "";
			
			_bottomBar.y = 700;
			
			_sendButton.label = "Send";
			_sendButton.addEventListener(Event.TRIGGERED, sendListener);
			_sendButton.removeEventListener(Event.TRIGGERED, anotherListener);
			_sendButton.y = 609;
		}
		
		override protected function draw():void
		{
			
		}
		
	}
	
}

