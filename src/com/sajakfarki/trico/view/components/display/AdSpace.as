package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class AdSpace extends FeathersControl
	{
		public var clicked:Signal = new Signal();
		public var exclusive:Signal = new Signal(int);
		
		private var _button:Button;
		private var _loader:ImageLoader;
		private var _exclusive:Button;
		private var _bg:Quad;
		
		private var _isExclusive:Boolean = false;
		
		private var _adTransition:Tween;
		private var _exclusiveTransition:Tween;
		private var _ease:Object = Transitions.LINEAR;
		private var _duration:Number = 0.3;
		
		private var _source:DisplayAd;
		
		private var _width:int;
		private var _height:int;
		
		public function AdSpace(width:int, height:int)
		{
			super();
			
			_width = width;
			_height = height;
			
			_bg = new Quad(_width, _height);
			addChild(_bg);
			
			_loader = new ImageLoader();
			_loader.clipRect = new Rectangle(0,0, _width, _height);
			_loader.isEnabled = false;
			_loader.smoothing = TextureSmoothing.NONE;
			_loader.alpha = 0;
			_loader.addEventListener(Event.COMPLETE, loadComplete);
			_loader.addEventListener(FeathersEventType.ERROR, errorListener);
			addChild(_loader);
			
			_exclusive = new Button();
			_exclusive.nameList.add("counter");
			_exclusive.label = "EXCLUSIVE OFFER";
			_exclusive.alpha = 0;
			addChild(_exclusive);
			
			//HITSPOT
			_button = new Button();
			_button.width = _width;
			_button.height = _height;
			addChild(_button);
			
		}
		
		public function get displayAd():DisplayAd
		{
			return _source;
		}
		
		public function setAd(source:DisplayAd, exclusiveOverride:Boolean = false):void
		{
			//trace("AdSpace :: setAd = " + source.adName);
			_source = source;
			
			
			if(exclusiveOverride){
				_isExclusive = false;
			}else{
				_isExclusive = _source.isExclusive;
			}
			
			
			// click handler clickAd.dispatch(id);
			if( _source.actionURL != "" && (!_isExclusive || exclusiveOverride) ){
				_button.addEventListener(Event.TRIGGERED, adClicked);
			}else{
				_button.removeEventListener(Event.TRIGGERED, adClicked);
			}
						
			if(_loader.isLoaded){
				
				removeAd();
				
			}else{
				
				_loader.source = _source.media;
				
			}
		}
		
		private function adClicked(e:Event):void
		{
			if(_source.actionURL.indexOf("http") < 0){
				_source.actionURL = "http://"+_source.actionURL;
			}
			
			var request:URLRequest = new URLRequest(_source.actionURL);
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
			
			clicked.dispatch();
		}
		
		private function errorListener(e:Event):void
		{
			trace(" >>>> AdSpace :: Error  - " + e);
		}
		
		private function loadAd():void
		{
			_loader.source = _source.media;
		}
		
		private function removeAd():void
		{
			this.isExclusive = false;
			
			_adTransition = new Tween(_loader, _duration, _ease);
			_adTransition.fadeTo(0);
			_adTransition.onComplete = removeAd_onComplete;
			Starling.juggler.add(this._adTransition);
		}
		
		private function removeAd_onComplete():void
		{
			Starling.juggler.remove(_adTransition);
			_adTransition = null;
			loadAd();
		}
		
		private function loadComplete(e:Event):void
		{
			_adTransition = new Tween(_loader, _duration, _ease);
			_adTransition.fadeTo(1)
			_adTransition.onComplete = activeTransition_onComplete;
			Starling.juggler.add(_adTransition);
			
			this.isExclusive = _isExclusive;
		}
		
		private function activeTransition_onComplete():void
		{
			this._adTransition = null;
		}
		
		public function set isExclusive(b:Boolean):void
		{
			
			if(b){
				
				_button.addEventListener(Event.TRIGGERED, exclusiveClicked);
				
				if(_exclusive.alpha != 1){
					_exclusiveTransition = new Tween(_exclusive, _duration, _ease);
					_exclusiveTransition.fadeTo(1)
					_exclusiveTransition.onComplete = exclusiveTransition_onComplete;
					Starling.juggler.add(this._exclusiveTransition);
				}
				
			}else{
				
				_button.removeEventListener(Event.TRIGGERED, exclusiveClicked);
				
				if(_exclusive.alpha != 0){
					_exclusiveTransition = new Tween(_exclusive, _duration, _ease);
					_exclusiveTransition.fadeTo(0)
					_exclusiveTransition.onComplete = exclusiveTransition_onComplete;
					Starling.juggler.add(this._exclusiveTransition);
				}
			}
		}
		
		private function exclusiveClicked(e:Event):void
		{
			exclusive.dispatch(_source.adID);
		}
		
		private function exclusiveTransition_onComplete():void
		{
			Starling.juggler.remove(_exclusiveTransition);
			_exclusiveTransition = null;
		}
		
		override protected function initialize():void
		{
			
		}
		
		override protected function draw():void
		{
			_exclusive.validate();
			_exclusive.x = _width - _exclusive.width;
		}
		
	}
}