package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.core.FeathersControl;
	
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class OfferPanel extends FeathersControl
	{
		private var _title:TextField;
		private var _body:TextField;
		
		private var _width:int;
		
		private var _padding:int;
		
		public function OfferPanel(width:int, padding:int)
		{
			super();			
			_width = width;
			_padding = padding;
		}
		
		public function offer(title:String, body:String):void
		{
			_title.text = title;
			
			_body.text = body;
			_body.height = _body.textBounds.height;
			
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			
		}
		
		override protected function initialize():void
		{
			_title = new TextField(_width, 24, 
											"",
											TricoTheme.FONT_HELL_67MediumCond_20,
											BitmapFont.NATIVE_SIZE,
											TricoTheme.COLOR_ACCENT_BUTTON);
			
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			_title.x = _padding;
			_title.y = _padding;
			
			addChild(_title);
			
			_body = new TextField(_width, 10, "",
												TricoTheme.FONT_HELL_55Roman_18,
												BitmapFont.NATIVE_SIZE,
												TricoTheme.COLOR_SIDENAV_TEXT);
			_body.vAlign = VAlign.TOP;
			_body.hAlign = HAlign.LEFT;
			_body.x = _title.x;
			_body.y = _title.y + 16 + _padding;
			
			addChild(_body);
		}
		
		override protected function draw():void
		{
			this.height = _body.y + _body.textBounds.height + _padding;
			
			//TODO: title, phone, email (x,y);
			
		}
		
	}
	
}