package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class EquityForm extends FeathersControl
	{
		public static var EMAIL_SUBJECT:String = "Hey, need some email subject copy for the equity section";
		public static var EMAIL_LIST:String = "hogan@sajakfarki.com";
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
		
		private var _emailTitle:TextField;
		private var _emailInput:UserTextInput;
		
		private var _sendButton:Button;
		
		private var _verify:TextField;
		
		private var _validEmail:Boolean;
		
		private var _bottomBar:Quad;
		private var _title:TextField;
		
		public function EquityForm()
		{
			super();
		}
		
		override protected function initialize():void
		{
			addBarRight(106);
			//addBarRight(700);
			
			_title = new TextField(575, 300, "Please contact me, I’m interested in your Equity Saver Program", TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			_title.y = 133;
			this.addChild(_title);
			
			
			_emailTitle = new TextField(600, 100, "PLEASE CONFIRM YOUR CURRENT EMAIL", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_emailTitle.vAlign = VAlign.TOP;
			_emailTitle.hAlign = HAlign.LEFT;
			_emailTitle.y = 228;
			this.addChild(_emailTitle);
						
			_emailInput = new UserTextInput();
			_emailInput.setDimensions(325, 50);
			_emailInput.addEventListener(FeathersEventType.FOCUS_IN, resetEmail);
			_emailInput.y = 252;
			addChild(_emailInput);
						
			_verify = new TextField(475, 60, "sending...", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON); 
			_verify.vAlign = VAlign.CENTER;
			_verify.hAlign = HAlign.CENTER;
			_verify.y = 251;
			_verify.x = 280;
			addChild(_verify);
			
			_sendButton= new Button();
			_sendButton.nameList.add("sendLong");
			//			_sendButton.width = 475;
			_sendButton.label = "Send";
			_sendButton.addEventListener(Event.TRIGGERED, sendListener);
			_sendButton.width = 325;
			_sendButton.y = 251;
			_sendButton.x = 350;
			addChild(_sendButton);
			
			_bottomBar = new Quad(675, 1, 0xD9D8D4);
			_bottomBar.y = 338;
			_bottomBar.x = 0;
			addChild(_bottomBar); 
			
		}
		
		private function resetEmail(e:Event):void
		{
			_emailInput.reset();
		}
				
		private function isValidEmail(email:String):Boolean {
			var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test(email);
		}
		
		private function sendListener(event:Event):void
		{
			if (!isValidEmail(_emailInput.text)) {
				_emailTitle.color = TricoTheme.COLOR_ERROR;
//				_emailTitle.text = "PLEASE CONFIRM YOUR CURRENT EMAIL ADDRESS"
				_emailInput.error();
				_validEmail = false;
			}else{
				_emailTitle.color = TricoTheme.COLOR_ACCENT_BUTTON;
//				_emailTitle.text = "PLEASE CONFIRM YOUR CURRENT EMAIL ADDRESS";
				_validEmail = true;
			}
			
			if(_validEmail) {
				
				_sendButton.visible = false;
				_verify.text = "Sending...";
				
				//send email
				sendEmail.dispatch(EMAIL_SUBJECT, EMAIL_LIST, 
					"Email: "+ _emailInput.text , true); 
			}
		}
		
		private function addBarRight(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			bar.x = 0;
			addChild(bar); 
		}
		
		public function emailSent(value:Boolean):void
		{
			if(value){
				_title.text = "Thank you! We will contact you shortly.";
				_emailTitle.visible = false;
				_emailInput.visible =false;
				_bottomBar.y = 182;
				_verify.text = "";
				
			}else{
				_verify.text = "Service unavailable...";
			}
		}
	
		override protected function draw():void
		{
			
		}
		
	}
	
}
