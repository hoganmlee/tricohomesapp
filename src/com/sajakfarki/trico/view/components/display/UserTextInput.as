package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.controls.Button;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.textures.Scale9Textures;
	
	import starling.events.Event;
	
	public class UserTextInput extends FeathersControl
	{
		private var _bg:Scale9Image;
		private var _userInput:TextInput;
		
		private var _width:int = 325;
		private var _height:int = 50;
		
		private var _invalidInputButton:Button;
		
		public function UserTextInput()
		{
			super();
		}
		
		public function get text():String
		{
			return _userInput.text;
		}
		
		public function set text(value:String):void
		{
			_userInput.text = value;
		}
				
		public function setBG(texture:Scale9Textures):void
		{
			_bg = new Scale9Image(texture);
			_bg.width  = _width;
			_bg.height  = _height;
		}
		
		public function setDimensions(width:int, height:int):void
		{
			_width = width;			
			_height = height;
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
		}
		
		override protected function initialize():void
		{
			addChild(_bg);
			
			_userInput = new TextInput();
			_userInput.textEditorProperties.color = 0x97978E;
			_userInput.textEditorProperties.autoCorrect = false;
			_userInput.textEditorProperties.fontFamily = "Helvetica";			
			_userInput.nameList.add("userTextInput");
			_userInput.addEventListener(FeathersEventType.FOCUS_IN, inputFocusIn);
			_userInput.addEventListener(FeathersEventType.FOCUS_OUT, inputFocusOut);
//			_userInput.addEventListener(FeathersEventType.ENTER, focusPassword);
			this.addChild(_userInput);
			
			_invalidInputButton = new Button();
			_invalidInputButton.visible = false;
			_invalidInputButton.nameList.add("closeButton");
			_invalidInputButton.x = 287;
			_invalidInputButton.y = 11;
			this.addChild(_invalidInputButton);
			
		}
		
		public function error():void
		{
			_invalidInputButton.visible = true;
			_userInput.textEditorProperties.color = TricoTheme.COLOR_ERROR;
		}
		
		public function reset():void
		{
			_invalidInputButton.visible = false;
			_userInput.textEditorProperties.color = TricoTheme.COLOR_ACCENT_BUTTON;
		}
		
		private function inputFocusIn(e:Event):void
		{
			this.dispatchEventWith(FeathersEventType.FOCUS_IN);
		}
				
		private function inputFocusOut(e:Event):void
		{
			this.dispatchEventWith(FeathersEventType.FOCUS_OUT);
		}
		
		override protected function draw():void
		{
			_bg.height = _height;
			_bg.width = _width;
			
			_invalidInputButton.x = _width - 38;
			
			_userInput.height = _height;
			_userInput.width = _width;
			_userInput.validate();
			_userInput.x = 8;
			
		}
			
	}
}