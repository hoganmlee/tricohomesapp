package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
		
	import feathers.controls.Button;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.textures.Scale9Textures;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class LoginInput extends FeathersControl
	{
//		public static const FOCUS_IN:String = "login.input.focus.in";
//		public static const FOCUS_OUT:String = "login.input.focus.out";
		
		private static const INPUT_WIDTH:int = 280;
		
		private var _bg:Scale9Image;
		private var _userName:TextInput;
		private var _userPassword:TextInput;
		private var _nameOverlay:LoginOverlayBox;
		private var _passwordOverlay:LoginOverlayBox;
		
		private var _closeButtonUser:Button;
		private var _closeButtonPassword:Button;
				
		private var _closeLabelUser:Button;
		private var _closeLabelPassword:Button;
		
		private var _userTween:Tween;
		private var _passwordTween:Tween
		
		private var _loginUserFail:Boolean = true;
		private var _loginPasswordFail:Boolean = true;
		
		public function LoginInput()
		{
			super();
		}
		
		public function setBG(texture:Scale9Textures):void
		{
			_bg = new Scale9Image(texture);
			_bg.width  = 375;
			_bg.height  = 125;
		}
		
		public function get userName():String
		{
			return _userName.text;
		}
		
		public function set userName(s:String):void
		{
			_userName.text = s;
		}
		
		public function get userPassword():String
		{
			return _userPassword.text;
		}
		
		public function set userPassword(s:String):void
		{
			_userPassword.text = s;
		}
		
		override protected function initialize():void
		{
			addChild(_bg);
			
			var divider:Quad = new Quad(375, 2, 0xCCCCCC);
			divider.y = 62;
			addChild(divider);
			
			_userName = new TextInput();
			_userName.textEditorProperties.color = TricoTheme.COLOR_ACCENT_BUTTON;
			_userName.textEditorProperties.autoCorrect = false;
			_userName.textEditorProperties.fontFamily = "Helvetica";
			_userName.height = 50;
			_userName.width = INPUT_WIDTH;
			_userName.addEventListener(FeathersEventType.FOCUS_IN, userNameFocus);
			_userName.addEventListener(FeathersEventType.FOCUS_OUT, inputFocusOut);
			_userName.addEventListener(FeathersEventType.ENTER, focusPassword);
			
			this.addChild(_userName);
			
			_userPassword = new TextInput();
			_userPassword.textEditorProperties.color = TricoTheme.COLOR_ACCENT_BUTTON;
			_userPassword.textEditorProperties.autoCorrect = false;
			_userPassword.textEditorProperties.fontFamily = "Helvetica";
			_userPassword.height = 50;
			_userPassword.width = INPUT_WIDTH;
			_userPassword.textEditorProperties.displayAsPassword = true;
			_userPassword.addEventListener(FeathersEventType.FOCUS_IN, userPasswordFocus);
			_userPassword.addEventListener(FeathersEventType.FOCUS_OUT, inputFocusOut);
			_userPassword.addEventListener(FeathersEventType.ENTER, submitLogin);
			
			this.addChild(_userPassword);
			
			_nameOverlay = new LoginOverlayBox("User Name", 13, INPUT_WIDTH, 100);
			_nameOverlay.addEventListener(Event.OPEN, focusName);
//			_nameOverlay.visible = false;
			this.addChild(_nameOverlay);
			
			_passwordOverlay = new LoginOverlayBox("User Password", 13, INPUT_WIDTH, 100);
			_passwordOverlay.addEventListener(Event.OPEN, focusPassword);
//			_passwordOverlay.visible = false;
			this.addChild(_passwordOverlay);
			
			_closeButtonUser = new Button();
			_closeButtonUser.visible = false;
			_closeButtonUser.nameList.add("closeButton");
			this.addChild(_closeButtonUser);
			
			_closeLabelUser = new Button();
			_closeLabelUser.alpha = 0;
			_closeLabelUser.nameList.add("closeLabelUser");
			this.addChild(_closeLabelUser);
			
			_closeButtonPassword = new Button();
			_closeButtonPassword.visible = false;
			_closeButtonPassword.nameList.add("closeButton");
			this.addChild(_closeButtonPassword);
			
			_closeLabelPassword = new Button();
			_closeLabelPassword.alpha = 0;
			_closeLabelPassword.nameList.add("closeLabelPassword");
			this.addChild(_closeLabelPassword);
			
		}
		
		private function checkSubmit():void
		{
			if(_loginPasswordFail == false && _loginUserFail == false){
				this.dispatchEventWith(FeathersEventType.BEGIN_INTERACTION);
			}else{
				this.dispatchEventWith(FeathersEventType.CLEAR);
			}
		}
		
		private function userNameFocus(e:Event):void
		{
			if(_loginUserFail){
				
				_userName.textEditorProperties.color = TricoTheme.COLOR_ACCENT_BUTTON;
				
				_closeButtonUser.visible = false;
				
				_closeButtonUser.removeEventListener(Event.TRIGGERED, focusName);
				_closeLabelUser.removeEventListener(Event.TRIGGERED, focusName);
				
				_userTween = new Tween(_closeLabelUser, 0.4, Transitions.EASE_IN_BACK);
				_userTween.delay = 0.3;
				_userTween.animate("alpha", 0);
				_userTween.animate("x", 397);
				Starling.juggler.add(_userTween);
				
				_loginUserFail = false;
			}
			
			checkSubmit();
			
			_nameOverlay.visible = false;
			
			_userName.selectRange(_userName.text.length);
			
			this.dispatchEventWith(FeathersEventType.FOCUS_IN);
		}
		
		private function userPasswordFocus(e:Event):void
		{
			if(_loginPasswordFail){
				
				_userPassword.textEditorProperties.color = TricoTheme.COLOR_ACCENT_BUTTON;
				_closeButtonPassword.visible = false;
				
				_closeButtonPassword.removeEventListener(Event.TRIGGERED, focusPassword);
				_closeLabelPassword.removeEventListener(Event.TRIGGERED, focusPassword);
				
				_passwordTween = new Tween(_closeLabelPassword, 0.4, Transitions.EASE_IN_BACK);
				_passwordTween.delay = 0.3;
				_passwordTween.animate("alpha", 0);
				_passwordTween.animate("x", 397);
				Starling.juggler.add(_passwordTween);
				
				_loginPasswordFail = false;
			}
			
			checkSubmit()
			
			_passwordOverlay.visible = false;
			_userPassword.selectRange(_userPassword.text.length);
			
			this.dispatchEventWith(FeathersEventType.FOCUS_IN);
		}
		
		private function submitLogin(e:Event):void
		{
			trace(">>>> LoginInput : submitLogin > "  + e);
			Starling.current.nativeStage.focus = null;
			this.dispatchEventWith(FeathersEventType.ENTER);
		}
		
		private function inputFocusOut(e:Event):void
		{
			switch(e.target){
				
				case _userPassword:
					if(_userPassword.text == ""){
						_loginPasswordFail = true 
						_passwordOverlay.visible = true;
					}
					break;
				case _userName:
					if(_userName.text == ""){
						_loginUserFail = true
						_nameOverlay.visible = true;
					}
					break;
			}
			
			checkSubmit();
			
			this.dispatchEventWith(FeathersEventType.FOCUS_OUT);
		}
		
		private function focusName(e:Event):void
		{
			trace(">>>> LoginInput :: nameclicked");
			_userName.setFocus();
		}
		
		private function focusPassword(e:Event):void
		{
			trace(">>>> LoginInput :: password clicked");
			_userPassword.setFocus();
		}
		
		public function wrongUser():void
		{
			_loginUserFail = true;
			
			_userName.textEditorProperties.color = TricoTheme.COLOR_ERROR;
			
			_closeButtonUser.visible = true;
			_closeButtonUser.addEventListener(Event.TRIGGERED, focusName);
			_closeLabelUser.addEventListener(Event.TRIGGERED, focusName);
//			_closeLabelPassword.visible = true;
			
			_userTween = new Tween(_closeLabelUser, 0.3, Transitions.EASE_OUT_BACK);
			_userTween.delay = 0.6;
			_userTween.animate("alpha", 1);
			_userTween.animate("x", 387);
			Starling.juggler.add(_userTween);
		}
		
		public function wrongPassword():void
		{
			_loginPasswordFail = true;
			
			_userPassword.textEditorProperties.color = TricoTheme.COLOR_ERROR;
			
			_closeButtonPassword.visible = true;
			_closeButtonPassword.addEventListener(Event.TRIGGERED, focusPassword);
			_closeLabelPassword.addEventListener(Event.TRIGGERED, focusPassword);
			
			_passwordTween = new Tween(_closeLabelPassword, 0.3, Transitions.EASE_OUT_BACK);
			_passwordTween.delay = 0.7;
			_passwordTween.animate("alpha", 1);
			_passwordTween.animate("x", 387);
			Starling.juggler.add(_passwordTween);
		}
		
		override protected function draw():void
		{
			_userName.validate();
			_userName.x = 48;
			_userName.y = 10;
			
			_nameOverlay.x = 102;
			_nameOverlay.y = 10;
			
			_userPassword.validate();
			_userPassword.x = 48;
			_userPassword.y = 70;

			_passwordOverlay.x = 79;
			_passwordOverlay.y = 70;
			
			_closeButtonUser.validate();
			_closeButtonUser.x = 333;
			_closeButtonUser.y = 17;
			
			_closeLabelUser.validate();
			_closeLabelUser.x = 397;
			_closeLabelUser.y = 16;
			
			_closeButtonPassword.validate();
			_closeButtonPassword.x = 333;
			_closeButtonPassword.y = 79;
			
			_closeLabelPassword.validate();
			_closeLabelPassword.x = 397;
			_closeLabelPassword.y = 76;
			
		}
		
	}
}