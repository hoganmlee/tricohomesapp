package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.controls.Button;
	
//	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import starling.display.Sprite;
	
	public class LoginOverlayBox extends Sprite
	{
//		public var clicked:Signal = new Signal();
		
		private var _labelName:TextField;
		private var _button:Button;
		
		public function LoginOverlayBox(label:String, padding:int, width:int, height:int)
		{
			super();
			
			_labelName = new TextField(width, height, label, TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, 0x999999);
			_labelName.hAlign = HAlign.LEFT;
			_labelName.vAlign = VAlign.TOP;
			_labelName.x = padding;
			_labelName.y = padding;
			
			this.addChild(_labelName);
			
			_button = new Button();
			_button.defaultSkin = new Quad(width, height, 0x000000);
			_button.alpha = 0;
			_button.addEventListener(Event.TRIGGERED, buttonPress);
			
			this.addChild(_button);
			
		}
		
		private function buttonPress(event:Event):void
		{
//			clicked.dispatch();
			this.dispatchEventWith(Event.OPEN);
		}
		
	}
	
}