package com.sajakfarki.trico.view.components.display
{
	import com.sajakfarki.trico.TricoTheme;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import org.osflash.signals.Signal;
	
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ServiceForm extends FeathersControl
	{
		public static var EMAIL_SUBJECT:String = "Hey, need some email subject copy for the service section";
		public static var EMAIL_LIST:String = "hogan@sajakfarki.com";
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
				
		private var _nameTitle:TextField;
		private var _nameInput:UserTextInput;
		private var _phoneTitle:TextField;
		private var _phoneInput:UserTextInput;
		private var _emailTitle:TextField;
		private var _emailInput:UserTextInput;
		
		private var _sendButton:Button;
		
		private var _verify:TextField;
		
		private var _validEmail:Boolean;
		private var _validName:Boolean;
				
		public function ServiceForm()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_nameTitle = new TextField(300, 100, "NAME", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_nameTitle.vAlign = VAlign.TOP;
			_nameTitle.hAlign = HAlign.LEFT;
			_nameTitle.y = 0;
			_nameTitle.x = 0;
			this.addChild(_nameTitle);
			
			_phoneTitle = new TextField(300, 100, "PHONE NUMBER", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_phoneTitle.vAlign = VAlign.TOP;
			_phoneTitle.hAlign = HAlign.LEFT;
			_phoneTitle.y = 0;
			_phoneTitle.x = 347;
			this.addChild(_phoneTitle);
			
			_emailTitle = new TextField(300, 100, "EMAIL", TricoTheme.FONT_HELL_57Cond_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);									
			_emailTitle.vAlign = VAlign.TOP;
			_emailTitle.hAlign = HAlign.LEFT;
			_emailTitle.y = 96;
			_emailTitle.x = 0;
			this.addChild(_emailTitle);
		
			
			_nameInput = new UserTextInput();
			_nameInput.addEventListener(FeathersEventType.FOCUS_IN, resetName);
			_nameInput.y = 25;
			this.addChild(_nameInput);
			
			_phoneInput = new UserTextInput();
			_phoneInput.y = 25;
			_phoneInput.x = 347;
			this.addChild(_phoneInput);
			
			_emailInput = new UserTextInput();
			_emailInput.addEventListener(FeathersEventType.FOCUS_IN, resetEmail);
			_emailInput.y = 121;
			this.addChild(_emailInput);
			
			
			_verify = new TextField(325, 60, "", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON); 
			_verify.vAlign = VAlign.CENTER;
			_verify.hAlign = HAlign.CENTER;
			_verify.y = 121;
			_verify.x = 347;
			this.addChild(_verify);
					
			_sendButton= new Button();
			_sendButton.nameList.add("send");
			_sendButton.label = "Send";
			_sendButton.addEventListener(Event.TRIGGERED, sendListener);
			_sendButton.y = 121;
			_sendButton.x = 347;
			this.addChild(_sendButton);
			
		}
		
		private function resetEmail(e:Event):void
		{
			_emailInput.reset();
		}
		
		private function resetName(e:Event):void
		{
			_nameInput.reset();
		}
		
		private function isValidEmail(email:String):Boolean {
			var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test(email);
		}
		
		private function sendListener(event:Event):void
		{
			if(_nameInput.text == ""){
				trace(_nameTitle);
				_nameTitle.color = TricoTheme.COLOR_ERROR;
				_nameTitle.text = "NAME (ex. Firstname Lastname)";
				_nameInput.error();
				_validName = false;
			}else{
				_nameTitle.color = TricoTheme.COLOR_ACCENT_BUTTON;
				_nameTitle.text = "NAME";
				_validName = true;
			}
			
			if (!isValidEmail(_emailInput.text)) {
				_emailTitle.color = TricoTheme.COLOR_ERROR;
				_emailTitle.text = "Email (please enter a valid email address)";
				_emailInput.error();
				_validEmail = false;
			}else{
				_emailTitle.color = TricoTheme.COLOR_ACCENT_BUTTON;
				_emailTitle.text = "Email";
				_validEmail = true;
			}
			
			if(_validEmail && _validName) {
				
				_sendButton.visible = false;
				_verify.text = "Sending...";
				
				trace("ServiceView >>> _nameInput.text = " , _nameInput.text);
				trace("ServiceView >>> _phoneInput.text = " , _phoneInput.text);
				trace("ServiceView >>> _emailInput.text = " , _emailInput.text);
				
				//send email
				sendEmail.dispatch(EMAIL_SUBJECT, EMAIL_LIST,
					"<br>Name: "+ _nameInput.text + 
					"<br>Phone: "+ _phoneInput.text + 
					"<br>Email: "+ _emailInput.text, true); 
			}
		}

		public function emailSent(value:Boolean):void
		{
			if(value){
				_verify.text = "Success!";
			}else{
				_verify.text = "Service unavailable...";
			}
		}
		
		override protected function draw():void
		{
			
		}
		
	}
	
}