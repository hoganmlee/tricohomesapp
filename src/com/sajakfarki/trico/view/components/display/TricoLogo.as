package com.sajakfarki.trico.view.components.display
{
	import feathers.core.FeathersControl;
	
	import starling.textures.Texture;
	import starling.display.Image;
	
	public class TricoLogo extends FeathersControl
	{
		private var _img:Image;
		
		public function TricoLogo()
		{
			super();
		}
		
		override public function dispose():void
		{
			if(_img) _img.dispose();
			super.dispose();
		}
		
		public function set image(texture:Texture):void
		{
			_img = new Image(texture);
			this.addChild(_img);
			this.invalidate(INVALIDATION_FLAG_SIZE);
		}
		
		override protected function draw():void
		{
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(sizeInvalid)
			{
				this.setSizeInternal(_img.width, _img.height, false);
			}
			
		}
		
	}
	
}