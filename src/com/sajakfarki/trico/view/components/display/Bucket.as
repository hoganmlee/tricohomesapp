package com.sajakfarki.trico.view.components.display
{
	
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import flash.geom.Rectangle;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class Bucket extends FeathersControl
	{
		public static const WINDOW_WIDTH:int = 362;
		public static const WINDOW_HEIGHT:int = 124;
				
		private var _source:String;
		private var _loader:ImageLoader;
		private var _counter:Button;
		private var _bg:Button;
		
		private var _activeTransition:Tween;
		private var _ease:Object = Transitions.LINEAR;
		private var _duration:Number = 0.6;
		private var _title:Button;
		
		public function Bucket(source:String, nameList:String, label:String, offsetY:int)
		{
			super();			
			
			_source = source;
						
			_bg = new Button();
			_bg.nameList.add("bucket");
			_bg.width = 375;
			_bg.height = 215;
			addChild(_bg);
			
			_loader = new ImageLoader();
			_loader.clipRect = new Rectangle(0,0, WINDOW_WIDTH, WINDOW_HEIGHT);
			_loader.isEnabled = false;
			_loader.smoothing = TextureSmoothing.NONE;
			_loader.alpha = 0;
			_loader.addEventListener(Event.COMPLETE, loadComplete);
			_loader.addEventListener(FeathersEventType.ERROR, errorListener);
			_loader.x = 6;
			_loader.y = 6;
			addChild(_loader);
						
			_counter = new Button();
			_counter.nameList.add("counter");
			_counter.label = "...";
			addChild(_counter);

			_title = new Button();
			_title.nameList.add(nameList);
			_title.label = label;
			_title.x = 6;
			_title.y = offsetY;
						
			addChild(_title);
			
			var b:Button = new Button();
//			b.defaultSkin = new Quad(10,10);
			b.width = 375;
			b.height = 215;
			b.addEventListener(Event.TRIGGERED, bgClicked);
			addChild(b);
			
		}
		
		private function bgClicked(e:Event):void
		{
			this.dispatchEventWith(Event.TRIGGERED);
		}
		
		public function set counter(value:String):void
		{
			_counter.label = value;
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
		}
		
		public function set source(s:String):void
		{
			_source = s;
			
			if(_loader.isLoaded){
				
				removeImage();
				
			}else{
				
				_loader.source = _source;
				
			}
			
		}
		
		private function errorListener(e:Event):void
		{
			trace(" >>>> Bucket :: Error  - " + e);
		}
				
		private function removeImage():void
		{
			this._activeTransition = new Tween(_loader, _duration, _ease);
			this._activeTransition.animate("alpha", 0);
			this._activeTransition.onComplete = removeImage_onComplete;
			Starling.juggler.add(this._activeTransition);
		}
		
		private function removeImage_onComplete():void
		{
			this._activeTransition = null;
			initialize();
		}
		
		private function loadComplete(e:Event):void
		{
			this._activeTransition = new Tween(_loader, _duration, _ease);
			this._activeTransition.animate("alpha", 1);
			this._activeTransition.onComplete = activeTransition_onComplete;
			Starling.juggler.add(this._activeTransition);
		}
		
		private function activeTransition_onComplete():void
		{
			this._activeTransition = null;
		}
	
		override protected function initialize():void
		{
			_loader.source = _source;
		}
		
		override protected function draw():void
		{
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const layoutInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_LAYOUT);
			
			if(layoutInvalid){
				
				_counter.validate();
				_counter.x = 368 - _counter.width;
				_counter.y = 6;
				
			}
			
		}
		
	}
}