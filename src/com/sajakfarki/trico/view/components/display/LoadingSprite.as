package com.sajakfarki.trico.view.components.display
{
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.Event;
	
	public class LoadingSprite extends FeathersControl
	{
		private var _mc:MovieClip;
				
		public function LoadingSprite()
		{
			super();
			this.alpha = 0;
		}
		
		public function setBG(mc:MovieClip):void
		{
			_mc = mc;
			_mc.x = -50;
			_mc.y = -50;
			_mc.fps = 24;
			_mc.loop = false;
			
			addChild(_mc);
		}
		
		override public function dispose():void
		{
			if(_mc){
				_mc.removeFromParent(true);
				Starling.juggler.remove(_mc);
				_mc.dispose();	
			}
			
			super.dispose();
		}
		
		public function load(b:Boolean):void
		{
			if(b){
				_mc.currentFrame = 0;
				_mc.addEventListener(Event.COMPLETE, movieCompletedHandler);
				this.alpha = 1;
				Starling.juggler.add(_mc);
			}else{
				this.alpha = 0;
				Starling.juggler.remove(_mc);
				
			}
		}
		
		private function movieCompletedHandler(e:Event):void
		{
			Starling.juggler.remove(_mc);
			_mc.removeFromParent(true);
			_mc.dispose();
			
			this.dispatchEventWith(FeathersEventType.TRANSITION_COMPLETE);
		}
				
		override protected function initialize():void
		{
			
		}
		
		override protected function draw():void
		{
			
		}
		
	}
	
}