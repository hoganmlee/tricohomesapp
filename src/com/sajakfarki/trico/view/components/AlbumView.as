package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.TricoNavigator;
	import com.sajakfarki.trico.model.vo.AlbumItem;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.HouseBuildSprite;
	import com.sajakfarki.trico.view.components.renderer.AlbumThumbRenderer;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	
	import feathers.controls.Button;
	import feathers.controls.List;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.data.ListCollection;
	import feathers.layout.HorizontalLayout;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.PanGesture;
	import org.gestouch.gestures.SwipeGesture;
	import org.gestouch.gestures.TapGesture;
	import org.gestouch.gestures.TransformGesture;
	import org.gestouch.gestures.ZoomGesture;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class AlbumView extends FeathersControl
	{
		private static const LOADER_CONTEXT:LoaderContext = new LoaderContext(true);
				
		public static var THUMB_HOLDER_POSITION_Y:int = 582;
		public static var THUMB_HOLDER_HEIGHT:int = 116;
		
//		public static var DESCRIPTION_POSITION_X:int = 191;
//		public static var DESCRIPTION_POSITION_Y:int = 510;
//		
//		public static var DATE_POSITION_X:int = 885; 
//		public static var DATE_POSITION_Y:int = 510;
		
		
		/**  Image height zoom  ********************************************/
//		public static var IMAGE_HEIGHT_FULL:int = 752;
		public static var IMAGE_OFFSET:int = -85;
		public static var IMAGE_OFFSET_FULLSCREEN:int = -50;		
		
		public static var IMAGE_HEIGHT_SMALL:int = 450;
		
		public static var IMAGE_FADE_X_DISTANCE:int = 100;
		
//		public static var FULLSCREEN_Y:int = 633;
		
		private static const LEFT:String = "throw.left";
		private static const RIGHT:String = "throw.right";
		
		private var _direction:String = LEFT;
		
		//TWEENS
		private var _fullScreenTransition:Tween;
//		private var _titleTransition:Tween;
//		private var _descriptionTransition:Tween;
//		private var _dateTransition:Tween;
//		private var _expandTransition:Tween;
		private var _thumbTransition:Tween;		
		private var _fadeTween:Tween;
		
		//LOADING VARS
		private var _currentIndex:int = 0;
		private var _isLoading:Boolean = false;
		private var _isTweening:Boolean = false;
		
		//IMAGE VARS
		private var _nextImageTexture:Texture;
		private var _xPos:int = TricoNavigator.WIDTH / 2;
		private var _yPos:int = TricoNavigator.HEIGHT / 2;
		
		
		private var _loadingSprite:HouseBuildSprite;
		
		private var _selectedImage:Image;
		private var _originalImageWidth:Number;
		private var _originalImageHeight:Number;
				
		private var _bg:Quad;
		
		//TEXT VARS
//		private var _title:TextField;
//		private var _description:TextField;
//		private var _date:TextField;
		
		//BUTTONS
		private var _isFullScreen:Boolean = false
		private var _fullScreenBTN:Button;
		private var _doneBTN:Button;
		
		//LIST FEATHERS CONTROL
		private var _thumbHolder:List;
		
		//LOADER
		private var _loader:Loader;
		
		//Gesture controls
		private var _swipe:SwipeGesture;
		private var _tap:TapGesture;
		private var _transform:TransformGesture
		
		
		private var _albumList:Vector.<AlbumItem>;
		
		
		public function AlbumView()
		{
			super();
		}
		
		public function set albumList(value:Vector.<AlbumItem>):void
		{
			_albumList = value;
		}

		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
			
/*			// title
			_title = new TextField(1200, 60, "HELLO World!", TricoTheme.FONT_HELL_75Bold_38, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			_title.x = 50;
			_title.y = 50;
			
			addChild(_title);
			
			// description
			_description = new TextField(730, 70, "Description", TricoTheme.FONT_HELL_55Roman_18, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);
			_description.vAlign = VAlign.TOP;
			_description.hAlign = HAlign.LEFT;
			_description.x = DESCRIPTION_POSITION_X;
			_description.y = DESCRIPTION_POSITION_Y;
			
			addChild(_description);
			
			// date
			_date = new TextField(200, 70, "00/00/2013", TricoTheme.FONT_HELL_55Roman_15, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_SIDENAV_TEXT);
			_date.vAlign = VAlign.TOP;
			_date.hAlign = HAlign.RIGHT;
			_date.x = DATE_POSITION_X
			_date.y = DATE_POSITION_Y;
			
			addChild(_date);*/
			
			
			//TODO: preload next 2 images
			
			_loadingSprite = new HouseBuildSprite();
			_loadingSprite.x = this.stage.stageWidth / 2;
			_loadingSprite.y = 300;
			addChild(_loadingSprite);
			
			
			const listLayout:HorizontalLayout = new HorizontalLayout();
			listLayout.paddingTop = 5;
			listLayout.paddingLeft = 20;
			listLayout.paddingRight = 20;
			listLayout.gap = 25;
			listLayout.verticalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
			listLayout.hasVariableItemDimensions = true;
			
			_thumbHolder = new List();
			_thumbHolder.layout = listLayout;
			_thumbHolder.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_FLOAT;
			_thumbHolder.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_thumbHolder.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_thumbHolder.scrollerProperties.snapScrollPositionsToPixels = true;
			
			_thumbHolder.itemRendererType = AlbumThumbRenderer;
			_thumbHolder.addEventListener(starling.events.Event.CHANGE, list_changeHandler);
			addChild(_thumbHolder);
			
			
//			_thumbHolder.dataProvider = new ListCollection(items);
//			_thumbHolder.selectedIndex = _currentIndex;
			
			/*_fullScreenBTN = new Button();
			_fullScreenBTN.nameList.add("topMenu");
			_fullScreenBTN.label = "ENHANCE!";
			_fullScreenBTN.addEventListener(starling.events.Event.TRIGGERED, fullScreenOpen);
			_fullScreenBTN.y = 526;
			_fullScreenBTN.x = 990;
			
			addChild(_fullScreenBTN);*/
			
			_doneBTN = new Button();
			_doneBTN.nameList.add("fullScreenDone");
			_doneBTN.label = "DONE";
			_doneBTN.addEventListener(starling.events.Event.TRIGGERED, fullScreenClose);
			_doneBTN.x = 1190;
			_doneBTN.y = -40;
			
			addChild(_doneBTN);
			
			if(_albumList != null){
				loadAlbum();
			}
			
		}
			
		public function loadAlbum():void
		{	
			if(_albumList.length >0){
				_thumbHolder.dataProvider = new ListCollection(_albumList);
				_thumbHolder.selectedIndex = _currentIndex;
				
				this.invalidate(INVALIDATION_FLAG_LAYOUT);
			}
		}
		
		private function layout():void
		{
			if(_selectedImage)
			{
				var scale:Number = getScale();
								
				_fadeTween = new Tween(_selectedImage, 0.5, Transitions.EASE_OUT);
				_fadeTween.delay = 0.2;
				_fadeTween.fadeTo(1);
				_fadeTween.animate("pivotX", _originalImageWidth/2);
				_fadeTween.animate("pivotY", _originalImageHeight/2);
				_fadeTween.animate("scaleX", scale);
				_fadeTween.animate("scaleY", scale);
				_fadeTween.animate("x", _xPos);
				_fadeTween.animate("y", getYOffset() );
				_fadeTween.onComplete = transitionClean;
				_fadeTween.onCompleteArgs = [_fadeTween];
				
				Starling.juggler.add(_fadeTween);
				
				if(_isFullScreen){
					
					_transform = new TransformGesture(_selectedImage);
					_transform.addEventListener(org.gestouch.events.GestureEvent.GESTURE_BEGAN, onTransform);
					_transform.addEventListener(org.gestouch.events.GestureEvent.GESTURE_CHANGED, onTransform);
					_transform.addEventListener(org.gestouch.events.GestureEvent.GESTURE_ENDED, onTransformEnded);
					
					if(_selectedImage.width <= _originalImageWidth*scale){
						
						_swipe = new SwipeGesture(_selectedImage);
						_swipe.minVelocity = 3;
						_swipe.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture);
												
					}else{
						
						if(_swipe){
							_swipe.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture);
							_swipe = null;
						}
						
					}
					
				}else{
					
					if(_transform){
						_transform.removeEventListener(org.gestouch.events.GestureEvent.GESTURE_BEGAN, onTransform);
						_transform.removeEventListener(org.gestouch.events.GestureEvent.GESTURE_CHANGED, onTransform);
						_transform.removeEventListener(org.gestouch.events.GestureEvent.GESTURE_ENDED, onTransformEnded);
						_transform = null;
					}
					
					/****** Add Gestures ********/
					_swipe = new SwipeGesture(_selectedImage);
					_swipe.minVelocity = 4;
					_swipe.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture);
					
					/****** Add Gestures ********/
					
				}
				
			}
			
		}
		
		protected function onTransformEnded(event:GestureEvent):void
		{
			if(_selectedImage.width <= _originalImageWidth*getScale()){
				
				layout();
				
			}else{
				
				if(_swipe){
					_swipe.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture);
					_swipe = null;
				}
			}
		}
		
		private function onTransform(event:org.gestouch.events.GestureEvent):void
		{
			const gesture:TransformGesture = event.target as TransformGesture;
			var matrix:Matrix = _selectedImage.transformationMatrix;
			
			// Panning
			matrix.translate(gesture.offsetX, gesture.offsetY);
			_selectedImage.transformationMatrix = matrix;
			
//			if (gesture.scale != 1 || gesture.rotation != 0)
//			{
				// Scale and rotation.
				var transformPoint:Point = matrix.transformPoint(_selectedImage.globalToLocal(gesture.location));
				matrix.translate(-transformPoint.x, -transformPoint.y);
//				matrix.rotate(gesture.rotation);
				matrix.scale(gesture.scale, gesture.scale);
				matrix.translate(transformPoint.x, transformPoint.y);
				
				_selectedImage.transformationMatrix = matrix;
//			}
		}
		
		private function getYOffset():int
		{
			var yOffset:int;
			
			if(_isFullScreen){
				yOffset = _yPos + IMAGE_OFFSET_FULLSCREEN;
			}else{
				yOffset = _yPos + IMAGE_OFFSET;
			}
			
			return yOffset;
		}
		
		private function getScale():Number
		{	
			var availableHeight:Number;
			if(_isFullScreen){
				availableHeight = TricoNavigator.HEIGHT;
			}else{
				availableHeight = IMAGE_HEIGHT_SMALL;
			}
			
			var widthScale:Number = TricoNavigator.WIDTH / _originalImageWidth;
			var heightScale:Number = availableHeight / _originalImageHeight;
			var scale:Number = Math.min(widthScale, heightScale);

			return scale;			
		}
		
		override public function dispose():void
		{
			_loadingSprite.dispose();
			
			if(_loader)
			{
				_loader.unloadAndStop(true);
				_loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, loader_completeHandler);
				_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, loader_errorHandler);
				_loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, loader_errorHandler);
			}
			
			super.dispose();
		}
		
		private function list_changeHandler(event:starling.events.Event):void
		{
			if(_currentIndex <= _thumbHolder.selectedIndex){
				_direction = LEFT;
			}else{
				_direction = RIGHT;
			}
			
			_currentIndex = _thumbHolder.selectedIndex;
			
			_thumbHolder.scrollToDisplayIndex(_thumbHolder.selectedIndex, 0.3)
			
			const item:AlbumItem = AlbumItem(_thumbHolder.selectedItem);
			
//			_description.text = item.description;
//			_date.text = item.date;
									
			//If the item doesn't exist
			if(!item)
			{
				if(_selectedImage)
				{
					_selectedImage.visible = false;
				}
				return;
			}
			
			if(_loader)
			{
				_loader.unloadAndStop(true);
			}
			else
			{
				_loader = new Loader();
				_loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, loader_completeHandler);
				_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, loader_errorHandler);
				_loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loader_errorHandler);
			}
			
			_isLoading = true;
			_loadingSprite.load(true);
			
			if(_selectedImage)
			{
				_isTweening = true;
				
				_fadeTween = new Tween(_selectedImage, 0.4, Transitions.EASE_IN_OUT);
				_fadeTween.fadeTo(0);
				_fadeTween.onComplete = fadeOutComplete;
				
				switch(_direction){
					case LEFT:
						_fadeTween.animate("x", _selectedImage.x - IMAGE_FADE_X_DISTANCE);
						break;
					case RIGHT:
						_fadeTween.animate("x", _selectedImage.x + IMAGE_FADE_X_DISTANCE);
						break;
				}
				
				Starling.juggler.add(_fadeTween);
				
			}
			
			_loader.load(new URLRequest(item.imageURL), LOADER_CONTEXT);
			
			
		}
		
		private function fadeOutComplete():void
		{
			_isTweening = false;
			
			checkNextImage();
		}
		
		protected function loader_completeHandler(event:flash.events.Event):void
		{
			_nextImageTexture = Texture.fromBitmap(Bitmap(_loader.content));

			_isLoading = false;
			
			checkNextImage();
		}
		
		/******************************************************************
		 * 
		 *  Check if the ImageLoader is complete, and fading out the last image is complete
		 * 
		 ******************************************************************/
		private function checkNextImage():void
		{
			if(!_isLoading && !_isTweening){
				
				_loadingSprite.reset();
//				_loadingSprite.load(false);
				
				if(_selectedImage)
				{
					_selectedImage.texture.dispose();
					_selectedImage.texture = _nextImageTexture;
					_selectedImage.scaleX = _selectedImage.scaleY = 1;
					_selectedImage.readjustSize();
				}
				else
				{
					_selectedImage = new Image(_nextImageTexture);
					
					//Tap gesture
					_tap = new TapGesture(_selectedImage);
					_tap.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
				
					this.addChild(_selectedImage);
										
					//put the done button above the new image
					this.setChildIndex(_doneBTN, this.numChildren);
				}
				
				_originalImageHeight = _selectedImage.height;
				_originalImageWidth = _selectedImage.width;
				_selectedImage.pivotX = _originalImageWidth/2;
				_selectedImage.pivotY = _originalImageHeight/2;
								
				//Hide image
				_selectedImage.alpha = 0;
				_selectedImage.visible = true;
				
				//Set scale
				_selectedImage.scaleX = _selectedImage.scaleY = getScale();
								
				//Set y Position
				_selectedImage.y = getYOffset();
				
				switch(_direction){
				
					case LEFT:
						_selectedImage.x = _xPos + IMAGE_FADE_X_DISTANCE;
						break;
					
					case RIGHT:
						_selectedImage.x = _xPos - IMAGE_FADE_X_DISTANCE;
						break;
					
				}
				
				layout();
				
			}
		
		}
		
		protected function onTap(event:GestureEvent):void
		{
//			const gesture:TapGesture = event.target as TapGesture;
			if(_isFullScreen){
				fullScreenClose();
			}else{
				fullScreenOpen();
			}
		}
		
		protected function onGesture(event:GestureEvent):void
		{
			const gesture:SwipeGesture = event.target as SwipeGesture;
			
			trace("direction! > " + gesture.direction);
			trace("velocity > " + gesture.minOffset);
			trace("swiped! > " + gesture.offsetX);
			
//			if(Math.abs(gesture.offsetX) > 20){
			
				if(gesture.offsetX > 0){
					
					if(_thumbHolder.selectedIndex != 0){
						
						_thumbHolder.selectedIndex--;
						
					}else{
						
						//TODO: shake
						
					}
					
				}else{
					
					if(_thumbHolder.selectedIndex < _thumbHolder.dataProvider.length - 1){
						
						_thumbHolder.selectedIndex++;
						
					}else{
						
						//TODO: shake
						
					}
					
				}
				
//			}
			
		}
		
		protected function loader_errorHandler(event:flash.events.Event):void
		{
//			message.text = "Error loading image.";
			this.layout();
		}
		
		private function fullScreenOpen(e:starling.events.Event=null):void
		{
			_isFullScreen = true;
			
//			_fullScreenBTN.alpha = 0;
//			_fullScreenBTN.visible = false;
			
			transitionMaker(_fullScreenTransition, _bg, 0.3, 0, [{attr:"alpha", amount:0}]);
//			transitionMaker(_titleTransition, _title, 0.4, 0, [{attr:"y", amount:-_title.height - 50}], Transitions.EASE_IN);
//			transitionMaker(_descriptionTransition, _description, 0.4, 0, [{attr:"y", amount:FULLSCREEN_Y}], Transitions.EASE_IN_OUT);
//			transitionMaker(_dateTransition, _date, 0.4, 0, [{attr:"y", amount:FULLSCREEN_Y}], Transitions.EASE_IN_OUT);
			transitionMaker(_thumbTransition, _thumbHolder, 0.4, 0, [{attr:"y", amount: TricoNavigator.HEIGHT}], Transitions.EASE_IN_OUT);
			
			this.layout();
			
			dispatchEvent(new TricoEvent(TricoEvent.HIDE_MENU));
		}
		
		private function fullScreenClose(e:starling.events.Event=null):void
		{
			_isFullScreen = false;
			
//			_fullScreenBTN.visible = true;
//			
//			transitionMaker(_expandTransition, _fullScreenBTN, 0.3, 0.3, [{attr:"alpha", amount:1}]);
			transitionMaker(_fullScreenTransition, _bg, 0.3, 0.3, [{attr:"alpha", amount:1}]);
//			transitionMaker(_titleTransition, _title, 0.4, 0.3, [{attr:"y", amount:50}], Transitions.EASE_OUT);
//			transitionMaker(_descriptionTransition, _description, 0.4, 0, [{attr:"y", amount:DESCRIPTION_POSITION_Y}], Transitions.EASE_IN);
//			transitionMaker(_dateTransition, _date, 0.4, 0, [{attr:"y", amount:DATE_POSITION_Y}], Transitions.EASE_IN);
			transitionMaker(_thumbTransition, _thumbHolder, 0.4, 0, [{attr:"y", amount:THUMB_HOLDER_POSITION_Y}], Transitions.EASE_IN);
			
			this.layout();
			
			dispatchEvent(new TricoEvent(TricoEvent.SHOW_MENU));
			
		}
		
		private function transitionMaker(t:Tween, obj:DisplayObject, time:Number, delay:Number, animateArray:Array, ease:String=Transitions.LINEAR):void
		{
			t = new Tween(obj, time, ease);
			
			t.delay = delay;
			
			for (var i:int = 0; i < animateArray.length; i++) 
			{
				t.animate(animateArray[0].attr, animateArray[0].amount);
			}
			
			t.onComplete = transitionClean;
			t.onCompleteArgs = [t];
			
			Starling.juggler.add(t);
			
		}
		
		private function transitionClean(t:Tween):void
		{
			Starling.juggler.remove(t);
			t = null;
		}
		
		override protected function draw():void
		{
			_thumbHolder.width = TricoNavigator.WIDTH;
			_thumbHolder.height = THUMB_HOLDER_HEIGHT;
			_thumbHolder.y = THUMB_HOLDER_POSITION_Y;
		}
	}
}