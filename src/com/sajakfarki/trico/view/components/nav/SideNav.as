package com.sajakfarki.trico.view.components.nav
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Dictionary;
	
	import feathers.controls.Button;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.display.Scale3Image;
	import feathers.textures.Scale3Textures;
	
	import starling.events.Event;
	
	public class SideNav extends FeathersControl
	{
		
		public static var WIDTH:int = 325;
		
		private var _sprites:Array;
		private var _linkDictionary:Dictionary;
		private var _scrollContainer:ScrollContainer;
		private var _dropShadow:Scale3Image;
		
		private var _buttonDictionary:Dictionary;
		
		private var _selected:String;
		
		
		public function SideNav(selected:String)
		{
			super();
			_selected = selected;
		}
		
		override protected function initialize():void
		{
			_sprites = new Array();
			
			_linkDictionary = new Dictionary();
			_buttonDictionary = new Dictionary();
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			addChild(_scrollContainer);
						
			addButton("sideNavLink", "HOME", 50, TricoEvent.HOME, menuListener);
			addDiv();
			addButton("sideChecklist", "ACCENTS DESIGN STUDIO\nINTERIOR CHECKLIST", 81, TricoEvent.CHECKLIST, menuListener);
			addDiv();
			addButton("sideOffers", "PARTNERS AND OFFERS", 81, TricoEvent.OFFERS, menuListener);
			addDiv();
			addButton("sideAlbum", "MY HOME PHOTO ALBUM", 81, TricoEvent.ALBUM, menuListener); 
			addDiv();
			
			addButton("sideNavHeader1", "Your Community", 50);
			addDiv();
			
			addButton("sideNavLink", "EXPLORE YOUR COMMUNITY", 50, TricoEvent.COMMUNITY, menuListener); 
			addDiv();
			addButton("sideNavLink", "TRICO CENTRE", 50, TricoEvent.TRICO_CENTRE, menuListener);
			addDiv();
//			addButton("sideNavLink", "GLOBAL FEST", 50, TricoEvent.GLOBAL_FEST, menuListener);
//			addDiv();
			
			addButton("sideNavHeader1", "Trico VIP Benefits", 50);
			addDiv();
			
			addButton("sideNavLink", "EQUITY SAVER", 50, TricoEvent.EQUITY_SAVER, menuListener);
			addDiv();
			addButton("sideNavLink", "REFERAL PROGRAM", 50, TricoEvent.REFERRAL, menuListener);
			addDiv();
			addButton("sideNavLink", "MOVE IN MADE EASY", 50, TricoEvent.SERVICE, menuListener);
			addDiv();
			
			addButton("sideNavHeader2", "Guides & Manuals", 50); 
			addDiv();
			
//			addButton("sideNavPDF", "THE BUILDING EXPERIENCE MANUAL", 50, "http://dev.tricohomes.com/customer_app/customer_manual/TC_10089_ExperienceLR.pdf", linkListener);
//			addDiv();
//			addButton("sideNavPDF", "INTERIOR SELECTIONS GUIDE", 50, "http://dev.tricohomes.com/customer_app/quick_reference/TC_10503_Building_Guide_LR.pdf", linkListener);
//			addDiv();
//			addButton("sideNavPDF", "CUSTOMER PORTAL USER MANUAL", 50, "http://dev.tricohomes.com/customer_app/customer_portal_manual/TC_10733_Cust_Portal.pdf", linkListener);
			
			addButton("sideNavPDF", "THE BUILDING EXPERIENCE MANUAL", 50, TricoEvent.BUILDING_EXPERIENCE, menuListener);
			addDiv();
			addButton("sideNavPDF", "INTERIOR SELECTIONS GUIDE", 50, TricoEvent.INTERIOR_SELECTION, menuListener);
			addDiv();
			addButton("sideNavPDF", "CUSTOMER PORTAL USER MANUAL", 50, TricoEvent.CUSTOMER_PORTAL, menuListener);
			addDiv();
			addButton("sideNavLogout", "LOGOUT", 50, TricoEvent.LOGOUT, menuListener);
			
			//disable nav _selected section
			disable(_selected);
			
			_dropShadow.x = this.width - _dropShadow.width;
			_dropShadow.height = this.height;
			addChild(_dropShadow);
		}
		
		private function disable(s:String):void
		{
			switch(s){
				
				case TricoEvent.CHECKLIST:
				case TricoEvent.OFFERS:
				case TricoEvent.ALBUM:
					
					(_buttonDictionary[s] as Button).isEnabled = false;
					(_buttonDictionary[s] as Button).isSelected = true;
					
					break;
				
			}
			
		}
	
		
		public function addShadow(texture:Scale3Textures):void
		{
			_dropShadow = new Scale3Image(texture);
		}
		
		private function addDiv():void
		{
			var div:SideNavDivider = new SideNavDivider(this.width, TricoTheme.COLOR_SIDENAV_DIVIDER, TricoTheme.COLOR_SIDENAV_HEADER);
			_scrollContainer.addChild(div);
			_sprites.push(div);
		}
		
		private function addButton(nameList:String, label:String, height:int, url:String="", func:Function=null):void
		{
			var button:Button = new Button();
			button = new Button();
			button.isToggle = true;
			button.nameList.add(nameList);
			button.label = label;
			button.width = this.width;
			button.height = height;
			
			if(func != null){
				_buttonDictionary[url] = button;				
				_linkDictionary[button] = url
				button.addEventListener(Event.TRIGGERED, func);
			}
			
			_scrollContainer.addChild(button);
			_sprites.push(button);
			
		}
		
		private function menuListener(e:Event):void
		{
			this.dispatchEventWith(_linkDictionary[e.target]);
		}
		
		private function pdfListener(e:Event):void
		{
			
		}
		
		private function linkListener(e:Event):void
		{
			//TC_10733_Cust_Portal.pdf
			
			
			//var f:File = File.applicationDirectory.resolvePath("TC_10733_Cust_Portal.pdf");
//			trace(File.applicationDirectory.resolvePath("TC_10733_Cust_Portal.pdf").url);
//			
//			var f:File = File.applicationDirectory.resolvePath("TC_10733_Cust_Portal.pdf");
//			trace(f.exists);
//			trace(f.type);
//			trace(f.
//			var tempFile:File = new File(File.applicationStorageDirectory().url);
			
//			var request:URLRequest = new URLRequest(f.url);
//			try {
//				navigateToURL(request, '_blank');
//			} catch (e:Error) {
//				trace("Error occurred!");
//			}
			

//			f.copyTo(tempFile);
//			tempFile.openWithDefaultApplication();
			//f.openWithDefaultApplication();
			
//			var request:URLRequest = new URLRequest(_linkDictionary[e.target]);
//			try {
//				navigateToURL(request, '_blank');
//			} catch (e:Error) {
//				trace("Error occurred!");
//			}
		}
		
		override protected function draw():void
		{
			_scrollContainer.width = this.width;
			_scrollContainer.height = this.height;
			
			for (var i:int = 1; i < _sprites.length; i++) 
			{
				if(_sprites[i-1] is Button) _sprites[i-1].validate();
				_sprites[i].y = _sprites[i-1].y + _sprites[i-1].height;
			}
			
			_scrollContainer.validate();
			
		}
		
	}
	
}