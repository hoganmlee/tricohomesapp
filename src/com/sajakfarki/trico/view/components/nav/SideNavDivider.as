package com.sajakfarki.trico.view.components.nav
{
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class SideNavDivider extends Sprite
	{
		private var _div1:Quad;
		private var _div2:Quad;
		
		public function SideNavDivider(width:int, colorTop:int, colorBottom:int)
		{
			super();
			
			_div1 = new Quad(width, 1, colorTop);
			addChild(_div1);
			
			_div2 = new Quad(width, 1, colorBottom);
			_div2.y = 1;
			addChild(_div2);
			
		}
		
	}
}