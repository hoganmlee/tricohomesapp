package com.sajakfarki.trico.view.components.nav
{
	import com.sajakfarki.trico.signal.events.TricoEvent;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.display.Scale3Image;
	import feathers.textures.Scale3Textures;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class TopNav extends FeathersControl
	{
		public static var BG_COLOR:int = 0x494944;
		
		public static var HEIGHT:int  = 50;
		public static var PADDING:int = 24;
		public static var MENU_OFFSET:int = 54
		
		private var _bgQuad:Quad;
		
		private var _tricoLogo:Button;
		
//		private var _serviceButton:Button;
		private var _portalButton:Button;
		private var _contactButton:Button;
		
		private var _positionLogo:int;
		private var _positionButtonService:int;
		private var _positionButtonPortal:int;
		private var _positionButtonContact:int;
		
		private var _callMenu:Button;
		
		private var _isOpen:Boolean = false;
		
		private var _transitionMenuButton:Tween;
		private var _transitionLogo:Tween;
		private var _transitionButtonService:Tween;
		private var _transitionButtonPortal:Tween;
		private var _transitionButtonContact:Tween;
		
		private var _duration:Number = 0.4;
		private var _ease:Object = Transitions.EASE_IN_OUT;
		
		private var _dropShadow:Scale3Image;
		
		private var _isMenuButtonShowing:Boolean = false;
		
		private var _hitspot:Button;
		
		public function TopNav()
		{
			super();
		}
		
		public function addShadow(bottomShadow:Scale3Textures):void
		{
			_dropShadow = new Scale3Image(bottomShadow);
		}
		
		public function hideMenuButton():void
		{
			if(_isMenuButtonShowing){
				_transitionMenuButton = new Tween(_callMenu, _duration, _ease);
				_transitionMenuButton.animate("y", -50);
				Starling.juggler.add(_transitionMenuButton);
				_isMenuButtonShowing = false;
			}
		}
		
		public function showMenuButton():void
		{
			if(!_isMenuButtonShowing){
				_transitionMenuButton = new Tween(_callMenu, _duration, _ease);
				_transitionMenuButton.delay = 0.6;
				_transitionMenuButton.animate("y", HEIGHT/2 - _callMenu.height/2);
				Starling.juggler.add(_transitionMenuButton);
				_isMenuButtonShowing = true;
			}
		}
		
		public function set isOpen(b:Boolean):void
		{
			_isOpen = b;
			
			var pos:int = 0; 
			
			if(_isOpen){
				pos = SideNav.WIDTH;
			}	
			
			_transitionLogo = new Tween(_tricoLogo, _duration, _ease);
			_transitionLogo.animate("x", _positionLogo - pos);
			Starling.juggler.add(_transitionLogo);
			
			/*_transitionButtonService = new Tween(_serviceButton, _duration, _ease);
//			_transitionButtonService.delay = 0.05;
			_transitionButtonService.animate("x", _positionButtonService - pos);
			Starling.juggler.add(_transitionButtonService);*/
			
			_transitionButtonPortal= new Tween(_portalButton, _duration, _ease);
//			_transitionButtonPortal.delay = 0.05;
			_transitionButtonPortal.animate("x", _positionButtonPortal - pos);
			Starling.juggler.add(_transitionButtonPortal);
			
			_transitionButtonContact = new Tween(_contactButton, _duration, _ease);
//			_transitionButtonContact.delay = 0.05;
			_transitionButtonContact.animate("x", _positionButtonContact - pos);
			Starling.juggler.add(_transitionButtonContact);
			
		}
				
		public function get isOpen():Boolean
		{
			return _isOpen;
		}
		
		override protected function initialize():void
		{
			_bgQuad = new Quad(stage.stageWidth, HEIGHT, BG_COLOR);
			addChild(_bgQuad);
			
			_dropShadow.height = 13;
			_dropShadow.width = this.width
			_dropShadow.y = HEIGHT - _dropShadow.height;
			addChild(_dropShadow);
			
			_tricoLogo = new Button();
			_tricoLogo.nameList.add("logo3");
			//_tricoLogo.addEventListener(Event.TRIGGERED, goHome);
			addChild(_tricoLogo);
			
			_hitspot = new Button();
			_hitspot.width = 200;
			_hitspot.height = 55;
			_hitspot.addEventListener(Event.TRIGGERED, goHome);
			addChild(_hitspot);
			
//			_serviceButton = new Button();
//			_serviceButton.nameList.add("topMenu");
//			_serviceButton.label = "SERVICE";
//			_serviceButton.addEventListener(Event.TRIGGERED, callServiceListener);
//			addChild(_serviceButton);
			
			_portalButton = new Button();
			_portalButton.nameList.add("topMenu");
			_portalButton.label = "CUSTOMER PORTAL";
			_portalButton.addEventListener(Event.TRIGGERED, callCustomerPortal);
			addChild(_portalButton);
			
			_contactButton = new Button();
			_contactButton.nameList.add("topMenu");
			_contactButton.label = "CONTACTS";
			_contactButton.addEventListener(Event.TRIGGERED, callContact);
			addChild(_contactButton);
			
			_callMenu = new Button();
			_callMenu.nameList.add("callMenu");
			_callMenu.label = "MENU";
			_callMenu.addEventListener(Event.TRIGGERED, callMenuListener);
//			_callMenu.y = -50;
			addChild(_callMenu);
			
		}
		
		private function callCustomerPortal(e:Event):void
		{
			var request:URLRequest = new URLRequest("http://portal.tricohomes.com/portal/customer");
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
		}
		
		private function callContact(e:Event):void
		{
			var request:URLRequest = new URLRequest("http://www.tricohomes.com/contact-trico.bpsx");
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
		}
		
		/*private function callServiceListener(e:Event):void
		{
			dispatchEventWith(TricoEvent.SERVICE);
		}*/
		
		private function goHome(e:Event):void
		{
			dispatchEventWith(TricoEvent.HOME);
		}
		
		private function callMenuListener(e:Event):void
		{
			dispatchEventWith(TricoEvent.MENU);
		}
		
		override protected function draw():void
		{
			_callMenu.validate();
			_callMenu.x = MENU_OFFSET;
			_callMenu.y = HEIGHT/2 - _callMenu.height/2;
			
			_tricoLogo.validate();
			_positionLogo = stage.stageWidth/2 - _tricoLogo.width/2;
			_tricoLogo.x = _positionLogo;
			_tricoLogo.y = HEIGHT/2 - _tricoLogo.height/2;
			
			_contactButton.validate();
			_positionButtonContact = stage.stageWidth - _contactButton.width - MENU_OFFSET; 
			_contactButton.x = _positionButtonContact
			_contactButton.y = HEIGHT/2 - _contactButton.height/2;
			
			_portalButton.validate();
			_positionButtonPortal = _contactButton.x - _portalButton.width - PADDING; 
			_portalButton.x = _positionButtonPortal
			_portalButton.y = HEIGHT/2 - _portalButton.height/2;
			
			_hitspot.validate();
			_hitspot.x = _tricoLogo.x - 40
			
			/*_serviceButton.validate();
			_positionButtonService = _portalButton.x - _serviceButton.width - PADDING;
			_serviceButton.x = _positionButtonService
			_serviceButton.y = HEIGHT/2 - _serviceButton.height/2;*/
			
		}
		
	}
}