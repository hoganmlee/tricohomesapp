package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.feathersextended.controls.GroupListMultiSelect;
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.CheckList;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.data.HierarchicalCollection;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ChecklistView extends FeathersControl
	{
		//groupID, itemID, selected
		public var checklistUpdate:Signal = new Signal(int, int, Boolean);
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		
		private var _list:GroupListMultiSelect;
		
		private var _bg:Quad;
		private var _adSpace:AdSpace;
		private var _title:TextField;
		private var _divider:Quad;
		private var _counter:Button;
		
		private var _count:int;
		private var _total:int;
		
		private var _checklist:CheckList;
		private var _checklistArray:Array;
				
		public function ChecklistView()
		{
			super();	
		}

		public function get checklist():CheckList
		{
			return _checklist;
		}

		public function set checklist(value:CheckList):void
		{
			_checklist = value;
		}

		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			this.addChild(_bg);
			
			
			_list = new GroupListMultiSelect();
			_list.nameList.add("checklistGroupedList");
			
			
			_list.typicalItem = { text: "Item 1000" };
			_list.typicalHeader = "Group 10";
			_list.typicalFooter = "Footer 10";
			_list.scrollerProperties.hasElasticEdges = true;
			_list.itemRendererProperties.labelField = "text";
			
			_list.addEventListener(Event.CHANGE, list_changeHandler);
			addChild(_list);
			
			
			_adSpace = new AdSpace(475, 662);
			this.addChild(_adSpace);
			
			
			//_adSpace.getAd("http://saf.sajakfarki.com/poopower/trico/ad3.png",  true);
			
			
			_title = new TextField(420, 100, "Accents Design Studio\nInterior Checklist", TricoTheme.FONT_HELL_75Bold_38, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			this.addChild(_title);
			
			
			_counter = new Button();
			_counter.nameList.add("accentCounter");
			_counter.isEnabled = false;
			_counter.label = "relax, it's loading your checklist...";
			this.addChild(_counter);
			
			
			_divider = new Quad(675, 1, TricoTheme.COLOR_DIVIDER);
			this.addChild(_divider);
			
			
			if(_checklist != null){
				loadChecklist();
			}
			
			selectAd.dispatch(DisplayAd.TYPE_AD_3);
			
		}
		
		
		/***************************************************************
		 * 
		 *   Advertisment listeners
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("ChecklistView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_3);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_3);
			
		}
		
		/***************************************************************
		 * 
		 *  Load CheckList into HierarchicalCollection
		 * 
		 ***************************************************************/
		public function loadChecklist():void
		{
			
			_checklistArray = getChecklistArray();
			_checklistArray.fixed = true;
			
			_list.dataProvider = new HierarchicalCollection(_checklistArray);			
			
			/** GET COUNT AND TOTAL *********************/
			_count = 0;
			_total = 0;
			
			for (var i:int = 0; i < _checklistArray.length; i++) 
			{
				for (var j:int = 0; j < _checklistArray[i].children.length; j++) 
				{
					_total++;
					
					if(_checklistArray[i].children[j].selected) _count++;
				}
			}
			
			_counter.label = _count+ " / " + _total;
			
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			
		}
		
		/***************************************************************
		 * 
		 * 	Get Checklist Array from CheckList Class
		 * 
		 ***************************************************************/
		private function getChecklistArray():Array
		{
			_checklistArray = new Array();
			
			for (var i:int = 0; i < _checklist.groups.length; i++) 
			{
				var obj:Object = new Object();
				obj.header = _checklist.groups[i].groupName
				obj.children = new Array();
				
				for (var j:int = 0; j < _checklist.groups[i].items.length; j++) 
				{
					var item:Object = new Object();
					item.text = _checklist.groups[i].items[j].itemName;
					item.selected = _checklist.groups[i].items[j].checked;
					item.groupID = _checklist.groups[i].items[j].groupID;
					item.itemID = _checklist.groups[i].items[j].itemID;
					
					obj.children.push(item);
					
				}
				
				_checklistArray.push(obj);
				
			}
			
			return _checklistArray;
		}
		
		/******************************************/
		
		private function list_changeHandler(e:Event):void
		{
			trace(">>>> ChecklistScreen :: list_changeHandler > " + e);
			
			if(_list.selectedItem)
			{
//				trace(_list.selectedGroupIndex);
//				trace(_list.selectedItemIndex);
//				trace(_list.selectedItem.groupID);
//				trace(_list.selectedItem.itemID);
//				trace(_list.selectedItem.selected)
//				trace(_list.selectedItem.text);
//				trace(_list.dataProvider.data);
			
				if(_list.selectedItem.selected){
					_count++;
				}else{
					_count--;
				}
				
				_counter.label = _count+ " / " + _total;
				
				
				//checklist signal dispatch
				checklistUpdate.dispatch(_list.selectedItem.groupID, _list.selectedItem.itemID, _list.selectedItem.selected)
				
			}
			
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
		}
		
		override protected function draw():void
		{			
			_adSpace.x = 750;
			_adSpace.y = 20;
			
			_title.x = 50;
			_title.y = 50;
			
			_counter.validate();
			_counter.x = 725 - _counter.width;
			_counter.y = 50;
			
			_divider.x = 50;
			_divider.y = 155;
			
			_list.x = 50;
			_list.y = 156;
			_list.width = 675
			_list.height = 546
			_list.validate();
			
		}
		
	}
	
}