package com.sajakfarki.trico.view.components.renderer
{
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.List;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
		
	import starling.animation.Tween;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import com.sajakfarki.trico.model.vo.AlbumItem;
		
	public class AlbumThumbRenderer extends FeathersControl implements IListItemRenderer
	{
		/**
		 * @private
		 */
		private static const HELPER_POINT:Point = new Point();
		
		/**
		 * @private
		 */
		private static const HELPER_TOUCHES_VECTOR:Vector.<Touch> = new <Touch>[];
		
		/**
		 * @private
		 * This will only work in a single list. If this item renderer needs to
		 * be used by multiple lists, this data should be stored differently.
		 */
		private static const CACHED_BOUNDS:Dictionary = new Dictionary(false);
		
//		private var swipe:SwipeGesture;
		
		/**
		 * Constructor.
		 */
		public function AlbumThumbRenderer()
		{
			this.isQuickHitAreaEnabled = true;
			this.addEventListener(TouchEvent.TOUCH, touchHandler);
			this.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
		}
				
		/**
		 * @private
		 */
		protected var _imageLoader:ImageLoader;
		protected var _holderBG:Quad;
		protected var _selectedBG:Quad;
		
		/**
		 * @private
		 */
		protected var touchPointID:int = -1;
		
		/**
		 * @private
		 */
		protected var fadeTween:Tween;
		
		/**
		 * @private
		 */
		private var _index:int = -1;
		
		/**
		 * @inheritDoc
		 */
		public function get index():int
		{
			return this._index;
		}
		
		/**
		 * @private
		 */
		public function set index(value:int):void
		{
			if(this._index == value)
			{
				return;
			}
			this._index = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		/**
		 * @private
		 */
		protected var _owner:List;
		
		/**
		 * @inheritDoc
		 */
		public function get owner():List
		{
			return List(this._owner);
		}
		
		/**
		 * @private
		 */
		public function set owner(value:List):void
		{
			if(this._owner == value)
			{
				return;
			}
			if(this._owner)
			{
				this._owner.removeEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this._owner = value;
			if(this._owner)
			{
				this._owner.addEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		/**
		 * @private
		 */
		private var _data:AlbumItem;
		
		/**
		 * @inheritDoc
		 */
		public function get data():Object
		{
			return this._data;
		}
		
		/**
		 * @private
		 */
		public function set data(value:Object):void
		{
			if(this._data == value)
			{
				return;
			}
			this.touchPointID = -1;
			this._data = AlbumItem(value);
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		/**
		 * @private
		 */
		private var _isSelected:Boolean;
				
		/**
		 * @inheritDoc
		 */
		public function get isSelected():Boolean
		{
			return this._isSelected;
		}
		
		/**
		 * @private
		 */
		public function set isSelected(value:Boolean):void
		{
			if(this._isSelected == value)
			{
				return;
			}
			
			this._isSelected = value;
			this.dispatchEventWith(Event.CHANGE);
			
			if(this._isSelected){
				
				_holderBG.visible = true;
				_selectedBG.visible = true;
				
			}else{
				
				_holderBG.visible = false;
				_selectedBG.visible = false;
			}
			
		}
		
		/**
		 * @private
		 */
		override protected function initialize():void
		{
			_selectedBG = new Quad(10, 10, 0x97978E);
			_selectedBG.x = -5;
			_selectedBG.y = -5;
			_selectedBG.visible = false;
			this.addChild(_selectedBG);
			
			_holderBG = new Quad(10, 10, 0xEFEFEF);
			_holderBG.x = -3;
			_holderBG.y = -3;
			this.addChild(_holderBG);
			
			this._imageLoader = new ImageLoader();
			this._imageLoader.addEventListener(Event.COMPLETE, image_completeHandler);
			this._imageLoader.addEventListener(FeathersEventType.ERROR, image_errorHandler);
			this.addChild(this._imageLoader);
			
		}
		
		/**
		 * @private
		 */
		override protected function draw():void
		{
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const selectionInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SELECTED);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(selectionInvalid){
				
				if(this.isSelected){
					
					_selectedBG.visible = true;
					_holderBG.visible = true;
					
				}else{
					
					_selectedBG.visible = false;
					_holderBG.visible = false;
					
				}
			}
			
			if(dataInvalid)
			{
				if(this.fadeTween)
				{
					this.fadeTween.advanceTime(Number.MAX_VALUE);
				}
				if(this._data)
				{
//					this.image.visible = false;
					_holderBG.visible = true;
					this._imageLoader.source = this._data.thumbURL;
				}
				else
				{
					this._imageLoader.source = null;
				}
			}
			
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			
			if(sizeInvalid)
			{
				this._imageLoader.width = this.actualWidth;
				this._holderBG.width = this.actualWidth + 6;
				this._selectedBG.width = this.actualWidth + 10;

				this._imageLoader.height = this.actualHeight;
				this._holderBG.height = this.actualHeight + 6;
				this._selectedBG.height = this.actualHeight + 10;
			}
		}
		
		/**
		 * @private
		 */
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			
			this._imageLoader.width = this._imageLoader.height = NaN;
			this._imageLoader.validate();
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._imageLoader.isLoaded)
				{
					if(!CACHED_BOUNDS.hasOwnProperty(this._index))
					{
						CACHED_BOUNDS[this._index] = new Point();
					}
					var boundsFromCache:Point = Point(CACHED_BOUNDS[this._index]);
					//also save it to a cache so that we can reuse the width and
					//height values later if the same image needs to be loaded
					//again.
					newWidth = boundsFromCache.x = this._imageLoader.width;
				}
				else
				{
					if(CACHED_BOUNDS.hasOwnProperty(this._index))
					{
						//if the image isn't loaded yet, but we've loaded it at
						//least once before, we can use a cached value to avoid
						//jittering when the image resizes
						boundsFromCache = Point(CACHED_BOUNDS[this._index]);
						newWidth = boundsFromCache.x;
					}
					else
					{
						//default to 100 if we've never displayed an image for
						//this index yet.
						newWidth = 96;
					}
					
				}
								
			}
			var newHeight:Number = this.explicitHeight;
			if(needsHeight)
			{
				if(this._imageLoader.isLoaded)
				{
					if(!CACHED_BOUNDS.hasOwnProperty(this._index))
					{
						CACHED_BOUNDS[this._index] = new Point();
					}
					boundsFromCache = Point(CACHED_BOUNDS[this._index]);
					newHeight = boundsFromCache.y = this._imageLoader.height;
				}
				else
				{
					if(CACHED_BOUNDS.hasOwnProperty(this._index))
					{
						boundsFromCache = Point(CACHED_BOUNDS[this._index]);
						newHeight = boundsFromCache.y;
					}
					else
					{
						newHeight = 96;
					}
				}
				
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		
		/**
		 * @private
		 */
		protected function fadeTween_onComplete():void
		{
			this.fadeTween = null;
		}
		
		/**
		 * @private
		 */
		protected function removedFromStageHandler(event:Event):void
		{
			this.touchPointID = -1;
		}
		
		/**
		 * @private
		 */
		protected function touchHandler(event:TouchEvent):void
		{
			const touches:Vector.<Touch> = event.getTouches(this, null, HELPER_TOUCHES_VECTOR);
			if(touches.length == 0)
			{
				return;
			}
			if(this.touchPointID >= 0)
			{
				var touch:Touch;
				for each(var currentTouch:Touch in touches)
				{
					if(currentTouch.id == this.touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				if(touch.phase == TouchPhase.ENDED)
				{
					this.touchPointID = -1;
					
					touch.getLocation(this, HELPER_POINT);
					if(this.hitTest(HELPER_POINT, true) != null && !this._isSelected)
					{
						this.isSelected = true;
					}
				}
			}
			else
			{
				for each(touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						this.touchPointID = touch.id;
						break;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}
		
		/**
		 * @private
		 */
		protected function owner_scrollHandler(event:Event):void
		{
			this.touchPointID = -1;
		}
		
		/**
		 * @private
		 */
		protected function image_completeHandler(event:Event):void
		{
//			this.image.alpha = 0;
//			this.image.visible = true;
//			this.fadeTween = new Tween(this.image, 0.25, Transitions.EASE_OUT);
//			this.fadeTween.fadeTo(1);
//			this.fadeTween.onComplete = fadeTween_onComplete;
//			Starling.juggler.add(this.fadeTween);
			
			this.invalidate(INVALIDATION_FLAG_SIZE);
		}
		
		protected function image_errorHandler(event:Event):void
		{
			this.invalidate(INVALIDATION_FLAG_SIZE);
		}
		
	}
}
