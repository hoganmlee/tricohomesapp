package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.view.components.display.ReferralForm;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ReferralView extends FeathersControl
	{
		[Embed(source="/../assets/images/thankyou.png")]
		private static const THANKS_IMAGE:Class;
		
		private var _bg:Quad;
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
		
		private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;
		
		private var _referralForm:ReferralForm;
				
		public function ReferralView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
						
			var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.scrollPositionVerticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.hasVariableItemDimensions = true;
			//listLayoutV.useVirtualLayout = false;
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.layout = listLayoutV;
			_scrollContainer.paddingLeft = 50;
			//			_scrollContainer.paddingTop = 50;
			
			_holder = new Sprite();
			_scrollContainer.addChild(_holder);
			
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_scrollContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_scrollContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			addChild(_scrollContainer);
			
			
			_referralForm = new ReferralForm();
			_referralForm.sendEmail.add(dispatchEmail);
			_referralForm.x = 748;
			//FORM
			addChild(_referralForm);
			
			//END FORM
									
			addText(550, 58,
				"Homeowner Referral Reward",
				TricoTheme.FONT_HELL_75Bold_38,
				TricoTheme.COLOR_TEXT_TITLE,
				50);
			
			addBar(106);
			
			addText(460, 300,
				"Refer a friend. They build a home.\nYou recieve $1500.\nIt's that easy!",
				TricoTheme.FONT_HELL_45Light_30,
				TricoTheme.COLOR_TEXT_TITLE,
				139);
			
			addBar(259);
			
			var img:Image = new Image(Texture.fromBitmap(new THANKS_IMAGE()));
			img.x = 92;
			img.y = 282;
			_holder.addChild(img);
			
			addBar(485);
			
			addText(658, 300,
				"Trico Homes is one of the top home builders in Calgary, because of customers like you. We appreciate that! And to let you know just how much, any time you refer Trico Homes as your builder of choice and they purchase a new home, you'll receive cash for doing it.",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				515);
			
			//It’s our way of saying, “Thank You So Much!”
			
			addText(658, 50,
				"It’s our way of saying, “Thank You So Much!”",
				TricoTheme.FONT_HELL_75Bold_18,
				TricoTheme.COLOR_TEXT_TITLE,
				632);			
				
			addText(658, 150,
				"If your referral results in an unconditional sale of a new Trico home we will present you with a referral reward, some conditions apply.  To submit a referral simply enter their name and contact info and suggested community in the form and “Send”",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				678);
			
			/*addText(658, 300,
				"Be sure to tell your friends, family and colleagues who are considering purchasing a well crafted home. There's no limit to the number of referrals you can make; we'll reward you with $2500 every time your recommendation results in a new home sale. \n\nWhen you have a referral ready to submit, please complete the form below and a Trico representative will contact you.",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				678);*/
			
			/*addText(658, 100,
				"Some conditions apply. Please read the program information.",
				TricoTheme.FONT_HELL_55Roman_12,
				TricoTheme.COLOR_ACCENT_BUTTON,
				835);*/
			
		}

		public function emailSent(value:Boolean):void
		{
			_referralForm.emailSent(value);
			
			/*if(value){
			_verify.text = "Success!";
			}else{
			_verify.text = "Oh nos. Something went wrong!";
			}*/
			
		}
		
		private function dispatchEmail(subject:String, emailList:String, body:String, isHTML:Boolean):void
		{
			sendEmail.dispatch(subject, emailList, body, isHTML);
		}
		
		private function addText(width:int, height:int, text:String, font:String, color:int, yPos:int, xPos:int=0):void
		{
			var copy:TextField = new TextField(width, height, text, font, BitmapFont.NATIVE_SIZE, color); 
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = yPos;
			copy.x = xPos;
			_holder.addChild(copy);
		}
		
		private function addBar(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			_holder.addChild(bar);
		}

		override protected function draw():void
		{
			_scrollContainer.width = 734
			_scrollContainer.height = 700;
			_scrollContainer.validate()
		}
		
	}
	
}