package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.view.components.display.LoginInput;
	import com.sajakfarki.trico.view.components.display.TricoLogo;
	
	import flash.utils.getTimer;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	
	public class WelcomeView extends FeathersControl
	{
				
//		[Embed(source="/../assets/trico_bg.atf", mimeType="application/octet-stream")]
//		private static const HOME_IMG:Class;
//		private var _homeImg:Texture;
//		const _homeScreenSettings:HomeScreenSettings = new HomeScreenSettings();
				
		[Embed(source="/../assets/trico_bg.png")]
		private static const HOME_IMAGE:Class;
		
		
		public var checkLogin:Signal = new Signal(String, String);
		
		
		private var _homeImage:Texture;
		private var _img:Image;
		
		
		private var _tricoLogo:TricoLogo;
		private var _loginInput:LoginInput;
		private var _signin:Button;
		
		
//		private var _checkBox:Check;
		
		
		private var _inputTransition:Tween;
//		private var _logoTransition:Tween;
		
		private var _initialized:Boolean = false;
		
		public function WelcomeView()
		{
			super();
		}
		
		override protected function initialize():void
		{	
			
			trace(">>>> this.width = " + this.width);
			trace(">>>> this.height = " + this.height);
						
/*			trace();			
						
			var checkFonts:TextField = new TextField(400, this.height, 
													"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\"!`?'.,;:()[]{}<>|/@\\^$-%+=#_&~*“”’—èÈo’éÉ– "+
													"hogan@sajakfarki.com $99.23049825 Apostrophe's {something} \ Between / Signs ~ _- 99% Okay Comma, something_underscore"
													,
													TricoTheme.FONT_HELL_75Bold_38,
													BitmapFont.NATIVE_SIZE,
													0x000000);
			checkFonts.vAlign = VAlign.TOP;
			checkFonts.hAlign = HAlign.LEFT
			checkFonts.x = 50;
			checkFonts.y = 50;
			addChild(checkFonts);*/
			
//			if(settings.isSelected){
//				
//				_checkboxRemember.isSelected = settings.isSelected;
//				
//				_userName.stageTextProperties.text = settings.userName;
//				_userPassword.stageTextProperties.text = settings.userPass;
//				
//			}
//			
//			_checkboxRemember.onRelease.add(toggleRemeber);
//			this.addChild(_checkboxRemember);
			
			
		}
		
		public function init():void
		{
			_homeImage = Texture.fromBitmap(new HOME_IMAGE());
			_img = new Image(_homeImage);
			addChild(_img);
			
			_tricoLogo = new TricoLogo();			
			_tricoLogo.nameList.add("logo1");
			addChild(_tricoLogo);
			
			_loginInput = new LoginInput();
			_loginInput.addEventListener(FeathersEventType.FOCUS_IN, loginFocusIn);
			_loginInput.addEventListener(FeathersEventType.FOCUS_OUT, loginFocusOut);
			_loginInput.addEventListener(FeathersEventType.ENTER, loginListener);
			_loginInput.addEventListener(FeathersEventType.BEGIN_INTERACTION, showSubmit);
			_loginInput.addEventListener(FeathersEventType.CLEAR, hideSubmit);
//			_loginInput.addEventListener(Event.TRIGGERED, loginFocus);
			addChild(_loginInput);
			
			_signin = new Button();
			_signin.nameList.add("signin");
			_signin.label = "Sign In";
			_signin.addEventListener(Event.TRIGGERED, loginListener);
			addChild(_signin);
			
			hideSubmit();
			
			/*_checkBox = new Check();
			_checkBox.label = "Remember me";
			addChild(_checkBox);*/

			_initialized = true;
		}
		
		private function hideSubmit(e:Event=null):void
		{
			_signin.isEnabled = false;
			_signin.alpha = 0.5;
		}
		
		private function showSubmit(e:Event=null):void
		{
			_signin.isEnabled = true;
			_signin.alpha = 1;

//			_signin.visible = true;
//			_signin.addEventListener(Event.TRIGGERED, loginListener);
		}
		
		private function loginFocusOut(e:Event):void
		{
			_inputTransition = new Tween(_loginInput, 0.5, Transitions.EASE_IN_OUT);
			_inputTransition.animate("y", 277);
			_inputTransition.delay = 0.4;
			Starling.juggler.add(_inputTransition);
		}
		
		private function loginFocusIn(e:Event):void
		{
			Starling.juggler.remove(_inputTransition);
//			_inputTransition = null;
			_inputTransition = new Tween(_loginInput, 0.5, Transitions.EASE_IN_OUT);
			_inputTransition.animate("y", 226);
			Starling.juggler.add(_inputTransition);
		}
		
		private function loginListener(event:Event):void
		{
			//TODO: disable login state
			_signin.isEnabled = false;
			_signin.alpha = 0.5;
//			_signin.visible = false;
			
//			_signin.removeEventListener(Event.TRIGGERED, loginListener);
			checkLogin.dispatch(_loginInput.userName, _loginInput.userPassword);
			
		}
		
		public function loginSuccess():void
		{
			//TODO: success, delay, dispatchEvent
			dispatchEventWith(Event.OPEN);
		}
		
		public function loginNameFail():void
		{
			_loginInput.wrongUser();
		}
		
		public function loginPasswordFail():void
		{			
			_loginInput.wrongPassword();			
		}
				
		override protected function draw():void
		{
			if(_initialized){
				
				_tricoLogo.validate();
				_tricoLogo.x = this.width/2 - _tricoLogo.width/2;
				_tricoLogo.y = 40;
				
				_loginInput.x = 450;
				_loginInput.y = 277;
				
				_signin.validate();
				_signin.x = 450;
				_signin.y = 427;
											
//				_checkBox.x = 450;
//				_checkBox.y = 497;
			}
		}
	
	}
}