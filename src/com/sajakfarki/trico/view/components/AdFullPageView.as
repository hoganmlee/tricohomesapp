package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	
	import flash.geom.Rectangle;
	
	import feathers.controls.ImageLoader;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
		
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class AdFullPageView extends FeathersControl
	{
		private var _width:int;
		private var _height:int;
		private var _bg:Quad;
		private var _loader:ImageLoader;
		
		private var _adTransition:Tween;
		private var _exclusiveTransition:Tween;
		private var _ease:Object = Transitions.LINEAR;
		private var _duration:Number = 0.3;
		
		private var _source:DisplayAd;
		
		public function AdFullPageView(width:int, height:int)
		{
			super();
			
			_width = width;
			_height = height;
			
			_bg = new Quad(_width, _height);
			addChild(_bg);
			
			_loader = new ImageLoader();
			_loader.clipRect = new Rectangle(0,0, _width, _height);
			_loader.isEnabled = false;
			_loader.smoothing = TextureSmoothing.NONE;
			_loader.alpha = 0;
			_loader.addEventListener(Event.COMPLETE, loadComplete);
			_loader.addEventListener(FeathersEventType.ERROR, errorListener);
			addChild(_loader);
			
		}
		
		public override function dispose():void
		{
			_loader.dispose();
			_loader = null;
			
			super.dispose();
		}
		
		public function setAd(source:DisplayAd, exclusiveOverride:Boolean = false):void
		{
			//trace("AdSpace :: setAd = " + source.adName);
			_source = source;
			_loader.source = _source.media;
		}
		
		private function errorListener(e:Event):void
		{
			trace(" >>>> AdSpace :: Error  - " + e);
		}
		
		private function loadComplete(e:Event):void
		{
			this.dispatchEventWith(TricoEvent.FULLPAGE)
		}
		
		public function showAd():void
		{
			_adTransition = new Tween(_loader, _duration, _ease);
			_adTransition.fadeTo(1)
			_adTransition.onComplete = activeTransition_onComplete;
			Starling.juggler.add(_adTransition);
		}
		
		private function activeTransition_onComplete():void
		{
			Starling.juggler.remove(_adTransition);
			this._adTransition = null;
		}
		
	}
	
}