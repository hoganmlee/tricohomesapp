package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.CommunityLink;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class CommunityView extends FeathersControl
	{
		private var _bg:Quad;
		private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;
		private var _adSpace:AdSpace;
		
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		
		private var _list:List;
		private var _title:TextField;
		private var _subTitle:TextField;
		
		private var _communities:Vector.<CommunityLink>;
		private var _communityName:String;
		
		private var _divider:Quad;
		private var _titleText:TextField;
		private var _divider2:Quad;
				
		public function CommunityView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
						
			_adSpace = new AdSpace(475, 662);
			this.addChild(_adSpace);
			
			_title = new TextField(600, 100, "Explore Your Community", TricoTheme.FONT_HELL_75Bold_38, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			
			this.addChild(_title);
			
			_divider = new Quad(675, 1, TricoTheme.COLOR_DIVIDER);
			_divider.x = 50;
			_divider.y = 110;
			addChild(_divider);
			
			_titleText = new TextField(420, 100, _communityName, TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_titleText.vAlign = VAlign.TOP;
			_titleText.hAlign = HAlign.LEFT;
			_titleText.x = 50;
			_titleText.y = 136;
			addChild(_titleText);	
			
			_divider2 = new Quad(675, 1, TricoTheme.COLOR_DIVIDER);
			_divider2.x = 50;
			_divider2.y = 183;
			addChild(_divider2);
			
			const listLayout:VerticalLayout = new VerticalLayout();
			listLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
//			listLayout.verticalAlign = Hor.HORIZONTAL_ALIGN_LEFT;
			listLayout.paddingTop = 30;
			listLayout.paddingBottom = 50;
			listLayout.gap = 3;
//			listLayout.hasVariableItemDimensions = true;
//			listLayout.useVirtualLayout = false;
			
			_list = new List();
			_list.layout = listLayout;
			_list.addEventListener(Event.CHANGE, list_changeHandler);
			_list.x = 50;
			_list.y = 183;
			_list.width = 675;
			_list.height = 519;
			_list.validate();	
			
//			_list.backgroundSkin = new Quad(100, 100, 0x222222);
			_list.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_list.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_list.scrollerProperties.snapScrollPositionsToPixels = true;
			_list.itemRendererName = "communityList";
//			_list.itemRendererType = ContentWithStarRenderer
			
			this.addChild(_list);
			
			/** ListCollection **/
			const items:Array = new Array();
			
			for (var i:int = 0; i < _communities.length; i++) 
			{
				if(_communities[i].description == ""){
					items.push({text:_communities[i].heading , link:_communities[i].link});
				}else{
					items.push({text:_communities[i].description , link:_communities[i].link});
				}
			}

//			items.push({text:"Calgary Events: http://www.discovercalgary.com/Calgary/Events", link:"http://www.discovercalgary.com/Calgary/Events"});
//			items.push({text:"Calgary Events: http://www.thiscity.com", link:"http://www.thiscity.com"});
//			items.push({text:"Calgary Schools: Catholic - http://www.cssd.ab.ca", link:"http://www.cssd.ab.ca"});
//			items.push({text:"Calgary Schools: Public - http://www.cbe.ab.ca", link:"http://www.cbe.ab.ca"});
//			items.push({text:"City of Calgary: http://www.calgary.ca", link:"http://www.calgary.ca"});
//			items.push({text:"City of Calgary: http://www.discovercalgary.com", link:"http://www.discovercalgary.com"});
//			items.push({text:"Trico Centre for Family Wellness: http://www.tricocentre.ca", link:"http://www.tricocentre.ca"});
			
			_list.itemRendererProperties.labelField = "text";
			_list.dataProvider = new ListCollection(items);
			
			selectAd.dispatch(DisplayAd.TYPE_AD_3);
			
		}
		
		private function list_changeHandler(e:Event=null):void
		{
//			trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + _list.selectedItem.link);
			if(_list.selectedItem)
			{	
				var request:URLRequest
				if(_list.selectedItem.link.indexOf("http") < 0){
					request = new URLRequest("http://"+_list.selectedItem.link);
				}else{
					request = new URLRequest(_list.selectedItem.link);
				}
				try {
					navigateToURL(request, '_blank');
				} catch (e:Error) {
					trace("Error occurred!");
				}
				
			}
		}
		
		/***************************************************************
		 * 
		 *   Advertisment listeners
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("CommunityView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_3);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_3);
		}
		
		override protected function draw():void
		{
			_adSpace.x = 750;
			_adSpace.y = 20;
			
			_title.x = 50;
			_title.y = 50;
		}
		
		public function addCommunities(communityLinks:Vector.<CommunityLink>, community:String):void
		{
			_communityName = community;
			_communities = communityLinks;
		}
	}
}