package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.model.vo.WishList;
	import com.sajakfarki.trico.model.vo.WishListFinder;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	import com.sajakfarki.trico.view.components.display.OfferPanel;
	
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	
	import feathers.controls.Button;
	import feathers.controls.GroupedList;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollText;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.data.HierarchicalCollection;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class OffersView extends FeathersControl
	{
		//show
		public var clickAd:Signal = new Signal(int, String);
		
		//edit
		public var addAd:Signal = new Signal(int, String);
		public var removeAd:Signal = new Signal(int);
		
		private var _wishlist:WishList;
		private var _wishlistArray:Array;
		private var _userlistArray:Array;
		
		private var _selectedAd:DisplayAd;
		
		private var _offerDictionary:Dictionary;
		private var _offerIndex:int;
		
		private var _bg:Quad;
		
		//OFFERS LIST
//		private var _list:GroupListMultiSelect;
		private var _list:GroupedList	
		
		//OFFERS LIST FILTER
		private var _buttonAll:Button;
		private var _buttonWish:Button;
		
		//ADSPACE
		private var _adSpace:AdSpace;
		private var _title:TextField;
		private var _divider:Quad;
		private var _wishlistButton:Button;
		private var _scrollText:ScrollText;
		
		private var _offerPanel:OfferPanel;
		private var _offerContainer:ScrollContainer;
		private var _firstSelect:Boolean = true;
		
		private var _whiteQuad:Quad;
		private var _blackQuad:Quad;		
		private var _titleText:TextField;
		private var _divider2:Quad;
		
		public function OffersView()
		{
			super();
		}
		
		public function set offerIndex(value:int):void
		{
			_offerIndex = value;
		}

		public function set wishlist(value:WishList):void
		{
			_wishlist = value;
		}

		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
			
			_list = new GroupedList();
			_list.nameList.add("offersGroupedList");
			
			_list.typicalItem = { text: "Item 1000" };
			_list.typicalHeader = "Group 10";
			_list.typicalFooter = "Footer 10";
//			_list.isSelectable = true
			_list.scrollerProperties.hasElasticEdges = true;
			_list.itemRendererProperties.labelField = "text";
			_list.addEventListener(Event.CHANGE, list_changeHandler);

			_list.x = 50;
			_list.y = 183;
			_list.width = 675;
			//_list.height = 591;
			_list.height = 519;
			_list.validate();
			addChild(_list);
			
			_title = new TextField(420, 100, "Partners and Offers", TricoTheme.FONT_HELL_75Bold_38, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_title.vAlign = VAlign.TOP;
			_title.hAlign = HAlign.LEFT;
			_title.x = 50;
			_title.y = 50;
			addChild(_title);
			
			_buttonWish = new Button();
			_buttonWish.nameList.add("offersFilter");
			_buttonWish.isToggle = true;
			_buttonWish.isSelected = false;
			_buttonWish.label = " MY WISHLIST";
			_buttonWish.y = 50;
			_buttonWish.addEventListener(Event.TRIGGERED, wishFilter);
			addChild(_buttonWish);
			
			_buttonAll = new Button();
			_buttonAll.nameList.add("offersFilter");
			_buttonAll.label = "ALL";
			_buttonAll.isToggle = true;
			_buttonAll.isSelected = true;
			_buttonAll.isEnabled = false;
//			_buttonAll.x = 552;
			_buttonAll.y = 50;
			_buttonAll.addEventListener(Event.TRIGGERED, allFilter)
			addChild(_buttonAll);
						
			
			
			_divider = new Quad(675, 1, TricoTheme.COLOR_DIVIDER);
			_divider.x = 50;
			_divider.y = 110;
			addChild(_divider);
			
			_titleText = new TextField(420, 100, "All Offers", TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_TEXT_TITLE);
			_titleText.vAlign = VAlign.TOP;
			_titleText.hAlign = HAlign.LEFT;
			_titleText.x = 50;
			_titleText.y = 136;
			addChild(_titleText);	
			
			_divider2 = new Quad(675, 1, TricoTheme.COLOR_DIVIDER);
			_divider2.x = 50;
			_divider2.y = 183;
			addChild(_divider2);
			
			
			
			_adSpace = new AdSpace(475, 320);
			_adSpace.x = 750;
			_adSpace.y = 20;
			addChild(_adSpace);
			
			//_adSpace.getAd("http://saf.sajakfarki.com/poopower/trico/ad2.png",  true);
			
			_wishlistButton = new Button();
			_wishlistButton.nameList.add("wishlist");
			_wishlistButton.isToggle = true;
//			_wishlistButton.isSelected = true;
//			_wishlistButton.label = "ADD TO WISHLIST";
			_wishlistButton.width = 475;
			_wishlistButton.height = 50;
			_wishlistButton.x = 750;
			_wishlistButton.y = 350;
			_wishlistButton.visible = false;
			_wishlistButton.addEventListener(Event.TRIGGERED, addToWishlist);
			
			addChild(_wishlistButton);
			
			_blackQuad = new Quad(475,274, TricoTheme.COLOR_SIDENAV_TEXT);
			_blackQuad.x = 750;
			_blackQuad.y = 410;
			this.addChild(_blackQuad);
			
			_whiteQuad = new Quad(473,272);
			_whiteQuad.x = 751;
			_whiteQuad.y = 411;
			this.addChild(_whiteQuad);
			
			
			/** Scroll Container **/
			var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = VerticalLayout.HORIZONTAL_ALIGN_LEFT;
			listLayoutV.hasVariableItemDimensions = true;
//			listLayoutV.useVirtualLayout = false;
			
			
			_offerContainer = new ScrollContainer();
			_offerContainer.layout = listLayoutV;
			_offerContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_FLOAT;
			_offerContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_offerContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_offerContainer.scrollerProperties.snapScrollPositionsToPixels = true;
			
			
			/** Text Panel **/
			_offerPanel = new OfferPanel(430, 24);
			_offerContainer.x = 750;
			_offerContainer.y = 412;
			_offerContainer.width = 475;
			_offerContainer.height = 272;
			_offerContainer.addChild(_offerPanel);
			
			this.addChild(_offerContainer);

			if(_wishlist != null){
				loadWishlist();
			}
			
		}
		
		/***************************************************************
		 * 
		 *  Load CheckList into HierarchicalCollection
		 * 
		 ***************************************************************/
		public function loadWishlist():void
		{
			_wishlistArray = getWishListArray();
			_wishlistArray.fixed = true;
			
			_list.dataProvider = new HierarchicalCollection(_wishlistArray);
						
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			
			if(_offerIndex)
			{	
				_list.setSelectedLocation(_offerDictionary[_offerIndex][0],_offerDictionary[_offerIndex][1]);
				setTimeout(_list.scrollToDisplayIndex, 0.5, _offerDictionary[_offerIndex][0],_offerDictionary[_offerIndex][1], 0.5);
			}
			
		}
		
		private function reloadWishList():void
		{
			_list.dataProvider = new HierarchicalCollection(_wishlistArray);
			
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			
			if(_offerIndex)
			{
				_list.setSelectedLocation(_offerDictionary[_offerIndex][0],_offerDictionary[_offerIndex][1]);	
				setTimeout(_list.scrollToDisplayIndex, 0.5, _offerDictionary[_offerIndex][0],_offerDictionary[_offerIndex][1], 0.5);
			}
			
		}
		
		private function getWishListArray():Array
		{
			var arr:Array = new Array();
			_offerDictionary = new Dictionary();
			
			for (var i:int = 0; i < _wishlist.groups.length; i++) 
			{				
				var obj:Object = new Object();
				obj.header = _wishlist.groups[i].advertiserName;
				obj.children = new Array();
				
				for (var j:int = 0; j < _wishlist.groups[i].ads.length; j++) 
				{
					var item:Object = new Object();
					
					item.adName = _wishlist.groups[i].advertiserName;
					item.adID = _wishlist.groups[i].ads[j].adID;
					item.text = _wishlist.groups[i].ads[j].exclusiveTitle;
					item.title = _wishlist.groups[i].ads[j].exclusiveTitle;
					item.media = _wishlist.groups[i].ads[j].media;
					item.desc = _wishlist.groups[i].ads[j].exclusiveDescription;
					
					item.contact = _wishlist.groups[i].adContactName;
					item.phone = _wishlist.groups[i].adContactPhone;
					item.email = _wishlist.groups[i].adContactEmail;
															
//					{ text: "SUPPLIER NAME 1", title: "Offer title", desc:"description of the offer", phone:"403.569.5465", address:"asd SDf sd fs dfs" },
					
					if(i == 0 && j ==0 && _offerIndex == 0){
						_offerIndex = item.adID;
					}
					
					obj.children.push(item);
					
					//REVERSE LOOKUP by adID
					var vec:Vector.<int> = new Vector.<int>();
					vec.push(i);
					vec.push(j);
					
					_offerDictionary[item.adID] = vec;
					
				}
				
				arr.push(obj);
				
			}
						
			return arr;
			
		}
		
		/***************************************************************
		 * 
		 *  set Display Ad
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("OffersView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad, true);
			_adSpace.clicked.add(adClicked);
		}
		
		/***************************************************************
		 * 
		 *  ad Clicked
		 * 
		 ***************************************************************/
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_2);
		}
		
		/***************************************************************
		 * 
		 *  GroupList Filter by 
		 * 
		 ***************************************************************/
		private function wishFilter(e:Event):void
		{
			_buttonAll.isSelected = false;
			_buttonAll.isEnabled = true;
			_buttonWish.isEnabled = false;
			
			_titleText.text = "My Wishlist";
			
			loadUserWishlist();
		}
		
		private function allFilter(e:Event):void
		{
			_buttonWish.isSelected = false;
			_buttonWish.isEnabled = true;
			_buttonAll.isEnabled = false;
			
			_titleText.text = "All Offers";
			
			reloadWishList();
		}
		
		private function addToWishlist(e:Event):void
		{
			var changed:Boolean = false;
			
			var finder:WishListFinder;
			
			if(!_wishlistButton.isSelected)
			{	
				//ADD
				if(!_wishlist.userDictionary[_offerIndex])
				{
					finder = new WishListFinder();
					finder.adID = _offerIndex;
					finder.group = _selectedAd.advertiserName;
					
					_wishlist.userDictionary[_offerIndex] = finder;
					
					//DISPATCH ADD AD
					trace("add ad = " + finder.adID);
					addAd.dispatch(finder.adID, DisplayAd.TYPE_AD_2);
					
					changed = true;
				}
				
			}else{
				
				//REMOVE
				if(_wishlist.userDictionary[_offerIndex])
				{
					//DISPATCH REMOVE AD
					trace("remove ad = " + _wishlist.userDictionary[_offerIndex].adID);
					removeAd.dispatch(_wishlist.userDictionary[_offerIndex].adID);
					
					_wishlist.userDictionary[_offerIndex] = null;
					delete _wishlist.userDictionary[_offerIndex];
					
					changed = true;
				}
				
			}
			
			if(changed && _buttonWish.isSelected)
			{
				loadUserWishlist();
			}
			
		}
		
		private function loadUserWishlist():void
		{
			_userlistArray = getUserWishlist();
			
			if(_userlistArray.length > 0)
			{
				_userlistArray.fixed = true;
				
				_list.dataProvider = new HierarchicalCollection(_userlistArray);
				
//				_list.setSelectedLocation(null, null);
//				_list.setSelectedLocation(0,0);
//				_list.scrollToDisplayIndex(0,0);
//				this._list.setSelectedLocation(0, 0);
				
				this.invalidate(INVALIDATION_FLAG_LAYOUT);
			}
		}
		
		private function getUserWishlist():Array
		{
			var arr:Array = new Array();
			
			for each (var i:* in _wishlist.userDictionary) 
			{
				var finder:WishListFinder = new WishListFinder();
				finder = i;
								
				var obj:Object = new Object();
				obj.header = finder.group;
				obj.children = new Array();
				
				var item:Object = new Object();
				item.adName = _wishlist.groupDictionary[finder.group].advertiserName;
				item.adID = _wishlist.groupDictionary[finder.group].adsDictionary[finder.adID].adID;
				item.text = _wishlist.groupDictionary[finder.group].adsDictionary[finder.adID].exclusiveTitle;
				item.title = _wishlist.groupDictionary[finder.group].adsDictionary[finder.adID].exclusiveTitle;
				item.media = _wishlist.groupDictionary[finder.group].adsDictionary[finder.adID].media;
				item.desc = _wishlist.groupDictionary[finder.group].adsDictionary[finder.adID].exclusiveDescription;
				
				item.contact = _wishlist.groupDictionary[finder.group].adContactName;
				item.phone = _wishlist.groupDictionary[finder.group].adContactPhone;
				item.email = _wishlist.groupDictionary[finder.group].adContactEmail;
								
				var found:Boolean = false;
				for (var j:int = 0; j < arr.length; j++) 
				{
					if(arr[j].header == finder.group)
					{
						found = true;
						arr[j].children.push(item);
						break;
					}
				}
				
				if(!found){
					obj.children.push(item);
					arr.push(obj);
				}
				
			}
			
			return arr;
			
		}
		
		/***************************************************************
		 * 
		 *  list Change Handler
		 * 
		 ***************************************************************/
		private function list_changeHandler(e:Event=null):void
		{
			//trace("list_changeHandler = " + e);
			if(_offerIndex == 0)
			{
				_offerIndex = _selectedAd.adID;
			}
			
			_selectedAd = _wishlist.groupDictionary[_list.selectedItem.adName].adsDictionary[_list.selectedItem.adID];
			
			if(_selectedAd.adID != _offerIndex || _firstSelect)
			{
				_offerIndex = _selectedAd.adID;
				
				if(_wishlist.userDictionary[_offerIndex])
				{
					_wishlistButton.isSelected = true;
				}else{
					_wishlistButton.isSelected = false;
				}
				
				_offerContainer.stopScrolling();
				
				if(_list.selectedItem)
				{
//					trace(_list.selectedItem.desc);
					_offerPanel.offer(_list.selectedItem.title, _list.selectedItem.desc);
				}
				
				/** Dictionary access by advertiser name / ad ID **/
				_adSpace.setAd(_selectedAd, true);
				
			}
			
			if(_firstSelect){
				_firstSelect =  false;
				_wishlistButton.visible = true;
			}
			
		}
		
		/***************************************************************
		 * 
		 *   Draw
		 * 
		 ***************************************************************/
		override protected function draw():void
		{
			_buttonWish.validate();
			_buttonWish.x = 725 - _buttonWish.width;
			
			_buttonAll.validate();
			_buttonAll.x = _buttonWish.x - _buttonAll.width - 8;
			
		}
		
	}
	
}