package com.sajakfarki.trico.view.components.promotions
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	import com.sajakfarki.trico.view.components.display.EquityForm;
	
	import feathers.core.FeathersControl;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class EquitySaverView extends FeathersControl
	{
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		
		private var _bg:Quad;
		
		/*private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;*/
		
		private var _equityForm:EquityForm;
		private var _adSpace:AdSpace;
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
				
		public function EquitySaverView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
						
			_equityForm = new EquityForm();
//			_equityForm.x = 748;
			_equityForm.x = 50;
			_equityForm.y = 260;
			_equityForm.sendEmail.add(dispatchEmail);
			addChild(_equityForm);
			
			/*var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.scrollPositionVerticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.hasVariableItemDimensions = true;
			//listLayoutV.useVirtualLayout = false;
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.layout = listLayoutV;
			_scrollContainer.paddingLeft = 50;
			//			_scrollContainer.paddingTop = 50;
			
			_holder = new Sprite();
			_scrollContainer.addChild(_holder);
			
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_scrollContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_scrollContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			addChild(_scrollContainer);*/
			
			addText(420, 58,
				"Equity Saver Program",
				TricoTheme.FONT_HELL_75Bold_38,
				TricoTheme.COLOR_TEXT_TITLE,
				50);
			
			addBar(106);
			
			addText(658, 300,
				"It’s not that we expect you will be selling your new home soon, although you never know what the future may bring. Whether it be a new job, or a new baby, there are many reasons you may decide to sell your home. Whatever your reason, Trico’s Equity Saver Program is a way for you to ensure you retain your equity by reducing your costs to sell. If you choose to purchase another Trico Home we'll pay the listing and selling commissions to sell your present home. Our only stipulation is that you must use our Realtor representatives to list the property.\n\nThe Trico Homes Equity Saver program cannot be used in conjunction with any other promotion or licensed Realtor.",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				138);
			
			_adSpace = new AdSpace(475, 662);
			this.addChild(_adSpace);
			selectAd.dispatch(DisplayAd.TYPE_AD_3);
						
		}
		/***************************************************************
		 * 
		 *   Advertisment listeners
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("GlobalFestView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_3);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_3);
		}
		
		public function emailSent(value:Boolean):void
		{
			_equityForm.emailSent(value);
			
			/*if(value){
			_verify.text = "Success!";
			}else{
			_verify.text = "Oh nos. Something went wrong!";
			}*/
			
		}
		
		private function dispatchEmail(subject:String, emailList:String, body:String, isHTML:Boolean):void
		{
			sendEmail.dispatch(subject, emailList, body, isHTML);
		}
		
		/***************************************************************
		 * 
		 *  Create functions
		 * 
		 ***************************************************************/
		private function addText(width:int, height:int, text:String, font:String, color:int, yPos:int, xPos:int=50):void
		{
			var copy:TextField = new TextField(width, height, text, font, BitmapFont.NATIVE_SIZE, color); 
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = yPos;
			copy.x = xPos;
			addChild(copy);
		}
		
		private function addBar(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			bar.x = 50;
			addChild(bar);
		}
		
		//
		override protected function draw():void
		{
			_adSpace.x = 750;
			_adSpace.y = 20;
		}
	}
}