package com.sajakfarki.trico.view.components.promotions
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class TricoCentreView extends FeathersControl
	{
		[Embed(source="/../assets/images/logo_trico_centre.png")]
		private static const LOGO_IMAGE:Class;
		
		private var _bg:Quad;
		private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;
		private var _adSpace:AdSpace;
		
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		
		public function TricoCentreView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
			
			
			_adSpace = new AdSpace(475, 662);
			this.addChild(_adSpace);
			
			
			var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.scrollPositionVerticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.hasVariableItemDimensions = true;
			//listLayoutV.useVirtualLayout = false;
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.layout = listLayoutV;
			_scrollContainer.paddingLeft = 50;
			//			_scrollContainer.paddingTop = 50;
			
			_holder = new Sprite();
			_scrollContainer.addChild(_holder);
			
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_scrollContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_scrollContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			addChild(_scrollContainer);
			
			
			addText(420, 58,
				"Trico Centre",
				TricoTheme.FONT_HELL_75Bold_38,
				TricoTheme.COLOR_TEXT_TITLE,
				50);
			
			addBar(106);
			
			var img:Image = new Image(Texture.fromBitmap(new LOGO_IMAGE()));
			img.x = 199;
			img.y = 118;
			_holder.addChild(img);
			
			addBar(340);
			
			
			addText(658, 500,
				"    The Trico Centre for Family Wellness is a multi-purpose recreational complex that was built to enrich the health, wellness, and unity of communities in South Calgary. Trico Homes is proud to be the naming sponsor and of its commitment to assist the Trico Centre in serving the community with a great facility and excellent programs. Since its inauguration, the complex has grown to include two indoor ice arenas, a huge fitness centre, gymnasium, aquatics centre, health and wellness services, on-site child care, and meeting rooms, as well as learning programs for children, youths, and adults." +
				"\n    Our partnership with the Trico Centre for Family Wellness allows us to offer our customers yet another exciting perk! As a Trico Home purchaser we're delighted to offer you 50% off a One Year Family Membership at the Trico Center. You will receive your a certificate entitling you to a membership discount from your Trico Homes Sales Representative shortly before you take possession." +
				"\n    When activating your membership please take your certificate and proof of purchase (Trico Homes Purchase Agreement or Your Possession Letter) along with you to the Trico Centre. Your certificate can be redeemed up to one year after your possession date." +
				"\n    For more information on the Trico Centre and their programs please visit: www.tricocentre.ca",
				TricoTheme.FONT_HELL_57Cond_20,
				TricoTheme.COLOR_ACCENT_BUTTON,
				368);
			
			addBar(793);
			
			selectAd.dispatch(DisplayAd.TYPE_AD_3);
			
		}
		
		/***************************************************************
		 * 
		 *   Advertisment listeners
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("GlobalFestView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_3);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_3);
		}
		
		/***************************************************************
		 * 
		 *  Create functions
		 * 
		 ***************************************************************/
		private function addText(width:int, height:int, text:String, font:String, color:int, yPos:int, xPos:int=0):void
		{
			var copy:TextField = new TextField(width, height, text, font, BitmapFont.NATIVE_SIZE, color); 
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = yPos;
			copy.x = xPos;
			_holder.addChild(copy);
		}
		
		private function addBar(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			_holder.addChild(bar);
		}
		
		
		//
		override protected function draw():void
		{
			_scrollContainer.width = 734;
			_scrollContainer.height = 700;
			_scrollContainer.validate();
			
			_adSpace.x = 750;
			_adSpace.y = 20;
		}
		
	}
}
