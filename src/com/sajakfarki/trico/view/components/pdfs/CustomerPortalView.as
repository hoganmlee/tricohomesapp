package com.sajakfarki.trico.view.components.pdfs
{
	import com.sajakfarki.trico.TricoContext;
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.PDFService;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	
	import flash.filesystem.File;
	
	import feathers.controls.Button;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class CustomerPortalView extends FeathersControl
	{
		private var _bg:Quad;
		private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;
		private var _adSpace:AdSpace;
		
		
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		private var _openPDF:Button;
		
		public function CustomerPortalView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
			
			_adSpace = new AdSpace(475, 662);
			this.addChild(_adSpace);
						
			var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.scrollPositionVerticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.hasVariableItemDimensions = true;
			//listLayoutV.useVirtualLayout = false;
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.layout = listLayoutV;
			_scrollContainer.paddingLeft = 50;
			
			_holder = new Sprite();
			_scrollContainer.addChild(_holder);
			
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_scrollContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_scrollContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			addChild(_scrollContainer);
			
			addText(658, 68,
				"Customer Portal User Manual",
				TricoTheme.FONT_HELL_75Bold_38,
				TricoTheme.COLOR_TEXT_TITLE,
				50);
			
			addBar(106);
			
			addText(640, 500,
				"Congratulations on the purchase of your new Trico home!",
				TricoTheme.FONT_HELL_45Light_30,
				TricoTheme.COLOR_TEXT_TITLE,
				133);
			
			addBar(223);
						
			addText(658, 500,
				"To help you keep track of your documents, Trico Homes has created a “Customer Portal”. A Customer Portal is a private website that enables our office to share all documents and information with you. The portal provides 24 hour, 7 days a week to access to all your documents with nothing more than an internet connection."+
 				"\n\nTo help you navigate through the portal, we have created an east to use manual for your convenience.",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				251);
			
			addBar(435);
			
			selectAd.dispatch(DisplayAd.TYPE_AD_3);
			
			_openPDF = new Button();
			_openPDF.nameList.add("sendLong");
			_openPDF.label = "Open Manual";
			_openPDF.addEventListener(Event.TRIGGERED, sendListener);
			_openPDF.y = 470;
			_openPDF.x = 0;
			_holder.addChild(_openPDF);
		}
		
		private function sendListener(e:Event):void
		{
//			PDFService.openPDF(File.documentsDirectory.resolvePath(TricoContext.TRICO_FOLDER+"/"+TricoContext.CUSTOMER_PORTAL).nativePath);
		}
		
		/***************************************************************
		 * 
		 *   Advertisment listeners
		 * 
		 ***************************************************************/
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("GlobalFestView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_3);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_3);
		}
		
		/***************************************************************
		 * 
		 *  Create functions
		 * 
		 ***************************************************************/
		private function addText(width:int, height:int, text:String, font:String, color:int, yPos:int, xPos:int=0):void
		{
			var copy:TextField = new TextField(width, height, text, font, BitmapFont.NATIVE_SIZE, color);
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = yPos;
			copy.x = xPos;
			_holder.addChild(copy);
		}
		
		private function addBar(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			_holder.addChild(bar);
		}
		
		//
		override protected function draw():void
		{
			_scrollContainer.width = 734;
			_scrollContainer.height = 700;
			_scrollContainer.validate();
			
			_adSpace.x = 750;
			_adSpace.y = 20;
		}
	}
}

