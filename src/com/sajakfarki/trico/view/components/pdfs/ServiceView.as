package com.sajakfarki.trico.view.components.pdfs
{
	import com.sajakfarki.trico.TricoContext;
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.service.PDFService;
	
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import feathers.controls.Button;
	import feathers.controls.Check;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ServiceView extends FeathersControl
	{
//		public static var EMAIL_SUBJECT:String = "Hey, need some email subject copy for the service section";
//		public static var EMAIL_LIST:String = "hogan@sajakfarki.com";
//		public static var CHECKLIST_URL:String = "http://www.google.com";
		
		public static var SERVICE_URL:String = "http://dev.tricohomes.com/service_warranty/";
		
		[Embed(source="/../assets/images/service_image01.png")]
		private static const SERVICE_IMAGE:Class;
		
		private var _bg:Quad;
		private var _scrollContainer:ScrollContainer;
		private var _holder:Sprite;
		
//		private var _serviceForm:ServiceForm;
		
		/*private var _nameInput:UserTextInput;
		private var _phoneInput:UserTextInput;
		private var _emailInput:UserTextInput;
		private var _sendButton:Button;
		private var _verify:TextField;*/
		
		public var sendEmail:Signal = new Signal(String,String,String,Boolean);
		private var _gotoForm:Button;
		private var _openChecklist:Button;
		private var _openTimeline :Button;
		private var _openCaring:Button;
		
		
		public function ServiceView()
		{
			super();
		}
		
		override protected function initialize():void
		{
			_bg = new Quad(width, height);
			addChild(_bg);
			
			var img:Image = new Image(Texture.fromBitmap(new SERVICE_IMAGE()));
			img.x = 751;
			img.y = 25;
			addChild(img);
			
			addBarRight(749, 381);
			addBarRight(749, 464);
			addBarRight(749, 690);
			
			var title:TextField = new TextField(
				300,
				50, 
				"Service Request", 
				TricoTheme.FONT_HELL_45Light_30, 
				BitmapFont.NATIVE_SIZE,
				TricoTheme.COLOR_TEXT_TITLE); 
			title.vAlign = VAlign.TOP;
			title.hAlign = HAlign.LEFT;
			title.y = 411;
			title.x = 749;
			addChild(title);
			
			var copy:TextField = new TextField(				
				454,
				200, 
				"It's our goal to provide you with excellent customer service. If there's anything you need done, please follow this link to our Service Request Form.", 
				TricoTheme.FONT_HELL_45Light_18, 
				BitmapFont.NATIVE_SIZE,
				TricoTheme.COLOR_ACCENT_BUTTON); 
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = 497;
			copy.x = 749;
			addChild(copy);
			
			
			
			_gotoForm = new Button();
			_gotoForm.nameList.add("sendLong");
			_gotoForm.label = "Service Request Form";
			_gotoForm.addEventListener(Event.TRIGGERED, sendListener);
			_gotoForm.x = 749;
			_gotoForm.y = 599;
			addChild(_gotoForm);
			
			
			
			var listLayoutV:VerticalLayout = new VerticalLayout();
			listLayoutV.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.scrollPositionVerticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			listLayoutV.hasVariableItemDimensions = true;
			//listLayoutV.useVirtualLayout = false;
			
			_scrollContainer = new ScrollContainer();
			_scrollContainer.layout = listLayoutV;
			_scrollContainer.paddingLeft = 50;
//			_scrollContainer.paddingTop = 50;
			
			_holder = new Sprite();
			_scrollContainer.addChild(_holder);
			
			_scrollContainer.scrollerProperties.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_NONE;
			_scrollContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_scrollContainer.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			addChild(_scrollContainer);
			
			addText(420, 58,
				"Move In Made Easy",
				TricoTheme.FONT_HELL_75Bold_38,
				TricoTheme.COLOR_TEXT_TITLE,
				54);
			
			addBar(110);
			
			addText(658, 300,
				"Moving into a new home is an exciting time but it can also be stressful. Don’t despair; whether you’re doing it yourself, asking friends for a little help or hiring professionals, we've come up with a convenient moving guide to help you through it!",
				TricoTheme.FONT_HELL_45Light_30,
				TricoTheme.COLOR_ACCENT_BUTTON,
				136);
			
			addBar(335);
			
			
			
			
						
			/******* fill out form *******/
			/*
			_serviceForm = new ServiceForm();
			_serviceForm.sendEmail.add(dispatchEmail);
			_serviceForm.y = 409;
			_holder.addChild(_serviceForm);*/
			
			/******* end form *******/
								
			/*addText(658, 24,
				"Moving Check List",
				TricoTheme.FONT_HELL_75Bold_18,
				TricoTheme.COLOR_TEXT_TITLE,
				252);

			addText(658, 400,
				"Following a scheduled maintenance program is the best way to maintain the most valuable assets within your new home. Detailed warranty and maintenance information can be found in The Building Experience Manual. For your convenience, the following quick links will provide you with some handy tips for recommended regular maintenance:",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				276);*/
			
			/*var button:Button = new Button();
			button = new Button();
			button.nameList.add("homeLink");
			button.label = "MOVING CHECK LIST";
			button.width = 675;
			button.height = 50;
			button.y = 661 + 54;
			button.addEventListener(Event.TRIGGERED, checklistURL);
			_holder.addChild(button);
			
			addBar(404+309);
			
			*/
			/*addBar(404);
			
			addText(658, 24,
				"Regular Homeowner Scheduled Maintenance",
				TricoTheme.FONT_HELL_75Bold_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				418);
			
			
			addText(658, 24,
				"Seasonal Maintenance Scheduled",
				TricoTheme.FONT_HELL_75Bold_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				418 + 40 * 5);
			
			addBar(404+40);
			addBar(404+40*2);
			addBar(404+40*3);
			addBar(404+40*4);
			addBar(404+40*5);
			addBar(404+40*6);
			addBar(404+40*7);
			addBar(404+40*8);*/
			
			
			
			
			
		/*	addCheck("Monthly", 447);
			addCheck("Seasonly", 447+40);
			addCheck("Twice per year", 447+40*2);
			addCheck("Annually", 447+40*3);
			
			addCheck("Spring/Summer Interior", 447+40*5);
			addCheck("Fall Interior", 447+40*6);
			addCheck("Winter Interior", 447+40*7);
						*/
			
/*			addCheck(658, 24,
				"•  Monthly",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 324 + 42, 3);
			
			
			addCheck(658, 24,
				"•  Seasonly",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 324 + 42 * 2, 3);
			
			addCheck(658, 24,
				"•  Twice per year",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 324 + 42 * 3, 3);
			
			addCheck(658, 24,
				"•  Annually",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 324 + 42 * 4, 3);
									
			addCheck(658, 24,
				"Seasonal Maintenance Scheduled",
				TricoTheme.FONT_HELL_75Bold_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 321 + 42 * 5);
			
			addCheck(658, 24,
				"•  Spring/Summer Interior",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 320 + 42 * 6, 3);
			
			addCheck(658, 24,
				"•  Fall Interior",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 320 + 42 * 7, 3);
			
			addCheck(658, 24,
				"•  Winter Interior",
				TricoTheme.FONT_HELL_45Light_18,
				TricoTheme.COLOR_ACCENT_BUTTON,
				404 + 320 + 42 * 8, 3);*/
			
			/*var paddingBottom:Quad = new Quad(50, 100);
			paddingBottom.y = _holder.height + 100;
			_holder.addChild(paddingBottom);*/
			
			
			_openChecklist = new Button();
			_openChecklist.nameList.add("sendLong");
			_openChecklist.label = "Moving Checklist";
			_openChecklist.addEventListener(Event.TRIGGERED, openChecklist);
			_openChecklist.y = 369;
			_openChecklist.x = 0;
			_holder.addChild(_openChecklist);
			
			
			_openTimeline = new Button();
			_openTimeline.nameList.add("sendLong");
			_openTimeline.label = "Moving Timeline";
			_openTimeline.addEventListener(Event.TRIGGERED, openTimeline);
			_openTimeline.y = 438;
			_openTimeline.x = 0;
			_holder.addChild(_openTimeline);
			
			_openCaring = new Button();
			_openCaring.nameList.add("sendLong");
			_openCaring.label = "Caring For Your Home";
			_openCaring.addEventListener(Event.TRIGGERED, openCaring);
			_openCaring.y = 507;
			_openCaring.x = 0;
			_holder.addChild(_openCaring);
			
		}
		
		private function openChecklist(e:Event):void
		{
//			PDFService.openPDF(File.documentsDirectory.resolvePath(TricoContext.TRICO_FOLDER+"/"+TricoContext.MOVING_CHECKLIST).nativePath);
		}
		
		private function openTimeline(e:Event):void
		{
//			PDFService.openPDF(File.documentsDirectory.resolvePath(TricoContext.TRICO_FOLDER+"/"+TricoContext.MOVING_TIMELINE).nativePath);
		}
		
		private function openCaring(e:Event):void
		{
//			PDFService.openPDF(File.documentsDirectory.resolvePath(TricoContext.TRICO_FOLDER+"/"+TricoContext.CARING_HOME).nativePath);
		}
		
		private function sendListener(e:Event):void
		{
			var request:URLRequest = new URLRequest(SERVICE_URL);
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
		}
		
		private function addBarRight(x:int, y:int):void
		{
			var bar:Quad = new Quad(475, 1, 0xD9D8D4);
			bar.x = x;
			bar.y = y;
			addChild(bar);
		}
		
		/*private function dispatchEmail(subject:String, emailList:String, body:String, isHTML:Boolean):void
		{
			sendEmail.dispatch(subject, emailList, body, isHTML);
		}*/
		
		private function addCheck(text:String, yPos:int, xPos:int=0):void
		{
			var check:Check = new Check();
			check.nameList.add("serviceCheck");
			check.label = text;
			check.width = 673;
			check.y = yPos;
			check.x = xPos;
			_holder.addChild(check);
		}
		
		
		private function addText(width:int, height:int, text:String, font:String, color:int, yPos:int, xPos:int=0):void
		{
			var copy:TextField = new TextField(width, height, text, font, BitmapFont.NATIVE_SIZE, color); 
			copy.vAlign = VAlign.TOP;
			copy.hAlign = HAlign.LEFT;
			copy.y = yPos;
			copy.x = xPos;
			_holder.addChild(copy);
		}
		
		private function addBar(yPos:int):void
		{
			var bar:Quad = new Quad(675, 1, 0xD9D8D4);
			bar.y = yPos;
			_holder.addChild(bar);
		}
		
		/*private function checklistURL(e:Event):void
		{
			var request:URLRequest = new URLRequest(CHECKLIST_URL);
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
		}*/
		
		override protected function draw():void
		{
			_scrollContainer.width = 734;
			_scrollContainer.height = 700;
			_scrollContainer.validate();
		}
		
		/*public function emailSent(value:Boolean):void
		{
			_serviceForm.emailSent(value);
			
			/*if(value){
				_verify.text = "Success!";
			}else{
				_verify.text = "Oh nos. Something went wrong!";
			}*/
			
		//}
	}
}