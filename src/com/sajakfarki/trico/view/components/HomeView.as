package com.sajakfarki.trico.view.components
{
	import com.sajakfarki.trico.TricoTheme;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.display.AdSpace;
	import com.sajakfarki.trico.view.components.display.Bucket;
	import com.sajakfarki.trico.view.components.display.TricoLogo;
	import com.sajakfarki.trico.view.components.nav.SideNavDivider;
	
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Dictionary;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.display.Scale3Image;
	import feathers.textures.Scale3Textures;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class HomeView extends FeathersControl
	{			
		public static var BUCKET_CHECKLIST:String = "studio_362.png";
		public static var BUCKET_PARTNERS:String = "partners_362.png";
		public static var BUCKET_ALBUM:String = "album_362.png";
		
		public var selectAd:Signal = new Signal(String);
		public var clickAd:Signal = new Signal(int, String);
		public var exclusive:Signal = new Signal(int);
		
		private var _isConnected:Boolean = false;
		private var _isStartUp:Boolean = true;
		private var _countCheckList:String;
		private var _countOffersList:String;
		private var _countAlbumList:String;
		
		private var _linkDictionary:Dictionary;
		
		private var _bgQuad:Quad;
		private var _bgQuad2:Quad;
		
		private var _logo:TricoLogo;
				
		private var _adSpace:AdSpace;
		
		private var _checkList:Bucket;
		private var _album:Bucket;
		private var _offers:Bucket;
		private var _yourCommunity:TextField;
		private var _tricoBenefits:TextField;
		private var _guidesManuals:TextField;
		private var _div:SideNavDivider;
		
		private var _dropShadow1:Image;
		private var _dropShadow2:Image;
		private var _dropShadow3:Image;
		
		private var _bottomShadow1:Scale3Image;
		private var _bottomShadow2:Scale3Image;
		private var _bottomShadow3:Scale3Image;

		//FONT DEBUG
		private var _introCopy:TextField;
		
		public function HomeView()
		{
			super();
		}

		public function get isStartUp():Boolean
		{
			return _isStartUp;
		}

		public function set isStartUp(value:Boolean):void
		{
			_isStartUp = value;
		}

		public function get isConnected():Boolean
		{
			return _isConnected;
		}

		public function set isConnected(value:Boolean):void
		{
			if(value){
				
				if(_isInitialized){
					getUser();
				}
				
			}
			_isConnected = value;
		}

		override protected function initialize():void
		{
		
			_linkDictionary = new Dictionary();
			
			_bgQuad = new Quad(this.stage.stageWidth, 197, 0xbdbcb7);
			addChild(_bgQuad);
			
			_bgQuad2 = new Quad(this.stage.stageWidth, 505, 0xE0E0DB);
			_bgQuad2.y = 197;
			addChild(_bgQuad2);
			
			_bottomShadow1.width =  this.width;
			_bottomShadow1.height = 103;
			_bottomShadow1.y = 197 - _bottomShadow1.height;
			addChild(_bottomShadow1);
			
			_bottomShadow2.width =  this.width;
			_bottomShadow2.height = 13;
			_bottomShadow2.y = 197 - _bottomShadow2.height;
			addChild(_bottomShadow2);
			
			_bottomShadow3.width =  this.width;
			_bottomShadow3.height = 400;
			_bottomShadow3.y = this.height - _bottomShadow3.height;
			addChild(_bottomShadow3);
			
			_logo = new TricoLogo();
			_logo.nameList.add("logo2");
			_logo.x = 68;
			_logo.y = 38;
			addChild(_logo);
			
			_dropShadow1.x = 50;
			_dropShadow1.y = 455;
			addChild(_dropShadow1);
			
			_dropShadow2.x = 450;
			_dropShadow2.y = 455;
			addChild(_dropShadow2);
			
			_dropShadow3.x = 850;
			_dropShadow3.y = 455;
			addChild(_dropShadow3);
			
			_div = new SideNavDivider(this.width, 0xC4C3BF, 0xF7F6F2);
			_div.y = 451;
			addChild(_div);
			
			_yourCommunity = new TextField(375, 24, "YOUR COMMUNITY", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON);
			_yourCommunity.vAlign = VAlign.TOP;
			_yourCommunity.hAlign = HAlign.LEFT;
			_yourCommunity.x = 50;
			_yourCommunity.y = 478;
			addChild(_yourCommunity);
			
			_tricoBenefits = new TextField(375, 24, "TRICO VIP BENEFITS", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON);
			_tricoBenefits.vAlign = VAlign.TOP;
			_tricoBenefits.hAlign = HAlign.LEFT;
			_tricoBenefits.x = 450;
			_tricoBenefits.y = 478;
			addChild(_tricoBenefits);
			
			_guidesManuals = new TextField(375, 24, "GUIDES & MANUALS", TricoTheme.FONT_HELL_67MediumCond_20, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON);
			_guidesManuals.vAlign = VAlign.TOP;
			_guidesManuals.hAlign = HAlign.LEFT;
			_guidesManuals.x = 850;
			_guidesManuals.y = 478;
			addChild(_guidesManuals);
			
			/*_introCopy = new TextField(375, 300, 
							"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\"!`?'.,;:()[]{}<>|/@\^$-%+=#_&~*“”’—èÈo’éÉ–®™ •…",
							TricoTheme.FONT_HELL_45Light_30, BitmapFont.NATIVE_SIZE, TricoTheme.COLOR_ACCENT_BUTTON);
			_introCopy.vAlign = VAlign.TOP;
			_introCopy.hAlign = HAlign.LEFT;
			_introCopy.x = 50;
			_introCopy.y = 520;
			addChild(_introCopy);*/
			
			
			addSectionButton("homeLink", "EXPLORE YOUR COMMUNITY", 50, 375, 47, 522, TricoEvent.COMMUNITY, bucketListener);
			addSectionButton("homeLink", "TRICO CENTRE", 50, 375, 47, 575, TricoEvent.TRICO_CENTRE, bucketListener);
//			addSectionButton("homeLink", "GLOBAL FEST", 50, 375, 47, 575, TricoEvent.GLOBAL_FEST, bucketListener);
			
			
			addSectionButton("homeLink", "EQUITY SAVER", 50, 375, 450, 522, TricoEvent.EQUITY_SAVER, bucketListener);
			addSectionButton("homeLink", "REFERAL PROGRAM", 50, 375, 450, 575, TricoEvent.REFERRAL, bucketListener);
			addSectionButton("homeLink", "MOVE IN MADE EASY", 50, 375, 450, 628, TricoEvent.SERVICE, bucketListener);
			
			
			addSectionButton("homeLink", "THE BUILDING EXPERIENCE MANUAL", 50, 375, 850, 522, TricoEvent.BUILDING_EXPERIENCE, bucketListener);
			addSectionButton("homeLink", "INTERIOR SELECTIONS GUIDE", 50, 375, 850, 575, TricoEvent.INTERIOR_SELECTION, bucketListener);
			addSectionButton("homeLink", "CUSTOMER PORTAL USER MANUAL", 50, 375, 850, 628, TricoEvent.CUSTOMER_PORTAL, bucketListener);
			
			
//			addButton("homePDF", "THE BUILDING EXPERIENCE MANUAL", 50, 375, 850, 522, "http://dev.tricohomes.com/customer_app/customer_manual/TC_10089_ExperienceLR.pdf", linkListener);
//			addButton("homePDF", "INTERIOR SELECTIONS GUIDE", 50, 375, 850, 575, "http://dev.tricohomes.com/customer_app/quick_reference/TC_10503_Building_Guide_LR.pdf", linkListener);
//			addButton("homePDF", "CUSTOMER PORTAL USER MANUAL", 50, 375, 850, 628, "http://dev.tricohomes.com/customer_app/customer_portal_manual/TC_10733_Cust_Portal.pdf", linkListener);
									
			
			_adSpace = new AdSpace(975, 160);
			_adSpace.x = 250;
			_adSpace.y = 18;
			addChild(_adSpace);	
			
//			_checkList = new Bucket("http://saf.sajakfarki.com/poopower/trico/bucket1.png", "checklist", "ACCENTS DESIGN STUDIO\nINTERIOR CHECKLIST", 143);
			_checkList = new Bucket(File.applicationDirectory.resolvePath(BUCKET_CHECKLIST).url, "checklist", "ACCENTS DESIGN STUDIO\nINTERIOR CHECKLIST", 143);
			_checkList.addEventListener(Event.TRIGGERED, bucketListener);
			_checkList.x = 50;
			_checkList.y = 216;
			addChild(_checkList);
			_linkDictionary[_checkList] = TricoEvent.CHECKLIST;
			
//			_offers = new Bucket("http://saf.sajakfarki.com/poopower/trico/bucket2.png", "offers", "PARTNERS AND OFFERS", 148);
			_offers = new Bucket(File.applicationDirectory.resolvePath(BUCKET_PARTNERS).url, "offers", "PARTNERS AND OFFERS", 148);
			_offers.addEventListener(Event.TRIGGERED, bucketListener);
			_offers.x = 450;
			_offers.y = 216;
			addChild(_offers);
			_linkDictionary[_offers] = TricoEvent.OFFERS;
			
//			_album= new Bucket("http://saf.sajakfarki.com/poopower/trico/bucket3.png", "album", "MY HOME PHOTO ALBUM", 148);
			_album= new Bucket(File.applicationDirectory.resolvePath(BUCKET_ALBUM).url, "album", "MY HOME PHOTO ALBUM", 148);
			_album.addEventListener(Event.TRIGGERED, bucketListener);
			_album.x = 850;
			_album.y = 216;
			addChild(_album);
			_linkDictionary[_album] = TricoEvent.ALBUM;
			
			if(_isConnected){
				getUser();
			}
						
		}
		
		private function sectionListener():void
		{
			
		}
				
		public function addCounts(checkList:String, offersList:String, albumList:String):void
		{
			_countCheckList = checkList;
			_countOffersList = offersList;
			_countAlbumList = albumList;
			
//			trace(this.contains(_checkList));
			
			checkCounts();
			
		}
		
		private function checkCounts():void
		{
			if(this.contains(_checkList)){
				_checkList.counter = _countCheckList;
				_offers.counter = _countOffersList;
				_album.counter = _countAlbumList;
			}
		}
		
		public function addShadow(texture:Texture, bottomShadow:Scale3Textures):void
		{
			_dropShadow1 = new Image(texture);
			_dropShadow2 = new Image(texture);
			_dropShadow3 = new Image(texture);
			
			_bottomShadow1 = new Scale3Image(bottomShadow);
			_bottomShadow2 = new Scale3Image(bottomShadow);
			_bottomShadow3 = new Scale3Image(bottomShadow);
		}
		
		private function linkListener(e:Event):void
		{
			var request:URLRequest = new URLRequest(_linkDictionary[e.target]);
			try {
				navigateToURL(request, '_blank');
			} catch (e:Error) {
				trace("Error occurred!");
			}
		}
		
		private function bucketListener(e:Event):void
		{
			this.dispatchEventWith(_linkDictionary[e.target]);
		}
		
		private function addButton(nameList:String, label:String, height:int, width:int, xPos:int, yPos:int, url:String="", func:Function=null):void
		{
			var button:Button = new Button();
			button = new Button();
			button.nameList.add(nameList);
			button.label = label;
			button.width = width;
			button.height = height;
			button.x = xPos;
			button.y = yPos;
			
			if(func != null){
				_linkDictionary[button] = url
				button.addEventListener(Event.TRIGGERED, func);
			}
			
			this.addChild(button);
		}
		
		private function addSectionButton(nameList:String, label:String, height:int, width:int, xPos:int, yPos:int, section:String, func:Function):void
		{
			var button:Button = new Button();
			button = new Button();
			button.nameList.add(nameList);
			button.label = label;
			button.width = width;
			button.height = height;
			button.x = xPos;
			button.y = yPos;
			
			_linkDictionary[button] = section
			button.addEventListener(Event.TRIGGERED, func);
						
			this.addChild(button);
		}
		
		public function getUser():void
		{
			checkCounts();
			
			selectAd.dispatch(DisplayAd.TYPE_AD_1);
			
			if(_isStartUp){
				dispatchEventWith(TricoEvent.START_APP);
			}
			
		}
		
		public function setDisplayAd(ad:DisplayAd):void
		{
			trace("HomeView :: setDisplayAd("+ad.adID+")");
			_adSpace.setAd(ad);
			_adSpace.clicked.add(adClicked);
			_adSpace.exclusive.add(exclusiveClicked);
		}
		
		private function exclusiveClicked(adID:int):void
		{
			clickAd.dispatch(adID, DisplayAd.TYPE_AD_1);
			_adSpace.exclusive.remove(exclusiveClicked);
			dispatchEventWith(TricoEvent.OFFER_CLICKED, false, adID); 
		}
		
		private function adClicked():void
		{
			_adSpace.clicked.remove(adClicked);
			clickAd.dispatch(_adSpace.displayAd.adID, DisplayAd.TYPE_AD_1);
		}
		
		override protected function draw():void
		{
			
		}
		
	}
	
}