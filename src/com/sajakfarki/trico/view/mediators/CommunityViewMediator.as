package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.view.components.CommunityView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class CommunityViewMediator extends StarlingMediator
	{
		[Inject]
		public var _view:CommunityView;
		
		[Inject]
		public var _adService:IAdService;
		
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		[Inject]
		public var _appModel:AppModel
		
				
		public function CommunityViewMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			//_view.appModel
			_view.selectAd.add(selectAdListener);
			_view.clickAd.add(clickAdListener);
			
			
			_view.addCommunities(_appModel.communityLinks, _appModel.tricoUser.community);

			
			_displayAdSignal.add(setDisplayAd);
		}
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setDisplayAd(ad);
		}
		
		private function selectAdListener(mediaType:String):void
		{
			_adService.selectAd(mediaType);
		}		
		
		override public function onRemove():void
		{
			_view.dispose()
			
			_adService.cleanup();
			
			_displayAdSignal.remove(setDisplayAd);
			
			_view.selectAd.remove(selectAdListener);
			_view.clickAd.remove(clickAdListener);
			
		}
		
	}
}