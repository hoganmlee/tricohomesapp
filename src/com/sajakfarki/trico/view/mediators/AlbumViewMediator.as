package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IAlbumService;
	import com.sajakfarki.trico.signal.UpdateAlbumSignal;
	import com.sajakfarki.trico.view.components.AlbumView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class AlbumViewMediator extends StarlingMediator
	{
		[Inject]
		public var _view:AlbumView;
		
		[Inject]
		public var _appModel:AppModel;
		
		[Inject]
		public var _albumService:IAlbumService;
		
		[Inject]
		public var _albumUpdated:UpdateAlbumSignal;
		
		
		public function AlbumViewMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			if(_appModel.albumList.length == 0)
			{
				_albumUpdated.add(albumUpdatedListener);
				_albumService.getAlbum(TricoHomesApp.BASE_URL);	
			}else{
				albumUpdatedListener();
			}
			
		}
		
		private function albumUpdatedListener():void
		{
			//pass app model reference
			_view.albumList = _appModel.albumList;
			
			if(_view.isInitialized){
				_view.loadAlbum();
			}
		}
		
		override public function onRemove():void
		{
			_view.dispose();
		}
		
	}
	
}