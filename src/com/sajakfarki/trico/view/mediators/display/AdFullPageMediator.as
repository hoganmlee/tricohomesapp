package com.sajakfarki.trico.view.mediators.display
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.signal.AdFullPageSignal;
	import com.sajakfarki.trico.view.components.AdFullPageView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class AdFullPageMediator extends StarlingMediator
	{
		[Inject]
		public var _view:AdFullPageView;
		
		[Inject]
		public var _adService:IAdService;
		
		[Inject]
		public var _fullPageSignal:AdFullPageSignal;
		
		
		public function AdFullPageMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			_adService.selectAd(DisplayAd.TYPE_AD_FULLPAGE);
			_fullPageSignal.add(setDisplayAd);
		}
				
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setAd(ad);
		}
		
		override public function onRemove():void
		{
			_view.dispose()
			_adService.cleanup();
			_fullPageSignal.remove(setDisplayAd);
		}
	}
}