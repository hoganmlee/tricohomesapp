package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.ILoginService;
	import com.sajakfarki.trico.signal.InvalidNameSignal;
	import com.sajakfarki.trico.signal.InvalidPasswordSignal;
	import com.sajakfarki.trico.signal.LoggedInSignal;
	import com.sajakfarki.trico.signal.LogoutSignal;
	import com.sajakfarki.trico.view.components.WelcomeView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class WelcomeViewMediator extends StarlingMediator
	{
		
		[Inject]
		public var _view:WelcomeView;
		
		[Inject]
		public var _loggedInSignal:LoggedInSignal;
		
		[Inject]
		public var _logoutSignal:LogoutSignal;
		
		[Inject]
		public var _invalidNameSignal:InvalidNameSignal;
		
		[Inject]
		public var _invalidPasswordSignal:InvalidPasswordSignal;
		
		[Inject]
		public var _loginService:ILoginService;
		
		[Inject]
		public var _adService:IAdService;
		
		
		private var _isInitialized:Boolean = false;
		
		
		public function WelcomeViewMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{			
			_view.checkLogin.add(checkLoginListener);
			
			_loggedInSignal.add(loggedInSignalListener);
			_logoutSignal.add(logoutSignalListener);
			_invalidPasswordSignal.add(invalidPasswordListener);
			_invalidNameSignal.add(invalidNameListener);
			
			//Check Shared Object for token, else 
			_loginService.checkToken();
			
		}
		
		private function invalidNameListener():void
		{
			_view.loginNameFail();
		}
		
		private function invalidPasswordListener():void
		{
			_view.loginPasswordFail();
		}
		
		private function logoutSignalListener():void
		{
			_loginService.expireToken();
		}
		
		private function checkLoginListener(name:String, pDub:String):void
		{
			_loginService.checkLogin(name, pDub);
		}
		
		private function loggedInSignalListener(loggedIn:Boolean):void
		{
			if(!loggedIn){
				
				if(!_isInitialized){
					
					_view.init();
					_isInitialized = true;
					
				}else{
					
					//_view.loginFail();
					//IMPLEMENTED INVALID NAME/PASSWORD SIGNALS
				}
				
			}else{
				
				_view.loginSuccess();
				
			}
			
		}
		
		override public function onRemove():void
		{
			_view.checkLogin.remove(checkLoginListener);
			_view.dispose()
			
			_loggedInSignal.remove(loggedInSignalListener);
			_logoutSignal.remove(logoutSignalListener);
		}
		
	}
}