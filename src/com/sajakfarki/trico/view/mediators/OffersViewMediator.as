package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.model.vo.WishList;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.IWishlistService;
	import com.sajakfarki.trico.signal.UpdateWishlistSignal;
	import com.sajakfarki.trico.view.components.OffersView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class OffersViewMediator extends StarlingMediator
	{
		
		[Inject]
		public var _view:OffersView;
		
		[Inject]
		public var _adService:IAdService;
		
		[Inject] 
		public var _wishlistService:IWishlistService;
		
		[Inject]
		public var _appModel:AppModel;
		
		[Inject]
		public var _updateExclusive:UpdateWishlistSignal;
		
		
		public function OffersViewMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{			
			_view.clickAd.add(clickAdListener);
			_view.addAd.add(addAdListener);
			_view.removeAd.add(removeAdListener);
									
			if(_appModel.checklist == null){
				_adService.getExclusive(DisplayAd.TYPE_AD_2);
				_updateExclusive.add(updateExclusiveListener);
			}else{
				updateExclusiveListener();
			}
			
		}
		
		private function removeAdListener(id:int):void
		{
			_wishlistService.removeWishlist(id);
		}
		
		private function addAdListener(id:int, type:String):void
		{
			_wishlistService.addWishlist(id, type);
		}
		
		private function updateExclusiveListener(wishlist:WishList=null):void
		{
			//pass app model reference
			_view.wishlist = _appModel.wishlist;
			
			if(_appModel.offerSelected)
			{
				_view.offerIndex = _appModel.offerSelected;
			}
			
			if(_view.isInitialized)
			{
				_view.loadWishlist();
			}
			
		}		
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		override public function onRemove():void
		{
			_view.dispose();
			
			_adService.cleanup();
			
			_view.clickAd.remove(clickAdListener);
			
		}
	}
}