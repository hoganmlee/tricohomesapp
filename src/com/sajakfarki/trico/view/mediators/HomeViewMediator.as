package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.CheckList;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.model.vo.WishList;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.IAlbumService;
	import com.sajakfarki.trico.service.IChecklistService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.signal.UpdateAlbumSignal;
	import com.sajakfarki.trico.signal.UpdateChecklistSignal;
	import com.sajakfarki.trico.signal.UpdateWishlistSignal;
	import com.sajakfarki.trico.view.components.HomeView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class HomeViewMediator extends StarlingMediator
	{
		[Inject]
		public var _view:HomeView;
			
		[Inject]
		public var _adService:IAdService;
		
		[Inject]
		public var _albumService:IAlbumService;
		
		[Inject]
		public var _checklistService:IChecklistService;
				
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		[Inject]
		public var _updateWishlist:UpdateWishlistSignal;
		
		[Inject]
		public var _updateChecklist:UpdateChecklistSignal;
		
		[Inject]
		public var _updateAlbum:UpdateAlbumSignal;
		
		
		public function HomeViewMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			if(_appModel.checklist == null){
				_checklistService.getChecklist();
				_updateChecklist.add(updateChecklistListener);
			}
						
			if(_appModel.wishlist == null){
				_adService.getExclusive(DisplayAd.TYPE_AD_2, TricoHomesApp.BASE_URL + "AdMedia/");
				_updateWishlist.add(updateWishlistListener);
			}
			
			if(_appModel.albumList == null){
				_updateAlbum.add(albumUpdatedListener);
				_albumService.getAlbum(TricoHomesApp.BASE_URL);	
			}
			
			_view.selectAd.add(selectAdListener);
			_view.clickAd.add(clickAdListener);		
			
			_displayAdSignal.add(setDisplayAd);
			
			checkData();
			
		}
		
		private function albumUpdatedListener():void
		{
			checkData();
		}
		
		private function updateChecklistListener(checklist:CheckList):void
		{
			checkData();
		}
		
		private function updateWishlistListener(wishlist:WishList):void
		{
			checkData();
		}
		
		private function checkData():void
		{
			if(_appModel.checklist && _appModel.wishlist && _appModel.albumList){
				
				_view.isConnected = true;
				
				var checklistCount:String;
				checklistCount = _appModel.checklist.tally + " / " + _appModel.checklist.total;
				
				var exclusiveCount:String;
				var count:int = 0;
				for each (var i:int in _appModel.wishlist.userDictionary) 
				{
					count++;
				}
				
				exclusiveCount = count + " / " + _appModel.wishlist.total;
				
				var photoCount:String = _appModel.albumList.length.toString();
				
				_view.addCounts(checklistCount, exclusiveCount, photoCount);
				
				_updateChecklist.remove(updateChecklistListener);
				_updateWishlist.remove(updateWishlistListener);
				
			}
			
		}
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setDisplayAd(ad);
		}
		
		private function selectAdListener(mediaType:String):void
		{
			_adService.selectAd(mediaType);
		}	
		
		override public function onRemove():void
		{
			_view.dispose();
			
			_adService.cleanup();
			
			_displayAdSignal.remove(setDisplayAd);
			_updateChecklist.remove(updateChecklistListener);
			_updateWishlist.remove(updateWishlistListener);
			_view.selectAd.remove(selectAdListener);
			_view.clickAd.remove(clickAdListener);
			
		}
		
	}
	
}