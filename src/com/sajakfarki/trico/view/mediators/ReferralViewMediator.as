package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IEmailService;
	import com.sajakfarki.trico.signal.EmailSentSignal;
	import com.sajakfarki.trico.view.components.ReferralView;
	import com.sajakfarki.trico.view.components.display.ReferralForm;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class ReferralViewMediator extends StarlingMediator
	{
		[Inject]
		public var _view:ReferralView;
		
		[Inject]
		public var _emailService:IEmailService;
		
		[Inject]
		public var _emailSentSignal:EmailSentSignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		public function ReferralViewMediator()
		{
			super();
		}
		override public function onRegister():void
		{	
			_view.sendEmail.add(dispatchEmail);
			_emailSentSignal.add(emailSentListener);
			
			ReferralForm.EMAIL_SUBJECT = _appModel.tricoUser.name + " ( " + _appModel.tricoUser.email + " ) Referral";
			
		}
		
		private function emailSentListener(value:Boolean):void
		{
			_view.emailSent(value);
		}
		
		private function dispatchEmail(subject:String, emailList:String, body:String, isHTML:Boolean):void
		{
			_emailService.sendEmail(subject, emailList, body, isHTML);
		}		
		
		override public function onRemove():void
		{
			_emailSentSignal.remove(emailSentListener);
			_emailService.cleanup();
		}
	}
}