package com.sajakfarki.trico.view.mediators.promotions
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.view.components.promotions.GlobalFestView;
	import com.sajakfarki.trico.view.components.promotions.TricoCentreView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class TricoCentreMediator extends StarlingMediator
	{
		[Inject]
		public var _view:TricoCentreView;
				
		[Inject]
		public var _adService:IAdService;
				
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		public function TricoCentreMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			//_view.appModel
			_view.selectAd.add(selectAdListener);
			_view.clickAd.add(clickAdListener);
			
			_displayAdSignal.add(setDisplayAd);
		}
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setDisplayAd(ad);
		}
		
		private function selectAdListener(mediaType:String):void
		{
			_adService.selectAd(mediaType);
		}		
		
		override public function onRemove():void
		{
			_view.dispose()
			
			_adService.cleanup();
			
			_displayAdSignal.remove(setDisplayAd);
			
			_view.selectAd.remove(selectAdListener);
			_view.clickAd.remove(clickAdListener);
			
		}
		
	}
	
}