package com.sajakfarki.trico.view.mediators.promotions
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.IEmailService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.signal.EmailSentSignal;
	import com.sajakfarki.trico.view.components.display.EquityForm;
	import com.sajakfarki.trico.view.components.promotions.EquitySaverView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class EquitySaverMediator extends StarlingMediator
	{
		[Inject]
		public var _view:EquitySaverView;
		
		[Inject]
		public var _emailService:IEmailService;
		
		[Inject]
		public var _emailSentSignal:EmailSentSignal;
		
		[Inject]
		public var _adService:IAdService;
		
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		public function EquitySaverMediator()
		{
			super();
		}
		override public function onRegister():void
		{	
			_view.sendEmail.add(dispatchEmail);
			_emailSentSignal.add(emailSentListener);
			
			_view.selectAd.add(selectAdListener);
			_view.clickAd.add(clickAdListener);
			
			_displayAdSignal.add(setDisplayAd);
			
			EquityForm.EMAIL_SUBJECT = _appModel.tricoUser.name + " ( " + _appModel.tricoUser.email + " ) Equity Saver";
		}
		
		private function emailSentListener(value:Boolean):void
		{
			_view.emailSent(value);
		}
		
		private function dispatchEmail(subject:String, emailList:String, body:String, isHTML:Boolean):void
		{
			_emailService.sendEmail(subject, emailList, body, isHTML);
		}
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setDisplayAd(ad);
		}
		
		private function selectAdListener(mediaType:String):void
		{
			_adService.selectAd(mediaType);
		}		
		
		override public function onRemove():void
		{
			_view.dispose()
			
			_adService.cleanup();
			
			_displayAdSignal.remove(setDisplayAd);
			
			_view.selectAd.remove(selectAdListener);
			_view.clickAd.remove(clickAdListener);
			
			_emailSentSignal.remove(emailSentListener);
			_emailService.cleanup();
		}
		
	}
	
}

