package com.sajakfarki.trico.view.mediators
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.CheckList;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.service.IChecklistService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.signal.UpdateChecklistSignal;
	import com.sajakfarki.trico.view.components.ChecklistView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class ChecklistViewMediator extends StarlingMediator
	{
		[Inject]
		public var _view:ChecklistView;
		
		[Inject]
		public var _checklistService:IChecklistService;
		
		[Inject]
		public var _adService:IAdService;
		
		[Inject]
		public var _updateChecklist:UpdateChecklistSignal;
		
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		
		public function ChecklistViewMediator()
		{
			super();

		}
		
		override public function onRegister():void
		{
			if(_appModel.checklist == null){
				
				_checklistService.getChecklist();
				_updateChecklist.add(updateChecklistListener);
				
			}else{
				
				updateChecklistListener();
				
			}
			
			_view.checklistUpdate.add(checklistUpdateListener);
			//_view.appModel
			_view.selectAd.add(selectAdListener);
			_view.clickAd.add(clickAdListener);
			
			_displayAdSignal.add(setDisplayAd);
		}
		
		private function clickAdListener(id:String, mediaType:String):void
		{
			_adService.clickAd(id, mediaType);
		}
		
		private function setDisplayAd(ad:DisplayAd):void
		{
			_view.setDisplayAd(ad);
		}
		
		private function selectAdListener(mediaType:String):void
		{
			_adService.selectAd(mediaType);
		}		
		
		private function checklistUpdateListener(groupID:int, itemID:int, selected:Boolean):void
		{
			_checklistService.setCheck(itemID, selected);
			
			
			if(selected){
				_appModel.checklist.tally++;
			}else{
				_appModel.checklist.tally--;
			}
			//Update internal model
			_appModel.checklist.groupDictionary[groupID].itemDictionary[itemID].checked = selected;
//			_appModel.checklist.groupsDictionary[groupID]
			
		}
		
		private function updateChecklistListener(checklist:CheckList=null):void
		{
			//pass app model reference
			_view.checklist = _appModel.checklist;
			
			if(_view.isInitialized){
				_view.loadChecklist();
			}
		}
				
		override public function onRemove():void
		{
			_view.dispose()
				
			_adService.cleanup();
			
			_displayAdSignal.remove(setDisplayAd);
			
			_view.selectAd.remove(selectAdListener);
			_view.clickAd.remove(clickAdListener);
			
			_updateChecklist.remove(updateChecklistListener);
			
		}
	}
}