package com.sajakfarki.trico.service
{
	import com.sajakfarki.openpdf.OpenPDFExtension;

	public class PDFService
	{
		private static var _pdfService:OpenPDFExtension;
		
		public static function openPDF(file:String):void
		{
			if(_pdfService == null) _pdfService = new OpenPDFExtension(null);
			_pdfService.openPDF(file);
		}
		
	}
	
}