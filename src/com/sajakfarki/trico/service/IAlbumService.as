package com.sajakfarki.trico.service
{
	public interface IAlbumService
	{
		function getAlbum(baseURL:String=""):void;
		function cleanup():void;
	}
}