package com.sajakfarki.trico.service
{
	
	public interface ILoginService
	{
	
		function checkToken():void;
		function checkLogin(name:String, pDub:String):void;
		function expireToken():void;
		
		function cleanup():void;
		
	}
	
}