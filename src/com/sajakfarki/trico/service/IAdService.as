package com.sajakfarki.trico.service
{
	public interface IAdService
	{
		function selectAd(mediaType:String):void;
		function getExclusive(mediaType:String, baseURL:String = ""):void;
		function getAdvertiser(id:String):void;
		function clickAd(id:String, mediaType:String):void;
		function cleanup():void;
		
	}
}