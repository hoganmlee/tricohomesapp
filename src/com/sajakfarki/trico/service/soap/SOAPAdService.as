package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.Advertiser;
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import com.sajakfarki.trico.model.vo.WishList;
	import com.sajakfarki.trico.model.vo.WishListFinder;
	import com.sajakfarki.trico.model.vo.WishListGroup;
	import com.sajakfarki.trico.service.IAdService;
	import com.sajakfarki.trico.signal.AdDisplaySignal;
	import com.sajakfarki.trico.signal.AdFullPageSignal;
	import com.sajakfarki.trico.signal.AdInfoSignal;
	import com.sajakfarki.trico.signal.UpdateWishlistSignal;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPAdService implements IAdService
	{
//		public static var MEDIA_URL:String = "http://trico.sajakfarki.com/AdMedia/";
		
		private var _baseURL:String;
		
		[Inject]
		public var _appModel:AppModel;
		
		[Inject]
		public var _displayAdSignal:AdDisplaySignal;
		
		[Inject]
		public var _fullpageAdSignal:AdFullPageSignal;
		
		[Inject]
		public var _adInfoSignal:AdInfoSignal;
		
		[Inject]
		public var _updateWishlistSignal:UpdateWishlistSignal;
		
		
		private var _adService:WebService;
		
		private var _exclusiveOperation:AbstractOperation;
		private var _displayOperation:AbstractOperation;
		private var _advertiserOperation:AbstractOperation;
		private var _clickOperation:AbstractOperation;
		
		
		private var _exclusiveToken:AsyncToken;
		private var _displayToken:AsyncToken;
		private var _adToken:AsyncToken;
		
		
		public function SOAPAdService()
		{
			_adService = new WebService();
			_adService.addEventListener(LoadEvent.LOAD, BuildAdRequest);
			_adService.addEventListener(FaultEvent.FAULT, BuildAdFault);
			_adService.loadWSDL("http://trico.sajakfarki.com/Services/AdService.asmx?WSDL");
			
		}
		
		protected function BuildAdFault(event:FaultEvent):void
		{
			//TODO: retry connection
			
		}
		
		protected function BuildAdRequest(event:LoadEvent):void
		{
			trace("SOAPAdService :: BuildAdRequest connected");
			trace();
			_adService.removeEventListener(LoadEvent.LOAD, BuildAdRequest);
			_adService.removeEventListener(FaultEvent.FAULT, BuildAdFault);
		}
		
		
		
		/****************************************************
		 * 
		 *   Get exclusive offers
		 * 
		 ****************************************************/
		public function getExclusive(mediaType:String, baseURL:String = ""):void
		{
			_baseURL = baseURL;
			trace("SOAPAdService :: getExclusive " + mediaType);
			trace();
			
			_exclusiveOperation = _adService.getOperation("GetAllExclusiveOffers");
			_exclusiveOperation.arguments = [_appModel.token, mediaType];
			
			_exclusiveToken = _exclusiveOperation.send();
			_exclusiveToken.addResponder(new Responder(DisplayExclusiveResult, DisplayAdError)); 
			
		}
		
		private function DisplayExclusiveResult(evt:ResultEvent):void
		{
			//
			if(evt.result != null){
				
				var wishlist:WishList = new WishList();
				
				for each (var i:* in evt.result) 
				{
					
					var wishlistGroup:WishListGroup;
					
					if(wishlist.groupDictionary[i.AdvertiserName] == null){
						
						wishlistGroup = new WishListGroup();
						
						wishlistGroup.advertiserName = i.AdvertiserName;
						wishlistGroup.adContactName = i.AdvertiserContactName;
						wishlistGroup.adContactEmail = i.AdvertiserContactEmail;
						wishlistGroup.adContactPhone = i.AdvertiserContactPhone;
						
						wishlist.groupDictionary[wishlistGroup.advertiserName] = wishlistGroup;
						wishlist.groups.push(wishlistGroup);
						
					}else{
						wishlistGroup = wishlist.groupDictionary[i.AdvertiserName];
					}
					
					var displayAd:DisplayAd = new DisplayAd();
					displayAd.advertiserName = i.AdvertiserName;
					displayAd.adID = i.AdID;
					displayAd.media = _baseURL + i.Media;
					displayAd.actionURL = i.ActionURL;
					displayAd.isExclusive = i.ExclusiveOffer;
					displayAd.exclusiveTitle = i.ExclusiveOfferTitle;
					displayAd.exclusiveDescription = i.ExclusiveOfferDescription;
					
					wishlistGroup.ads.push(displayAd);
					wishlistGroup.adsDictionary[displayAd.adID] = displayAd;
					
					// If it's part of the users wishlist
					if(i.WishList){
						
						var finder:WishListFinder = new WishListFinder();
						finder.group = wishlistGroup.advertiserName;
						finder.adID = displayAd.adID;
												
						wishlist.userDictionary[displayAd.adID] = finder;
																		
					}
					
					wishlist.total++;
									
				}
				
				_appModel.wishlist = wishlist;
								
				trace("SOAPAdService :: wishlist success! = " + wishlist);
				trace();
				
				_updateWishlistSignal.dispatch(wishlist);
				
			}
			
		}
		
		/****************************************************
		 * 
		 *   Select Advertisement by Type
		 * 
		 ****************************************************/
		public function selectAd(mediaType:String):void
		{
			trace("SOAPAdService :: selectAd " + mediaType);
			trace();

			_displayOperation = _adService.getOperation("SelectAdToDisplay");
			_displayOperation.arguments = [_appModel.token, mediaType];
			
			_displayToken = _displayOperation.send();
			_displayToken.addResponder(new Responder(DisplaySelectResult, DisplayAdError)); 
		}
		
		private function DisplayAdError(evt:FaultEvent):void
		{
			trace("error");
		}
		
		private function DisplaySelectResult(evt:ResultEvent):void
		{
						
			if(evt.result != null){
				
				var ad:DisplayAd = new DisplayAd();
				ad.adID = evt.result.AdID;
				ad.media = _baseURL + evt.result.Media;
				ad.actionURL = evt.result.ActionURL;
				ad.isExclusive = evt.result.ExclusiveOffer;
				ad.exclusiveTitle = evt.result.ExclusiveOfferTitle;
				ad.exclusiveDescription = evt.result.ExclusiveOfferDescription;
				
				//ad.adName = evt.result.AdvertiserName;
				//ad.adContactName = evt.result.AdvertiserContactName;
				//ad.adContactEmail = evt.result.AdvertiserContactEmail;
				//ad.adContactPhone = evt.result.AdvertiserContactPhone;
				
				trace("SOAPAdService :: result = " + ad.media);
				trace();
				
				if(evt.result.MediaType == DisplayAd.TYPE_AD_FULLPAGE){
					_fullpageAdSignal.dispatch(ad);
				}else{
					_displayAdSignal.dispatch(ad);
				}
				
			}else{
				
				trace("SOAPAdService :: get ad fail!");
				
			}
			
		}
		
		/****************************************************
		 * 
		 *   Get extended Advertisement by ID
		 * 
		 ****************************************************/
		public function getAdvertiser(id:String):void
		{
			_displayOperation = _adService.getOperation("SelectAdToDisplay");
			_displayOperation.arguments = [_appModel.token, id];
			
			_displayToken = _displayOperation.send();
			_displayToken.addResponder(new Responder(GetSelectResult, DisplayAdError)); 
		}
				
		private function GetSelectResult(evt:ResultEvent):void
		{
			
			if(evt.result != null){
				
				var advertiser:Advertiser = new Advertiser();
				advertiser.advertiserID = evt.result.AdvertiserID;
				advertiser.name = evt.result.Name;
				advertiser.memo = evt.result.Memo;
				advertiser.contactName = evt.result.ContactName;
				advertiser.contactEmail = evt.result.ContactEmail;
				advertiser.contactPhone = evt.result.ContactPhone;
				advertiser.contactNotes = evt.result.ContactNotes;
				
				_adInfoSignal.dispatch(advertiser);
				
			}else{
				
				trace("SOAPAdService :: get ad fail!");
				
			}
			
		}
		
		/****************************************************
		 * 
		 *   Click Ad by ID
		 * 
		 ****************************************************/
		public function clickAd(id:String, mediaType:String):void
		{
			trace("SOAPAdService :: clickAd");
			
			_clickOperation = _adService.getOperation("RecordAdClick");
			_clickOperation.arguments = [_appModel.token, id, mediaType];
					
			_clickOperation.send();
			
		}
		
		public function cleanup():void
		{
			trace("SOAPAdSerivce :: cleanup()") ;
			if(_displayOperation){
				_displayOperation.cancel();
			}
			
			if(_displayToken != null){
				_displayToken = null;
			}
		}
	}
}