package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.CheckList;
	import com.sajakfarki.trico.model.vo.CheckListGroup;
	import com.sajakfarki.trico.model.vo.CheckListItem;
	import com.sajakfarki.trico.service.IChecklistService;
	import com.sajakfarki.trico.signal.UpdateChecklistSignal;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPChecklistService implements IChecklistService
	{
		
		private var _webService:WebService;
		private var _serviceOperation:AbstractOperation;
		
		[Inject]
		public var _checklistUpdate:UpdateChecklistSignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		public function SOAPChecklistService()
		{
			_webService =  new WebService();
			_webService.addEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.addEventListener(FaultEvent.FAULT, BuildServiceFault);
			_webService.loadWSDL("http://trico.sajakfarki.com/Services/ChecklistService.asmx?WSDL");
		}
		
		protected function BuildServiceFault(event:FaultEvent):void
		{
			//TODO: retry connection
			
		}
		
		private function BuildServiceRequest(evt:LoadEvent):void
		{
			trace("SOAPChecklistService :: BuildAdRequest connected");
			trace();
			_webService.removeEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.removeEventListener(FaultEvent.FAULT, BuildServiceFault);
		}
		
		
		/****************************************************
		 * 
		 *   Get Checklist
		 * 
		 ****************************************************/
		public function getChecklist():void
		{
			trace("SOAPChecklistService :: GetChecklistGroups");
			trace();
			
			_serviceOperation = _webService.getOperation("GetChecklistGroups");
			_serviceOperation.arguments = [_appModel.token];
			
			var token:AsyncToken = _serviceOperation.send();
			token.addResponder(new Responder(DisplayCheckChangeResult, DisplayCheckError));
		}
		
		private function DisplayCheckError(evt:FaultEvent):void
		{
			trace("error");
		}
		
		private function DisplayCheckChangeResult(evt:ResultEvent):void
		{
			
			if(evt.result != null){
				
				var checklist:CheckList = new CheckList();
				
				for each (var i:* in evt.result) 
				{
					var checklistGroup:CheckListGroup = new CheckListGroup();
					
					checklistGroup.groupID = i.ChecklistGroupID;
					checklistGroup.orderID = i.OrderID;
					checklistGroup.groupName = i.Name;
					
					checklist.groupDictionary[checklistGroup.groupID] = checklistGroup;
				/*	
					trace(i.OrderID);
					trace(i.ChecklistGroupID);
					trace(i.Name);
					trace("===============");*/
					
					for each (var j:* in i.ChecklistItems) 
					{
						var checklistItem:CheckListItem = new CheckListItem();
						
						checklistItem.itemID = j.ChecklistItemID;
						checklistItem.groupID = j.ChecklistGroupID;
						checklistItem.itemName = j.Name;
						checklistItem.orderID = j.OrderID;
						checklistItem.description = j.Description;
						checklistItem.checked = j.Checked;
						
						if(checklistItem.checked)
						{
							checklist.tally++;
						}
						
						checklist.total++;
						checklistGroup.itemDictionary[checklistItem.itemID] = checklistItem;
						
//						trace(checklistItem.checked);
						
					/*	trace(j.ChecklistItemID);
						trace(j.ChecklistGroupID);
						trace(j.Name);
						trace(j.OrderID);
						trace(j.Description);
						trace(j.Checked);
						trace(">>>>>>>>>>>>>>>>>>>>>");*/
						
						checklistGroup.items.push(checklistItem);
					}
					
					checklist.groups.push(checklistGroup);
					
				}
				
				_appModel.checklist = checklist;
				
				trace("SOAPChecklistService :: checklist success! " + _appModel.checklist);
				trace();
				
				_checklistUpdate.dispatch(checklist);
				
			}else{
				
				trace("SOAPChecklistService :: checklist fail!");
				
			}
			
		}
		
		/****************************************************
		 * 
		 *   Set Checklist.selected
		 * 
		 ****************************************************/
		public function setCheck(id:int, bool:Boolean):void
		{
			trace("SOAPChecklistService :: ToggleChecklistItem");
			trace();
			
			_serviceOperation = _webService.getOperation("ToggleChecklistItem");
			_serviceOperation.arguments = [_appModel.token, id, bool];
			
			_serviceOperation.send();
			
		}
		
		public function cleanup():void
		{
			trace("SOAPChecklistSerivce :: cleanup()") ;
			
		}
			
	}
	
}

