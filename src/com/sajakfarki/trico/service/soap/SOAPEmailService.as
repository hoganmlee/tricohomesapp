package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IEmailService;
	import com.sajakfarki.trico.signal.EmailSentSignal;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPEmailService implements IEmailService
	{
		[Inject]
		public var _appModel:AppModel;
		[Inject]
		public var _sentEmail:EmailSentSignal;
		
		private var _webService:WebService;
		private var _serviceOperation:AbstractOperation;
		
		public function SOAPEmailService()
		{
			_webService =  new WebService();
			_webService.addEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.addEventListener(FaultEvent.FAULT, BuildServiceFault);
			_webService.loadWSDL("http://trico.sajakfarki.com/Services/EmailService.asmx?WSDL");
		}
		
		protected function BuildServiceFault(event:FaultEvent):void
		{
			//TODO: retry connection
		}
		
		private function BuildServiceRequest(evt:LoadEvent):void
		{
			trace("SOAPEmailService :: BuildServiceRequest connected");
			trace();
			_webService.removeEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.removeEventListener(FaultEvent.FAULT, BuildServiceFault);
		}
		
		public function sendEmail(subject:String, emailTo:String, body:String, isHTML:Boolean):void
		{
			trace("sending with app token = " + _appModel.token, isHTML);
			
			trace(body);
			
			_serviceOperation = _webService.getOperation("SendEmail");
			_serviceOperation.arguments = [_appModel.token, subject, emailTo, isHTML, body];
			
			var token:AsyncToken = _serviceOperation.send();
			token.addResponder(new Responder(DisplayChangeResult, DisplayError));
		}
		
		private function DisplayError(evt:FaultEvent):void
		{
			trace("error");
			_sentEmail.dispatch(false);
		}
		
		private function DisplayChangeResult(evt:ResultEvent):void
		{
			if(evt.result != null){
				trace("SOAPEmailService < RESULT >>> " + evt.result);
				_sentEmail.dispatch(Boolean(evt.result));
			}
		}
		
		public function cleanup():void
		{
			
		}
		
	}
	
}