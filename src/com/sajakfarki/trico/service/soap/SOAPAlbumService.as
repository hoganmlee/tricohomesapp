package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.AlbumItem;
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IAlbumService;
	import com.sajakfarki.trico.signal.UpdateAlbumSignal;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPAlbumService implements IAlbumService
	{
		private var _baseURL:String;
		
		private var _thumbService:String = "http://trico.sajakfarki.com/Services/GalleryThumbnails.ashx?";
		
		private var _webService:WebService;
		private var _serviceOperation:AbstractOperation;
				
		[Inject]
		public var _albumUpdateSignal:UpdateAlbumSignal
		
		[Inject]
		public var _appModel:AppModel;
		
		
		public function SOAPAlbumService()
		{
			_webService =  new WebService();
			_webService.addEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.addEventListener(FaultEvent.FAULT, BuildServiceFault);
			_webService.loadWSDL("http://trico.sajakfarki.com/Services/GalleryService.asmx?WSDL");
		}
				
		protected function BuildServiceFault(event:FaultEvent):void
		{
			//TODO: retry connection	
		}
		
		private function BuildServiceRequest(evt:LoadEvent):void
		{
			trace("SOAPAlbumService :: BuildAdRequest connected");
			trace();
			_webService.removeEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.removeEventListener(FaultEvent.FAULT, BuildServiceFault);
		}
		
		/****************************************************
		 * 
		 *   Get Album Images
		 * 
		 ****************************************************/
		public function getAlbum(baseURL:String = ""):void
		{
			_baseURL = baseURL;
			
			trace("SOAPAlbumService :: album GetCustomerGalleryImages");
			trace();
				
			_serviceOperation = _webService.getOperation("GetCustomerGalleryImages");
			_serviceOperation.arguments = [_appModel.token];
			
			var token:AsyncToken = _serviceOperation.send();
			token.addResponder(new Responder(DisplayAlbumResult, DisplayAlbumError));
		}
		
		private function DisplayAlbumError(evt:FaultEvent):void
		{
			trace("error");
		}
		
		private function DisplayAlbumResult(evt:ResultEvent):void
		{
			
			if(evt.result != null){
								
				for each (var i:* in evt.result) 
				{
					
					trace("SOAPAlbumService album result = " + i);
					var albumItem:AlbumItem = new AlbumItem();
						
					albumItem.imageURL = _thumbService + "securitytoken="+_appModel.token+"&filepath="+i+"&maxwidth="+1280+"&maxheight="+740;
					albumItem.thumbURL = _thumbService + "securitytoken="+_appModel.token+"&filepath="+i+"&maxwidth="+100+"&maxheight="+100;
					
					if(_appModel.albumList == null) _appModel.albumList = new Vector.<AlbumItem>();
					
					_appModel.albumList.push(albumItem);
					
				}
								
				_albumUpdateSignal.dispatch();
				
				trace("SOAPAlbumService :: album success! " + _appModel.token);
				trace();
				
			}else{
				
				trace("SOAPAlbumService :: album fail!");
				
			}
			
		}
		
		public function cleanup():void
		{
			trace("SOAPAlbumSerivce :: cleanup()") ;
			

		}
		
	}
	
}