package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.IWishlistService;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPWishlistService implements IWishlistService
	{
		[Inject]
		public var _appModel:AppModel;
		
		private var _webService:WebService;
		private var _serviceOperation:AbstractOperation;
		
		public function SOAPWishlistService()
		{
			_webService =  new WebService();
			_webService.addEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.addEventListener(FaultEvent.FAULT, BuildServiceFault);
			_webService.loadWSDL("http://trico.sajakfarki.com/Services/WishlistService.asmx?WSDL");
		}
		
		
		protected function BuildServiceFault(event:FaultEvent):void
		{
			//TODO: retry connection
		}
		
		protected function BuildServiceRequest(event:LoadEvent):void
		{
			trace("SOAPWishlistService :: BuildServiceRequest connected");
			trace();
			_webService.removeEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.removeEventListener(FaultEvent.FAULT, BuildServiceFault);
		}
		
		
		/****************************************************
		 * 
		 *   ** See AdService
		 * 
		 ****************************************************/
		/*public function getWishlist():void
		{
			trace("SOAPWishlistService :: getWishlist ");
			_serviceOperation = _webService.getOperation("GetCustomerWishlist");
			_serviceOperation.arguments = [_appModel.token];
			
			var token:AsyncToken = _serviceOperation.send();
			token.addResponder(new Responder(DisplayWishlistResult, DisplayWishlistError)); 
		}
		
		private function DisplayWishlistError(evt:FaultEvent):void
		{
			trace("error");
		}
		
		private function DisplayWishlistResult(evt:ResultEvent):void
		{
			
			if(evt.result != null){
				
			}else{
									
			}
			
		}*/
		
		/****************************************************
		 * 
		 *   Add Wishlist advert
		 * 
		 ****************************************************/
		public function addWishlist(adID:int, mediaType:String):void
		{
			_serviceOperation = _webService.getOperation("AddToWishlist");
			trace("SOAPWishlistService.AddToWishlist = " + _appModel.token);
			_serviceOperation.arguments = [_appModel.token, adID, mediaType];
			
			_serviceOperation.send();
		}
		
		/****************************************************
		 * 
		 *   Remove Wishlist advert
		 * 
		 ****************************************************/
		public function removeWishlist(adID:int):void
		{
			_serviceOperation = _webService.getOperation("RemoveFromWishlist");
			_serviceOperation.arguments = [_appModel.token, adID];
			
			_serviceOperation.send();
		}
		
		public function cleanup():void
		{
			
		}
	}
}