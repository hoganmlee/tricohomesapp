package com.sajakfarki.trico.service.soap
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.model.vo.CommunityLink;
	import com.sajakfarki.trico.model.vo.TricoUser;
	import com.sajakfarki.trico.service.ILoginService;
	import com.sajakfarki.trico.signal.InvalidNameSignal;
	import com.sajakfarki.trico.signal.InvalidPasswordSignal;
	import com.sajakfarki.trico.signal.LoggedInSignal;
	
	import flash.net.SharedObject;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.WebService;
	
	public class SOAPLoginService implements ILoginService
	{
		private var STORAGE_KEY:String = "trico.homes.store";
		
		private var _webService:WebService;
		private var _serviceOperation:AbstractOperation;
		
//		private var _isConnected:Boolean = false;
		
		
		//SHARED OBJECT
		private var _shareObj:Object;
		
		
		[Inject]
		public var _loggedInSignal:LoggedInSignal
		
		[Inject]
		public var _invalidName:InvalidNameSignal;
		
		[Inject]
		public var _invalidPassword:InvalidPasswordSignal;
		
		[Inject]
		public var _appModel:AppModel;
		
		
		public function SOAPLoginService()
		{
			_shareObj = SharedObject.getLocal(STORAGE_KEY);
			
			_webService =  new WebService();
			_webService.addEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.addEventListener(FaultEvent.FAULT, BuildServiceFault);
			_webService.loadWSDL("http://trico.sajakfarki.com/Services/AuthenticationService.asmx?WSDL");
		}
		
		protected function BuildServiceFault(event:FaultEvent):void
		{
//			_isConnected = false;
			
			
			//TODO: retry connection
			
			
		}
		
		private function BuildServiceRequest(evt:LoadEvent):void
		{
			trace("SOAPLoginService :: BuildAdRequest connected");
			_webService.removeEventListener(LoadEvent.LOAD, BuildServiceRequest);
			_webService.removeEventListener(FaultEvent.FAULT, BuildServiceFault);
			
//			_isConnected = true;
		}
				
		public function checkToken():void
		{
			
			trace("CHECK TOKEN!  :::::::::::::::::::::::::::::::::::::");
			if(_shareObj.data.token != undefined)
			{
				var tricoUser:TricoUser = new TricoUser();
				tricoUser.id = _shareObj.data.id;
				tricoUser.name = _shareObj.data.user;
				tricoUser.community = _shareObj.data.community;
				tricoUser.email = _shareObj.data.email;
				
				_appModel.tricoUser = tricoUser;
				_appModel.token = _shareObj.data.token;
				
//				trace("_shareObj.data.communityLinks = " , _shareObj.data.communityLinks);
//				trace("CHECK TOKEN!  :::::::::::::::::::::::::::::::::::::");
				if(_shareObj.data.communityLinks)
				{
					for (var i:int = 0; i <  _shareObj.data.communityLinks.length; i++) 
					{
						var community:CommunityLink = new CommunityLink();
						community.id = _shareObj.data.communityLinks[i].id;
						community.description = _shareObj.data.communityLinks[i].description;
						community.heading = _shareObj.data.communityLinks[i].heading;
						community.community = _shareObj.data.communityLinks[i].community;
						community.link = _shareObj.data.communityLinks[i].link;
						
						trace("community.link = " + community.link);
						
						_appModel.communityLinks.push(community);
					}
				}
				
				_loggedInSignal.dispatch(true);
				
			}else{
				_loggedInSignal.dispatch(false);
			}
			
		}
		
		public function checkLogin(name:String, pDub:String):void
		{
			trace("SOAPLoginService :: checkLogin ");
			
			trace(_webService.canLoadWSDL());
			
			_serviceOperation = _webService.getOperation("AuthenticateExtended");
			_serviceOperation.arguments = [name, pDub];
			
			var token:AsyncToken = _serviceOperation.send();
			token.addResponder(new Responder(DisplayLoginResult, DisplayLoginError)); 
		}
		
		private function DisplayLoginError(evt:FaultEvent):void
		{
			trace("error");
		}
		
		private function DisplayLoginResult(evt:ResultEvent):void
		{
			
			if(evt.result.Success){
				
				var tricoUser:TricoUser = new TricoUser();
				tricoUser.id = evt.result.Customer.CustomerNumber;
				tricoUser.name = evt.result.Customer.Name;
				tricoUser.community = evt.result.Customer.Community;
				tricoUser.email = evt.result.Customer.Email;				
				
				for each (var i:* in evt.result.Amenities) 
				{
					trace(i.URLID, i.Community, i.URLHeading, i.URLDescription, i.URL);
					var community:CommunityLink = new CommunityLink();
					
					community.id = i.URLID;
					community.community = i.Community;
					community.heading = i.URLHeading;
					community.description = i.URLDescription;
					community.link = i.URL;
										
					_appModel.communityLinks.push(community);
					
				}
				
				_shareObj.clear();
				_shareObj.data.communityLinks = _appModel.communityLinks;
				
//				trace("_shareObj.data.communityLinks = " + _shareObj.data.communityLinks);
				
				/*<Amenities>
						 <Amenity>
						   <URLID>int</URLID>
						   <Community>string</Community>
						   <URLHeading>string</URLHeading>
						   <URLDescription>string</URLDescription>
						   <URL>string</URL>
						 </Amenity>
						 <Amenity>
						   <URLID>int</URLID>
						   <Community>string</Community>
						   <URLHeading>string</URLHeading>
						   <URLDescription>string</URLDescription>
						   <URL>string</URL>
						 </Amenity>
				   </Amenities>*/

				_shareObj.data.token = _appModel.token = evt.result.SecurityToken;
				_shareObj.data.email = tricoUser.email;
				_shareObj.data.user = tricoUser.name;
				_shareObj.data.id = tricoUser.id;
				_shareObj.data.community = tricoUser.community;
				_shareObj.flush();
				
					
				_appModel.tricoUser = tricoUser;
				
				
				_loggedInSignal.dispatch(true);
				
				
//				trace(evt.result.SecurityToken);
//				trace(evt.result.Customer.CustomerNumber);
//				trace(evt.result.Customer.Name);
//				trace(evt.result.Customer.CoBuyerName);
//				trace(evt.result.Customer.Address1);
//				trace(evt.result.Customer.Address2);
//				trace(evt.result.Customer.City);
//				trace(evt.result.Customer.Province);
//				trace(evt.result.Customer.Zip);
//				trace(evt.result.Customer.Phone);
//				trace(evt.result.Customer.Email);
				
				trace("SOAPLoginService :: login success! " + _appModel.token);
				
			}else{
				
				trace("SOAPLoginService :: login fail!");
				
				if(evt.result.InvalidUserName){
					_invalidName.dispatch();
				}
				
				if(evt.result.InvalidPassword){
					_invalidPassword.dispatch();
				}
				
			}
			
		}
		
		public function expireToken():void
		{
			trace("SOAPLoginService :: expire token");
			
			_appModel.communityLinks = null;
			_appModel.tricoUser = null;
			_appModel.token = "";	
			_shareObj.clear();
			_shareObj.flush();
		}
		
		public function cleanup():void
		{
			trace("SOAPLoginSerivce :: cleanup()") ;
			
			
		}
		
	}
}