package com.sajakfarki.trico.service
{
	public interface IWishlistService
	{
		/** Get Wishlist from AdService **/
//		function getWishlist():void;
		function addWishlist(	adID:int, mediaType:String):void;
		function removeWishlist(adID:int):void;
		function cleanup():void;
	}
}