package com.sajakfarki.trico.service
{
	public interface IChecklistService
	{
		function getChecklist():void;
		function setCheck(id:int, bool:Boolean):void;
		function cleanup():void;
	}
}