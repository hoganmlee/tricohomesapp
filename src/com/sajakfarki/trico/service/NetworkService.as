package com.sajakfarki.trico.service
{
//	import flash.events.Event;
	import flash.events.StatusEvent;
	import flash.net.URLRequest;
	
	import air.net.URLMonitor;
	
	import org.osflash.signals.Signal;
	
	public class NetworkService
	{		
		private var _monitor:URLMonitor;
		private var _available:Boolean = false;
		
		public var networkChange:Signal = new Signal(Boolean);
		
		public function NetworkService()
		{
			//NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE, onNetworkChange);
			checkConnection();
		}
		
		public  function checkConnection():void
		{
			_monitor = new URLMonitor(new URLRequest('http://www.google.com'));
			_monitor.addEventListener(StatusEvent.STATUS, announceStatus);
			_monitor.start();
		}
		
		private function announceStatus(e:StatusEvent):void {
			trace("NetworkService >>>> Status change. Current status: " + _monitor.available);
			networkChange.dispatch(_monitor.available);
		}
		
		/*private function onNetworkChange(event:Event):void
		{
			trace("NetworkService >>>> type = ", event.type);
			trace(event);
			
		}*/
		
	}
	
}