package com.sajakfarki.trico.service
{
	public interface IEmailService
	{
		function sendEmail(subject:String, emailTo:String, body:String, isHTML:Boolean):void;
		function cleanup():void;
	}
}