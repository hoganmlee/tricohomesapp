package com.sajakfarki.trico.signal
{
	import org.osflash.signals.Signal;
	
	public class LogoutSignal extends Signal
	{
		public function LogoutSignal()
		{
			super();
		}
	}
}