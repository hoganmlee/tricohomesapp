package com.sajakfarki.trico.signal
{
	
	import org.osflash.signals.Signal;
	
	public class LoggedInSignal extends Signal
	{
		public function LoggedInSignal()
		{
			super(Boolean);
		}
	}
}