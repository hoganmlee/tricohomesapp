package com.sajakfarki.trico.signal
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import org.osflash.signals.Signal;
	
	public class AdFullPageSignal extends Signal
	{
		public function AdFullPageSignal()
		{
			super(DisplayAd);
		}
	}
}