package com.sajakfarki.trico.signal
{
	import com.sajakfarki.trico.model.vo.WishList;
	
	import org.osflash.signals.Signal;
	
	public class UpdateWishlistSignal extends Signal
	{
		public function UpdateWishlistSignal()
		{
			super(WishList);
		}
	}
}