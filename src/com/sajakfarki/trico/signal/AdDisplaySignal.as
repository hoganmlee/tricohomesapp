package com.sajakfarki.trico.signal
{
	import com.sajakfarki.trico.model.vo.DisplayAd;
	import org.osflash.signals.Signal;
	
	public class AdDisplaySignal extends Signal
	{
		public function AdDisplaySignal()
		{
			super(DisplayAd);
		}
	}
}