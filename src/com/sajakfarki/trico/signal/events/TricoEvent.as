package com.sajakfarki.trico.signal.events
{
	import starling.events.Event;
	
	public class TricoEvent extends Event
	{
		public static const MENU:String = "trico.menu";
		public static const LOGOUT:String = "trico.logout";
				
		public static const HOME:String = "trico.home";
		public static const CHECKLIST:String = "trico.checklist";
		public static const OFFERS:String = "trico.offers";
		public static const ALBUM:String = "trico.album";
		
		public static const SERVICE:String = "trico.service";
		public static const REFERRAL:String =  "trico.referral.program";
		public static const PROMOTIONS:String = "trico.promotions";
		public static const COMMUNITY:String = "trico.community";
		
		public static const TRICO_CENTRE:String =  "trico.centre";
		public static const EQUITY_SAVER:String = "trico.equity.saver";
//		public static const GLOBAL_FEST:String = "trico.global.fest";
		
		public static const HIDE_MENU:String = "trico.hide.menu";
		public static const SHOW_MENU:String = "trico.show.menu";
		
		public static const OFFER_CLICKED:String = "trico.offer.clicked";
		public static const START_APP:String = "trico.start.app";
		public static const BUILDING_EXPERIENCE:String = "trico.building.experience.manual";
		public static const INTERIOR_SELECTION:String = "trico.interior.selection.manual";
		public static const CUSTOMER_PORTAL:String = "trico.customer.portal.manual";
		
		public static const FULLPAGE:String = "trico.fullpage.ad";
		
		public function TricoEvent(type:String, bubbles:Boolean=false, data:Object=null)
		{
			super(type, bubbles, data);
		}
		
	}
	
}