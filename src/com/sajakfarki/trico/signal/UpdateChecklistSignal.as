package com.sajakfarki.trico.signal
{
	import com.sajakfarki.trico.model.vo.CheckList;
	
	import org.osflash.signals.Signal;
	
	public class UpdateChecklistSignal extends Signal
	{
		public function UpdateChecklistSignal()
		{
			super(CheckList);
		}
	}
}