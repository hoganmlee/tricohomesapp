package com.sajakfarki.trico.signal
{
	import org.osflash.signals.Signal;
	
	public class InvalidNameSignal extends Signal
	{
		public function InvalidNameSignal()
		{
			super();
		}
	}
}