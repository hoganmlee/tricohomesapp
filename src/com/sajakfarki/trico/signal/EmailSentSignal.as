package com.sajakfarki.trico.signal
{
	import org.osflash.signals.Signal;
	
	public class EmailSentSignal extends Signal
	{
		public function EmailSentSignal()
		{
			super(Boolean);
		}
	}
}