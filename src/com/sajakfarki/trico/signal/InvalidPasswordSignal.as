package com.sajakfarki.trico.signal
{
	import org.osflash.signals.Signal;
	
	public class InvalidPasswordSignal extends Signal
	{
		public function InvalidPasswordSignal()
		{
			super();
		}
	}
}