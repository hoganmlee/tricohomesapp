package com.sajakfarki.trico.signal
{
	import com.sajakfarki.trico.model.vo.Advertiser;
	
	import org.osflash.signals.Signal;
	
	public class AdInfoSignal extends Signal
	{
		public function AdInfoSignal()
		{
			super(Advertiser);
		}
	}
}