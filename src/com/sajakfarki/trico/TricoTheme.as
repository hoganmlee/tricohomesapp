package com.sajakfarki.trico
{
	import com.sajakfarki.feathersextended.controls.GroupListMultiSelect;
	import com.sajakfarki.feathersextended.renderer.DefaultGroupMultiSelectHeaderRenderer;
	import com.sajakfarki.feathersextended.renderer.DefaultGroupMultiSelectItemRenderer;
	import com.sajakfarki.trico.view.components.HomeView;
	import com.sajakfarki.trico.view.components.display.HouseBuildSprite;
	import com.sajakfarki.trico.view.components.display.LoadingSprite;
	import com.sajakfarki.trico.view.components.display.LoginInput;
	import com.sajakfarki.trico.view.components.display.TricoLogo;
	import com.sajakfarki.trico.view.components.display.UserTextInput;
	import com.sajakfarki.trico.view.components.display.WifiScreen;
	import com.sajakfarki.trico.view.components.nav.SideNav;
	import com.sajakfarki.trico.view.components.nav.TopNav;
	
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import feathers.controls.Button;
	import feathers.controls.Check;
	import feathers.controls.GroupedList;
	import feathers.controls.List;
	import feathers.controls.Scroller;
	import feathers.controls.SimpleScrollBar;
	import feathers.controls.TextInput;
	import feathers.controls.renderers.BaseDefaultItemRenderer;
	import feathers.controls.renderers.DefaultGroupedListHeaderOrFooterRenderer;
	import feathers.controls.renderers.DefaultGroupedListItemRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.core.DisplayListWatcher;
	import feathers.core.FeathersControl;
	import feathers.display.Scale3Image;
	import feathers.display.Scale9Image;
	import feathers.layout.VerticalLayout;
	import feathers.skins.ImageStateValueSelector;
	import feathers.skins.Scale9ImageStateValueSelector;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale3Textures;
	import feathers.textures.Scale9Textures;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.textures.TextureSmoothing;
	
	public class TricoTheme extends DisplayListWatcher
	{
		[Embed(source="/../assets/loading_sprite.atf", mimeType="application/octet-stream")]
		private static const LOADING_SHEET:Class;
		[Embed(source="/../assets/loading_sprite.xml", mimeType="application/octet-stream")]
		private static const LOADING_XML:Class;
		
		[Embed(source="/../assets/house_building.atf", mimeType="application/octet-stream")]
		private static const HOUSE_SHEET:Class;
		[Embed(source="/../assets/house_building.xml", mimeType="application/octet-stream")]
		private static const HOUSE_XML:Class;
				
		//45 Light
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_30.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_45Light_30:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_30.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_45Light_30:Class;
		
		public static const FONT_HELL_45Light_30:String = "fontHell45Light30";
		protected var bitmapFont45Light30:BitmapFont;
		protected var fontSize45Light30:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_20.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_45Light_20:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_20.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_45Light_20:Class;
		
		public static const FONT_HELL_45Light_20:String = "fontHell45Light20";
		protected var bitmapFont45Light20:BitmapFont;
		protected var fontSize45Light20:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_18.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_45Light_18:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_45Light_18.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_45Light_18:Class;
		
		public static const FONT_HELL_45Light_18:String = "fontHell45Light18";
		protected var bitmapFont45Light18:BitmapFont;
		protected var fontSize45Light18:Number;
		
		//55 Roman
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_12.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_55Roman_12:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_12.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_55Roman_12:Class;
		
		public static const FONT_HELL_55Roman_12:String = "fontHell55Roman12";
		protected var bitmapFont55Roman12:BitmapFont;
		protected var fontSize55Roman12:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_15.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_55Roman_15:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_15.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_55Roman_15:Class;
		
		public static const FONT_HELL_55Roman_15:String = "fontHell55Roman15";
		protected var bitmapFont55Roman15:BitmapFont;
		protected var fontSize55Roman15:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_18.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_55Roman_18:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_55Roman_18.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_55Roman_18:Class;
		
		public static const FONT_HELL_55Roman_18:String = "fontHell55Roman18";
		protected var bitmapFont55Roman18:BitmapFont;
		protected var fontSize55Roman18:Number;
		
		//57 Cond
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_57Cond_18.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_57Cond_18:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_57Cond_18.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_57Cond_18:Class;
		
		public static const FONT_HELL_57Cond_18:String = "fontHell57Cond18";
		protected var bitmapFont57Cond18:BitmapFont;
		protected var fontSize57Cond18:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_57Cond_20.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_57Cond_20:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_57Cond_20.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_57Cond_20:Class;
		
		public static const FONT_HELL_57Cond_20:String = "fontHell57Cond20";
		protected var bitmapFont57Cond20:BitmapFont;
		protected var fontSize57Cond20:Number;
		
		//67 Medium Cond
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_22.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_67MediumCond_22:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_22.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_67MediumCond_22:Class;
		
		public static const FONT_HELL_67MediumCond_22:String = "fontHell67MediumCond22";
		protected var bitmapFont67MediumCond22:BitmapFont;
		protected var fontSize67MediumCond22:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_20.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_67MediumCond_20:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_20.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_67MediumCond_20:Class;
		
		public static const FONT_HELL_67MediumCond_20:String = "fontHell67MediumCond20";
		protected var bitmapFont67MediumCond20:BitmapFont;
		protected var fontSize67MediumCond20:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_18.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_67MediumCond_18:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_67MediumCond_18.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_67MediumCond_18:Class;
		
		public static const FONT_HELL_67MediumCond_18:String = "fontHell67MediumCond18";
		protected var bitmapFont67MediumCond18:BitmapFont;
		protected var fontSize67MediumCond18:Number;
		
		//75 Bold
		[Embed(source="/../assets/fonts/helvetica_neue_lt_std_75_bold_38.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_75Bold_38:Class;
		[Embed(source="/../assets/fonts/helvetica_neue_lt_std_75_bold_38.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_75Bold_38:Class;
		
		public static const FONT_HELL_75Bold_38:String = "fontHell75Bold38";
		protected var bitmapFont75Bold38:BitmapFont;
		protected var fontSize75Bold38:Number;
		
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_75Bold_18.atf", mimeType="application/octet-stream")]
		protected static const FONT_IMAGE_75Bold_18:Class;
		[Embed(source="/../assets/fonts/HelvNeueLTDStd_75Bold_18.fnt",mimeType="application/octet-stream")]
		protected static const FONT_XML_75Bold_18:Class;
		
		public static const FONT_HELL_75Bold_18:String = "fontHell75Bold18";
		protected var bitmapFont75Bold18:BitmapFont;
		protected var fontSize75Bold18:Number;
		
		//Asset spritesheet
		[Embed(source="/../assets/trico_spritesheet.atf", mimeType="application/octet-stream")]
		protected static const SPRITE_ATLAS_IMAGE:Class;
		[Embed(source="/../assets/trico_spritesheet.xml",mimeType="application/octet-stream")]
		protected static const SPRITE_ATLAS_XML:Class;
		
		
		protected var atlas : TextureAtlas;
		protected var atlasBitmapData:BitmapData;
		
		public static const COLOR_ERROR:int = 0xEB1C24;
		
		public static const COLOR_SIDENAV_TEXT:int = 0xADADA5;
		public static const COLOR_SIDENAV_SUBTEXT:int = 0x797972;
		public static const COLOR_SIDENAV_UP:int = 0x33332D;
		public static const COLOR_SIDENAV_DOWN:int = 0x1C1C19;
		public static const COLOR_SIDENAV_HEADER:int = 0x47473E;
		public static const COLOR_SIDENAV_DIVIDER:int = 0x1F1F1B;
		
		public static const COLOR_BUTTON_HOVER:int = 0x62625C;
		
		public static const COLOR_HOME_DIVIDER:int = 0xC4C3BF;
		
		public static const COLOR_TEXT_TITLE:int = 0x494944;
		
		public static const COLOR_DIVIDER:int = 0xD9D8D4;
		public static const COLOR_ACCENT_BUTTON:int = 0x97978E;
		public static const COLOR_TEXT_PRIMARY:uint = 0xFFFFFF;
		
		protected static const BUTTON_ROUND15_SCALE9_GRID:Rectangle = new Rectangle(14, 14, 2, 2);
		protected static const BUTTON_ROUND18_SCALE9_GRID:Rectangle = new Rectangle(18, 18, 2, 2);
		protected static const BUTTON_ROUND6_SCALE9_GRID:Rectangle = new Rectangle(6, 6, 2, 2);
		protected static const BUTTON_ROUND10_SCALE9_GRID:Rectangle = new Rectangle(10, 10, 2, 2);
		
		protected static const PROGRESS_BAR_SCALE_3_FIRST_REGION:Number = 12;
		protected static const PROGRESS_BAR_SCALE_3_SECOND_REGION:Number = 12;
		
		protected static const SLIDER_FIRST:Number = 16;
		protected static const SLIDER_SECOND:Number = 8;
		protected static const CALLOUT_SCALE_9_GRID:Rectangle = new Rectangle(8, 24, 15, 33);
		protected static const SCROLL_BAR_THUMB_SCALE_9_GRID:Rectangle = new Rectangle(4, 4, 4, 4);
		
		protected static const BACKGROUND_COLOR:uint = 0x191919;
		protected static const SELECTED_TEXT_COLOR:uint = 0xCCCCCC;
		
		protected static function textRendererFactory():BitmapFontTextRenderer
		{
			return new BitmapFontTextRenderer();
		}
		
		protected static function textEditorFactory():StageTextTextEditor
		{
			return new StageTextTextEditor();
		}
				
		public function TricoTheme(root:DisplayObjectContainer, scaleToDPI:Boolean = true)
		{
			super(root);
			
			Starling.current.nativeStage.color = BACKGROUND_COLOR;
			
			if(root.stage)
			{
				root.stage.color = BACKGROUND_COLOR;
			}
			else
			{
				root.addEventListener(Event.ADDED_TO_STAGE, root_addedToStageHandler);
			}
			this._scaleToDPI = scaleToDPI;
			this.initialize();
		}
		
		protected var _originalDPI:int;
		
		public function get originalDPI():int
		{
			return this._originalDPI;
		}
		
		protected var _scaleToDPI:Boolean;
		
		public function get scaleToDPI():Boolean
		{
			return this._scaleToDPI;
		}
		
		protected var scale:Number = 1;
		
		private var _logo1:Texture;
		private var _logo2:Texture;
		private var _logo3:Texture;
		
		private var _buttonRound18Dark:Scale9Textures;
		private var _buttonRound18Light:Scale9Textures;
		private var _buttonRound18Red:Scale9Textures;
		
		private var _buttonRound6Dark:Scale9Textures;
		private var _buttonRound6Light:Scale9Textures;
		private var _buttonRound6Red:Scale9Textures;
		private var _buttonRound6White:Scale9Textures;		
		private var _buttonRound6Down:Scale9Textures;
		private var _buttonRound6Gradient:Scale3Textures;
		
		private var _buttonRound15Dark:Scale9Textures;
		private var _buttonRound15Light:Scale9Textures;
		
		private var _buttonRound10White:Scale9Textures;
		
		private var _buttonCounter:Scale3Textures;
		
		private var _buttonClose:Texture;
		private var _buttonLabelUser:Texture;
		private var _buttonLabelPassword:Texture;
		
		private var _buttonUnderline:Scale9Textures;
		
		private var _scrollBarVertical:Scale3Textures;
		private var _scrollBarHorizontal:Scale3Textures;
		
		private var _checkUpIconTexture:Texture;
		private var _checkDownIconTexture:Texture;
		private var _checkServiceUpIconTexture:Texture;
		private var _checkServiceDownIconTexture:Texture;
		
		
		private var _inputLogin:Scale9Textures;
		
		private var _iconWifi:Texture;
		
		private var _iconCarrot14Light:Texture;
		private var _iconCarrot14Dark:Texture;
		private var _iconCarrot14White:Texture;
		
		private var _iconCarrot20Light:Texture;
		private var _iconCarrot20Dark:Texture;
		private var _iconCarrot20White:Texture;
		
		private var _iconPDF:Texture;
		private var _iconPDFDark:Texture;
		private var _iconChecklist:Texture;
		private var _iconOffers:Texture;
		private var _iconAlbum:Texture;
		private var _iconLogout:Texture;
		
		private var _homeDropShadow:Texture;
		private var _bottomDropShadow:Scale3Textures;
		private var _sideNavDropShadow:Scale3Textures;
		
		private var _t:Texture;
		private var _d:XML;
		private var _ta:TextureAtlas;
		private var _mc:MovieClip;
		
		private var _houseTexture:Texture;
		private var _houseXML:XML;
		private var _houseAtlas:TextureAtlas;
		private var _houseMovie:MovieClip
						
		override public function dispose():void
		{
			if(this.root)
			{
				this.root.removeEventListener(Event.ADDED_TO_STAGE, root_addedToStageHandler);
			}
			if(this.atlas)
			{
				this.atlas.dispose();
				this.atlas = null;
			}
			if(this.atlasBitmapData)
			{
				this.atlasBitmapData.dispose();
				this.atlasBitmapData = null;
			}
			super.dispose();
		}
		
		protected function initialize():void
		{
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;
			
//			const scaledDPI:int = DeviceCapabilities.dpi / Starling.contentScaleFactor;
//			if(this._scaleToDPI)
//			{
//				if(DeviceCapabilities.isTablet(Starling.current.nativeStage))
//				{
//					this._originalDPI = ORIGINAL_DPI_IPAD_RETINA;
//				}
//				else
//				{
//					this._originalDPI = ORIGINAL_DPI_IPHONE_RETINA;
//				}
//			}
//			else
//			{
//				this._originalDPI = scaledDPI;
//			}
//			
//			this.scale = scaledDPI / this._originalDPI;
			
//			this.fontSize = 30 * this.scale;
			
//			Callout.stagePaddingTop = Callout.stagePaddingRight = Callout.stagePaddingBottom = Callout.stagePaddingLeft = 16 * this.scale;
			
			
			//loading sprite animation
			_t = Texture.fromAtfData(new LOADING_SHEET());
			_d = XML(new LOADING_XML());
			_ta = new TextureAtlas(_t, _d);
			_mc = new MovieClip(_ta.getTextures("tricoloader"), 30);
			

//			_houseTexture = Texture.fromBitmap(new HOUSE_SHEET());
			_houseTexture = Texture.fromAtfData(new HOUSE_SHEET());
			_houseXML = XML(new HOUSE_XML());
			_houseAtlas = new TextureAtlas(_houseTexture, _houseXML);
			_houseMovie = new MovieClip(_houseAtlas.getTextures("trico_loader_"), 97);
			
			
//			const atlasBitmapData:BitmapData = (new SPRITE_ATLAS_IMAGE()).bitmapData;
			this.atlas = new TextureAtlas(Texture.fromAtfData(new SPRITE_ATLAS_IMAGE()), XML(new SPRITE_ATLAS_XML()));
			
			if(Starling.handleLostContext)
			{
				this.atlasBitmapData = atlasBitmapData;
			}
			else
			{
				atlasBitmapData.dispose();
			}
			
			this.bitmapFont45Light30= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_45Light_30()), XML(new FONT_XML_45Light_30()));
			this.bitmapFont45Light30.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont45Light30, FONT_HELL_45Light_30);
			this.fontSize45Light30 = this.bitmapFont45Light30.size;
			
			this.bitmapFont45Light20= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_45Light_20()), XML(new FONT_XML_45Light_20()));
			this.bitmapFont45Light20.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont45Light20, FONT_HELL_45Light_20);
			this.fontSize45Light20 = this.bitmapFont45Light20.size;
			
			this.bitmapFont45Light18= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_45Light_18()), XML(new FONT_XML_45Light_18()));
			this.bitmapFont45Light18.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont45Light18, FONT_HELL_45Light_18);
			this.fontSize45Light18 = this.bitmapFont45Light18.size;
			
			this.bitmapFont55Roman12= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_55Roman_12()), XML(new FONT_XML_55Roman_12()));
			this.bitmapFont55Roman12.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont55Roman12, FONT_HELL_55Roman_12);
			this.fontSize55Roman12 = this.bitmapFont55Roman12.size;
			
			this.bitmapFont55Roman15= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_55Roman_15()), XML(new FONT_XML_55Roman_15()));
			this.bitmapFont55Roman15.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont55Roman15, FONT_HELL_55Roman_15);
			this.fontSize55Roman15 = this.bitmapFont55Roman15.size;
			
			this.bitmapFont55Roman18= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_55Roman_18()), XML(new FONT_XML_55Roman_18()));
			this.bitmapFont55Roman18.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont55Roman18, FONT_HELL_55Roman_18);
			this.fontSize55Roman18 = this.bitmapFont55Roman18.size;
			
			
			this.bitmapFont57Cond18= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_57Cond_18()), XML(new FONT_XML_57Cond_18()));
			this.bitmapFont57Cond18.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont57Cond18, FONT_HELL_57Cond_18);
			this.fontSize57Cond18 = this.bitmapFont57Cond18.size;
			
			this.bitmapFont57Cond20= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_57Cond_20()), XML(new FONT_XML_57Cond_20()));
			this.bitmapFont57Cond20.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont57Cond20, FONT_HELL_57Cond_20);
			this.fontSize57Cond20 = this.bitmapFont57Cond20.size;
			
			
			this.bitmapFont67MediumCond22 = new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_67MediumCond_22()), XML(new FONT_XML_67MediumCond_22()));
			this.bitmapFont67MediumCond22.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont67MediumCond22, FONT_HELL_67MediumCond_22);
			this.fontSize67MediumCond22 = this.bitmapFont67MediumCond22.size;
			
			this.bitmapFont67MediumCond20 = new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_67MediumCond_20()), XML(new FONT_XML_67MediumCond_20()));
			this.bitmapFont67MediumCond20.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont67MediumCond20, FONT_HELL_67MediumCond_20);
			this.fontSize67MediumCond20 = this.bitmapFont67MediumCond20.size;
			
			this.bitmapFont67MediumCond18 = new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_67MediumCond_18()), XML(new FONT_XML_67MediumCond_18()));
			this.bitmapFont67MediumCond18.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont67MediumCond18, FONT_HELL_67MediumCond_18);
			this.fontSize67MediumCond18 = this.bitmapFont67MediumCond18.size;
			
			
			this.bitmapFont75Bold38 = new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_75Bold_38()), XML(new FONT_XML_75Bold_38()));
			this.bitmapFont75Bold38.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont75Bold38, FONT_HELL_75Bold_38);
			this.fontSize75Bold38 = this.bitmapFont75Bold38.size;
			
			this.bitmapFont75Bold18= new BitmapFont(Texture.fromAtfData(new FONT_IMAGE_75Bold_18()), XML(new FONT_XML_75Bold_18()));
			this.bitmapFont75Bold18.smoothing = TextureSmoothing.NONE;
			TextField.registerBitmapFont(this.bitmapFont75Bold18, FONT_HELL_75Bold_18);
			this.fontSize75Bold18 = this.bitmapFont75Bold18.size;
		
			
			_buttonRound15Dark = new Scale9Textures(this.atlas.getTexture("button_round15_dark"), BUTTON_ROUND15_SCALE9_GRID );
			_buttonRound15Light = new Scale9Textures(this.atlas.getTexture("button_round15_light"), BUTTON_ROUND15_SCALE9_GRID );
			
			_buttonRound18Dark = new Scale9Textures(this.atlas.getTexture("button_round18_dark"), BUTTON_ROUND18_SCALE9_GRID);
			_buttonRound18Light = new Scale9Textures(this.atlas.getTexture("button_round18_light"), BUTTON_ROUND18_SCALE9_GRID);
			_buttonRound18Red = new Scale9Textures(this.atlas.getTexture("button_round18_red"), BUTTON_ROUND18_SCALE9_GRID);
			
			_buttonRound10White = new Scale9Textures(this.atlas.getTexture("button_round10_white"), BUTTON_ROUND10_SCALE9_GRID);
			
			_buttonRound6Dark = new Scale9Textures(this.atlas.getTexture("button_round6_dark"), BUTTON_ROUND6_SCALE9_GRID);
			_buttonRound6Light = new Scale9Textures(this.atlas.getTexture("button_round6_light"), BUTTON_ROUND6_SCALE9_GRID);
			_buttonRound6Red = new Scale9Textures(this.atlas.getTexture("button_round6_red"), BUTTON_ROUND6_SCALE9_GRID);
			_buttonRound6Down = new Scale9Textures(this.atlas.getTexture("button_round6_downstate"), BUTTON_ROUND6_SCALE9_GRID);
			_buttonRound6White = new Scale9Textures(this.atlas.getTexture("button_round6_white"), BUTTON_ROUND6_SCALE9_GRID);
			
			_buttonUnderline = new Scale9Textures(this.atlas.getTexture("button_underline"), new Rectangle(2, 2, 2, 2));
			_buttonRound6Gradient = new Scale3Textures(this.atlas.getTexture("button_round6_gradient"), 6, 6);
			_buttonCounter = new Scale3Textures(this.atlas.getTexture("button_counter"), 18, 2);
			
			_buttonClose = this.atlas.getTexture("button_close");
			_buttonLabelUser = this.atlas.getTexture("invalid_user");
			_buttonLabelPassword = this.atlas.getTexture("invalid_password");
			
			_inputLogin = new Scale9Textures(this.atlas.getTexture("input_login"), new Rectangle(12, 12, 2, 2) );
			
			_scrollBarVertical = new Scale3Textures(this.atlas.getTexture("scrollBar"), 2, 2, Scale3Textures.DIRECTION_VERTICAL);
			_scrollBarHorizontal = new Scale3Textures(this.atlas.getTexture("scrollBar"), 2, 2, Scale3Textures.DIRECTION_HORIZONTAL);
			
			_bottomDropShadow = new Scale3Textures(this.atlas.getTexture("bottomDropShadow"), 2, 2, Scale3Textures.DIRECTION_HORIZONTAL);
			_homeDropShadow = this.atlas.getTexture("homeDropShadow");
			_sideNavDropShadow = new Scale3Textures(this.atlas.getTexture("sideNavDropShadow"), 2, 2, Scale3Textures.DIRECTION_VERTICAL);
			
			_checkUpIconTexture = this.atlas.getTexture("checkbox_up");
			_checkDownIconTexture = this.atlas.getTexture("checkbox_down");
			_checkServiceUpIconTexture = this.atlas.getTexture("serviceCheckUp");
			_checkServiceDownIconTexture = this.atlas.getTexture("serviceCheckDown");
			
			_iconWifi = this.atlas.getTexture("wifi_signal");
			
			_iconCarrot14Light = this.atlas.getTexture("icon_carrot14_light");
			_iconCarrot14Dark = this.atlas.getTexture("icon_carrot14_dark");
			_iconCarrot14White = this.atlas.getTexture("icon_carrot14_white");
			
			_iconCarrot20Light = this.atlas.getTexture("icon_carrot20_light");
			_iconCarrot20Dark = this.atlas.getTexture("icon_carrot20_dark");
			_iconCarrot20White = this.atlas.getTexture("icon_carrot20_white");
			
			_iconPDF = this.atlas.getTexture("icon_pdf");
			_iconPDFDark = this.atlas.getTexture("icon_pdf_dark");
			_iconChecklist = this.atlas.getTexture("icon_checklist");
			_iconOffers = this.atlas.getTexture("icon_offers");
			_iconAlbum = this.atlas.getTexture("icon_album");
			_iconLogout = this.atlas.getTexture("icon_logout");
			
			_logo1 = this.atlas.getTexture("trico_logo1");
			_logo2 = this.atlas.getTexture("trico_logo2");
			_logo3 = this.atlas.getTexture("trico_logo3");
			
			this.setInitializerForClass(LoginInput, inputLoginInitializer);
			this.setInitializerForClass(UserTextInput, userTextInputInitializer);
			
			this.setInitializerForClass(WifiScreen, wifiScreenInitializer);
			
			this.setInitializerForClass(TricoLogo, logoInitializer1, "logo1");
			this.setInitializerForClass(TricoLogo, logoInitializer2, "logo2");
			
			this.setInitializerForClass(LoadingSprite, loadingSpriteInitializer);
			this.setInitializerForClass(HouseBuildSprite, houseBuildingInitializer);
			
			this.setInitializerForClass(Button, buttonLogoInitializer, "logo3");
			this.setInitializerForClass(Button, buttonSignInInitializer, "signin");
			this.setInitializerForClass(Button, buttonSendInitializer, "send")
			this.setInitializerForClass(Button, buttonSendLongInitializer, "sendLong")
				
			this.setInitializerForClass(Button, buttonTopMenuInitializer, "topMenu");
			this.setInitializerForClass(Button, buttonFullScreenDoneInitializer, "fullScreenDone");
			this.setInitializerForClass(Button, buttonCallMenuInitializer, "callMenu");
			this.setInitializerForClass(Button, buttonHomeLinkInitializer, "homeLink");
			this.setInitializerForClass(Button, buttonHomePDFInitializer, "homePDF");
			this.setInitializerForClass(Button, buttonHomeCommunityInitializer, "homeCommunity");
			this.setInitializerForClass(Button, buttonCounterInitializer, "counter");
			
			
			this.setInitializerForClass(Button, buttonAccentCounterInitializer, "accentCounter");
			this.setInitializerForClass(Button, buttonOffersFilterInitializer, "offersFilter");
			
			
			this.setInitializerForClass(Button, bucketInitializer, "bucket");
			this.setInitializerForClass(Button, buttonChecklistInitializer, "checklist");
			this.setInitializerForClass(Button, buttonOffersInitializer, "offers");
			this.setInitializerForClass(Button, buttonAlbumInitializer, "album");
			this.setInitializerForClass(Button, buttonWishlistInitializer, "wishlist");
			
			this.setInitializerForClass(Button, buttonSideNavChecklistInitializer, "sideChecklist");
			this.setInitializerForClass(Button, buttonSideNavOffersInitializer, "sideOffers");
			this.setInitializerForClass(Button, buttonSideNavAlbumInitializer, "sideAlbum");
			this.setInitializerForClass(Button, buttonSideNavLinkInitializer, "sideNavLink");
			this.setInitializerForClass(Button, buttonSideNavPDFInitializer, "sideNavPDF");
			this.setInitializerForClass(Button, buttonSideNavHeader1, "sideNavHeader1");
			this.setInitializerForClass(Button, buttonSideNavHeader2, "sideNavHeader2");
			this.setInitializerForClass(Button, buttonSideNavLogout, "sideNavLogout");
			
			this.setInitializerForClass(Button, buttonCloseInitializer, "closeButton");
			this.setInitializerForClass(Button, buttonLabelUserInitializer, "closeLabelUser");
			this.setInitializerForClass(Button, buttonLabelPasswordInitializer, "closeLabelPassword");
			
			this.setInitializerForClass(HomeView, homeScreenInitializer);
			this.setInitializerForClass(SideNav, sideNavInitializer);
			this.setInitializerForClass(TopNav, topNavInitializer);
		
			this.setInitializerForClass(DefaultListItemRenderer, linkListInitializer);
			this.setInitializerForClass(DefaultListItemRenderer, communityListInitializer, "communityList");
			
			this.setInitializerForClass(GroupedList, offersGroupedListInitializer, "offersGroupedList");
			this.setInitializerForClass(DefaultGroupedListItemRenderer, offersItemRendererInitializer, "offersGroupedListItem");
			this.setInitializerForClass(DefaultGroupedListHeaderOrFooterRenderer, offersHeaderRendererInitializer, "offersGroupedListHeader");
			
			this.setInitializerForClass(GroupListMultiSelect, checklistGroupedListInitializer, "checklistGroupedList");
			this.setInitializerForClass(DefaultGroupMultiSelectItemRenderer, checklistItemRendererInitializer, "checklistGroupedListItem");
			this.setInitializerForClass(DefaultGroupMultiSelectHeaderRenderer, checklistHeaderRendererInitializer, "checklistGroupedListHeader");
			
			this.setInitializerForClass(Scroller, scrollerInitializer);
			
			this.setInitializerForClass(TextInput, textInputInitializer);
			this.setInitializerForClass(TextInput, userInputInitializer, "userTextInput");
//			this.setInitializerForClass(ScrollText, scrollTextInitializer);
			this.setInitializerForClass(Check, checkInitializer);
			this.setInitializerForClass(Check, checkServiceInitializer, "serviceCheck");
			
		}
		
		private function wifiScreenInitializer(wifi:WifiScreen):void
		{
			wifi.setBG(_buttonRound10White, _iconWifi);
		}
		
		private function houseBuildingInitializer(loading:HouseBuildSprite):void
		{
			loading.setBG(_houseMovie);
		}
		
		private function buttonLabelPasswordInitializer(button:Button):void
		{
			button.defaultSkin = new Image(_buttonLabelPassword);	
		}
		
		private function buttonLabelUserInitializer(button:Button):void
		{
			button.defaultSkin = new Image(_buttonLabelUser);	
		}

		protected function communityListInitializer(renderer:BaseDefaultItemRenderer):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Dark;
			skinSelector.setValueForState(_buttonRound6Light, Button.STATE_DOWN, false);
			skinSelector.defaultSelectedValue = _buttonRound6Light;
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			//			renderer.acces
			//			renderer.accessorySourceFunction = getCheck();
			//			renderer.defaultSkin = new Scale9Image(_buttonUnderline);
			
			//			const upIcon:Image = new Image(_checkUpIconTexture);
			//			renderer.defaultIcon = upIcon
			//			renderer.defaultSelectedIcon = new Image(_checkDownIconTexture);
			//			renderer.upIcon = upIcon
			//			renderer.hoverIcon = upIcon
			//			renderer.iconOffsetY = -5;
			
			renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, COLOR_TEXT_PRIMARY);
			renderer.defaultLabelProperties.wordWrap = true;
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.paddingTop = renderer.paddingBottom = 12;
			renderer.paddingRight = 80;
			
			renderer.labelOffsetX = 17;
			renderer.labelOffsetY = 5;
		}
		
		protected function linkListInitializer(renderer:BaseDefaultItemRenderer):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Dark;
			skinSelector.setValueForState(_buttonRound6Light, Button.STATE_DOWN, false);
			skinSelector.defaultSelectedValue = _buttonRound6Light;
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, COLOR_TEXT_PRIMARY);
			renderer.defaultLabelProperties.wordWrap = true;
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.paddingTop = renderer.paddingBottom = 12;
			renderer.paddingRight = 80;
			
			renderer.labelOffsetX = 17;
			renderer.labelOffsetY = 5;
		}
		
		private function buttonCloseInitializer(button:Button):void
		{
			button.defaultSkin = new Image(_buttonClose);	
		}
				
		private function userTextInputInitializer(input:UserTextInput):void
		{
			input.setBG(_inputLogin);
		}
		
		private function loadingSpriteInitializer(loading:LoadingSprite):void
		{
			loading.setBG(_mc);
		}
		
		private function topNavInitializer(t:TopNav):void
		{
			t.addShadow(_bottomDropShadow);	
		}
		
		private function homeScreenInitializer(s:HomeView):void
		{
			s.addShadow(_homeDropShadow, _bottomDropShadow)
		}
		
		private function sideNavInitializer(s:SideNav):void
		{
			s.addShadow(_sideNavDropShadow);
		}
		
		private function logoInitializer1(logo:TricoLogo):void{ logo.image = _logo1; }
		private function logoInitializer2(logo:TricoLogo):void{ logo.image = _logo2; }
		private function logoInitializer3(logo:TricoLogo):void{ logo.image = _logo3; }
		
		private function inputLoginInitializer(input:LoginInput):void
		{
			input.setBG( _inputLogin);
		}
		
		/****************************************************************************
		 * 
		 *   Design Accent Checklist Button
		 * 
		 ****************************************************************************/
		protected function buttonAccentCounterInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound18Dark;
//			skinSelector.setValueForState(_buttonRound15Light, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 38,
					height: 38,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, COLOR_TEXT_PRIMARY);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 8
			button.paddingLeft = button.paddingRight = 15;
			
		}
		
		/****************************************************************************
		 * 
		 *    Exclusive Offers Filter Button
		 * 
		 ****************************************************************************/
		protected function buttonOffersFilterInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound18Red;
			skinSelector.defaultSelectedValue = _buttonRound18Light;
			skinSelector.setValueForState(_buttonRound18Dark, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 38,
					height: 38,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 8
			button.paddingLeft = button.paddingRight = 15;
			
		}
		
		protected function buttonWishlistInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Red;
			skinSelector.defaultSelectedValue = _buttonRound6Dark;
//			skinSelector.setValueForState(_buttonRound6Dark, Button.STATE_DOWN, false);
//			skinSelector.setValueForState(_buttonRound6Red, Button.STATE_DOWN, true);
			
			skinSelector.imageProperties =
				{
					width: 38,
					height: 38,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.label = "ADD TO WISHLIST";
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, COLOR_TEXT_PRIMARY);
			button.defaultLabelProperties.text = "ADD TO WISHLIST";
			
			button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, COLOR_TEXT_PRIMARY);
			button.defaultSelectedLabelProperties.text = "REMOVE FROM WISHLIST";
			
			button.paddingTop = 8;
			button.paddingLeft = button.paddingRight = 15;
			
		}
		
		/****************************************************************************
		 * 
		 *   Top Navigation Button
		 * 
		 ****************************************************************************/
		protected function buttonLogoInitializer(button:Button):void
		{
			button.defaultSkin = new Image(_logo3);	
		}
		
		protected function buttonTopMenuInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound15Dark;
			skinSelector.setValueForState(_buttonRound15Light, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, 0xADADA5);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
			button.labelOffsetY = 1;
			button.paddingTop = 8
			button.paddingLeft = button.paddingRight = 15;	
		}
		
		/****************************************************************************
		 * 
		 *   Album Fullscreen Done
		 * 
		 ****************************************************************************/
		protected function buttonFullScreenDoneInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound15Dark;
			skinSelector.setValueForState(_buttonRound15Light, Button.STATE_DOWN, false);
						
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
//			button.alpha = 0.5;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, 0xADADA5);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
			button.labelOffsetY = 1;
			button.paddingTop = 8
			button.paddingLeft = button.paddingRight = 15;	
		}
		
		
		/****************************************************************************
		 * 
		 *   Top Nav Call Menu Button
		 * 
		 ****************************************************************************/
		protected function buttonCallMenuInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound15Dark;
			skinSelector.setValueForState(_buttonRound15Light, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, 0xADADA5);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
//			button.labelOffsetY = 4;
			button.labelOffsetY = 1;
			button.labelOffsetX = 6;
			button.defaultIcon = new Image(_iconCarrot20Dark);
			button.iconOffsetY = -4;
			
//			button.iconPosition = Button.HORIZONTAL_ALIGN_RIGHT
			
			button.minTouchHeight = 50;
			button.minTouchWidth = 150;
			
			
			button.paddingTop = 8
			button.paddingLeft = 15 
			button.paddingRight = 22;
//			button.gap = Number.POSITIVE_INFINITY;
			
		}
		
		/****************************************************************************
		 * 
		 *   HomeScreen PDF Button
		 * 
		 ****************************************************************************/
		protected function buttonHomePDFInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Dark;
			skinSelector.setValueForState(_buttonRound6Down, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, 0xFFFFFF);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconPDF);
//			button.iconOffsetY = -4;
			button.iconPosition = Button.HORIZONTAL_ALIGN_RIGHT
				
			button.labelOffsetY = 4;
			
//			button.paddingTop = 8
			button.paddingLeft = button.paddingRight = 15;
			button.gap = Number.POSITIVE_INFINITY;
		}
		
		/****************************************************************************
		 * 
		 *   HomeScreen Link Button
		 * 
		 ****************************************************************************/
		protected function buttonHomeLinkInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Dark;
			skinSelector.setValueForState(_buttonRound6Down, Button.STATE_DOWN, false);
			
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, 0xFFFFFF);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconCarrot20White);
//			button.downIcon = new Image(_iconCarrot20Dark);
//			button.iconOffsetY = -4;
			button.iconPosition = Button.HORIZONTAL_ALIGN_RIGHT
			
//			button.paddingTop = 8
			button.labelOffsetY = 4;
			button.paddingLeft = button.paddingRight = 15;
			button.gap = Number.POSITIVE_INFINITY;
		}
		
		/****************************************************************************
		 * 
		 *   HomeScreen Community Button
		 * 
		 ****************************************************************************/
		protected function buttonHomeCommunityInitializer(button:Button):void
		{
			var buttonSkin:Scale3Image = new Scale3Image(_buttonRound6Gradient);
			buttonSkin.width = 375;
			
			button.defaultSkin = buttonSkin;
			button.defaultIcon = new Image(_iconCarrot20White);
			button.downIcon = new Image(_iconCarrot20Dark);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond20, this.fontSize67MediumCond20, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond20, this.fontSize67MediumCond20, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 13;
			button.paddingLeft = 20;
			button.iconPosition = Button.ICON_POSITION_MANUAL;
			button.iconOffsetX = 316;
			button.iconOffsetY = 5;
			
		}
		
		/****************************************************************************
		 * 
		 *   HomeScreen Bucket
		 * 
		 ****************************************************************************/
		protected function bucketInitializer(button:Button):void
		{
			button.defaultSkin = new Scale9Image(_buttonRound6White);
		}
		
		/****************************************************************************
		 * 
		 *   HomeScreen Bucket Label
		 * 
		 ****************************************************************************/
		protected function buttonChecklistInitializer(button:Button):void
		{
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, 0x97978E);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconChecklist);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 8;
			button.labelOffsetX = 10;
			button.paddingLeft = button.paddingRight = 22;
			
		}
		
		protected function buttonOffersInitializer(button:Button):void
		{
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, 0x97978E);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconOffers);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 8;
			button.labelOffsetX = 16;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonAlbumInitializer(button:Button):void
		{
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, 0x97978E);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond22, this.fontSize67MediumCond22, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconAlbum);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 8;
			button.labelOffsetX = 16;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		
		/****************************************************************************
		 * 
		 *   Side Nav Bucket labels
		 * 
		 ****************************************************************************/
		protected function buttonSideNavChecklistInitializer(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN)
			
			button.defaultSkin = upSkin;
			button.downSkin = downSkin;
			button.defaultSelectedSkin = downSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_TEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconChecklist);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			button.labelOffsetY = 8;
			button.labelOffsetX = 20;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonSideNavOffersInitializer(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN)
			
			button.defaultSkin = upSkin;
			button.downSkin = downSkin;
			button.defaultSelectedSkin = downSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_TEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconOffers);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 8;
			button.labelOffsetX = 28;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		
		protected function buttonSideNavAlbumInitializer(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN)
			
			button.defaultSkin = upSkin;
			button.downSkin = downSkin;
			button.defaultSelectedSkin = downSkin;
				
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_TEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconAlbum);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 8;
			button.labelOffsetX = 20;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonSideNavLinkInitializer(button:Button):void
		{
			const defaultIcon:Image = new Image(_iconCarrot14Dark);
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN);
			
			button.defaultSkin =  upSkin;
			button.downSkin = downSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_SUBTEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = defaultIcon;
			button.iconPosition = Button.HORIZONTAL_ALIGN_RIGHT;
			
//			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.gap = Number.POSITIVE_INFINITY;
			button.labelOffsetY = 7;
//			button.labelOffsetX = 20;
			button.paddingLeft = button.paddingRight = 22;
			
		}
		
		protected function buttonSideNavPDFInitializer(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN);
			
			button.defaultSkin =  upSkin;
			button.downSkin = downSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_SUBTEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
//			button.defaultIcon = new Image(_iconAlbum);
//			button.gap = Number.POSITIVE_INFINITY;
						
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			button.labelOffsetY = 5;
//			button.labelOffsetX = 20;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonSideNavLogout(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_UP);
			const downSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_DOWN)
			
			button.defaultSkin = upSkin;
			button.downSkin = downSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_SUBTEXT);
//			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, SELECTED_TEXT_COLOR);
			
			button.defaultIcon = new Image(_iconLogout);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT
			button.labelOffsetY = 6;
			button.labelOffsetX = 10;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonSideNavHeader1(button:Button):void
		{
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_HEADER);
			
			button.defaultSkin =  upSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_DOWN);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			button.labelOffsetY = 7;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		protected function buttonSideNavHeader2(button:Button):void
		{
			const defaultIcon:Image = new Image(_iconPDFDark);
			const upSkin:Quad = new Quad(10, 10, COLOR_SIDENAV_HEADER);
			
			button.defaultSkin =  upSkin;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond20, this.fontSize57Cond20, COLOR_SIDENAV_DOWN);
						
			button.defaultIcon = defaultIcon;
			button.iconPosition = Button.HORIZONTAL_ALIGN_RIGHT;
			button.gap = Number.POSITIVE_INFINITY;
			
//			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			button.labelOffsetY = 7;
			button.paddingLeft = button.paddingRight = 22;
		}
		
		/****************************************************************************
		 * 
		 *   Signin Button
		 * 
		 ****************************************************************************/
		protected function buttonSignInInitializer(button:Button):void
		{
			var buttonSkin:Scale3Image = new Scale3Image(_buttonRound6Gradient);
			buttonSkin.width = 375;
			
			button.defaultSkin = buttonSkin;
			button.defaultIcon = new Image(_iconCarrot20White);
			button.downIcon = new Image(_iconCarrot20Dark);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 16;
			button.iconPosition = Button.ICON_POSITION_MANUAL;
			button.iconOffsetX = 336;
			button.iconOffsetY = 2;

		}
		
		/****************************************************************************
		 * 
		 *   Send Button
		 * 
		 ****************************************************************************/
		protected function buttonSendLongInitializer(button:Button):void
		{
			var buttonSkin:Scale3Image = new Scale3Image(_buttonRound6Gradient);
			buttonSkin.width = 475;
			buttonSkin.height = 50;
			
			button.defaultSkin = buttonSkin;
			button.defaultIcon = new Image(_iconCarrot20White);
			button.downIcon = new Image(_iconCarrot20Dark);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 16;
			button.iconPosition = Button.ICON_POSITION_MANUAL;
			button.iconOffsetX = 436;
			//			button.iconOffsetY = 2;
			
		}
		
		protected function buttonSendInitializer(button:Button):void
		{
			var buttonSkin:Scale3Image = new Scale3Image(_buttonRound6Gradient);
			buttonSkin.width = 325;
			buttonSkin.height = 50;
			
			button.defaultSkin = buttonSkin;
			button.defaultIcon = new Image(_iconCarrot20White);
			button.downIcon = new Image(_iconCarrot20Dark);
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light30, this.fontSize45Light30, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 16;
			button.iconPosition = Button.ICON_POSITION_MANUAL;
			button.iconOffsetX = 286;
//			button.iconOffsetY = 2;
			
		}
		
		/****************************************************************************
		 * 
		 *   Exclusive/Counter Button
		 * 
		 ****************************************************************************/
		protected function buttonCounterInitializer(button:Button):void
		{
			var buttonSkin:Scale3Image = new Scale3Image(_buttonCounter);
			
			button.defaultSkin = buttonSkin;
			
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, COLOR_TEXT_PRIMARY);
			button.downLabelProperties.textFormat = button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond18, this.fontSize67MediumCond18, SELECTED_TEXT_COLOR);
			
			button.paddingTop = 6;
			button.paddingLeft = 15; 
			button.paddingRight = 12;
		}
		
		/****************************************************************************
		 * 
		 *   CheckBox
		 * 
		 ****************************************************************************/
		protected function checkInitializer(check:Check):void
		{
			const iconSelector:ImageStateValueSelector = new ImageStateValueSelector();
			iconSelector.defaultValue = _checkUpIconTexture;
			iconSelector.defaultSelectedValue = _checkDownIconTexture;
			iconSelector.setValueForState(_checkUpIconTexture, Button.STATE_DOWN, false);
//			iconSelector.setValueForState(checkDisabledIconTexture, Button.STATE_DISABLED, false);
			iconSelector.setValueForState(_checkDownIconTexture, Button.STATE_DOWN, true);
//			iconSelector.setValueForState(checkSelectedDisabledIconTexture, Button.STATE_DISABLED, true);
			iconSelector.imageProperties =
				{
					scaleX: this.scale,
					scaleY: this.scale
				};
			
			check.stateToIconFunction = iconSelector.updateValue;
			
			check.labelOffsetY = 5;
			check.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont55Roman15, this.fontSize55Roman15, COLOR_TEXT_TITLE);
			check.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont55Roman15, this.fontSize55Roman15, SELECTED_TEXT_COLOR);
			
//			check.minTouchWidth = check.minTouchHeight = 88 * this.scale;
			check.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			check.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
		}
		
		protected function checkServiceInitializer(check:Check):void
		{
			const iconSelector:ImageStateValueSelector = new ImageStateValueSelector();
			iconSelector.defaultValue = _checkServiceUpIconTexture;
			iconSelector.defaultSelectedValue = _checkServiceDownIconTexture;
			iconSelector.setValueForState(_checkServiceUpIconTexture, Button.STATE_DOWN, false);
			//			iconSelector.setValueForState(checkDisabledIconTexture, Button.STATE_DISABLED, false);
			iconSelector.setValueForState(_checkServiceDownIconTexture, Button.STATE_DOWN, true);
			//			iconSelector.setValueForState(checkSelectedDisabledIconTexture, Button.STATE_DISABLED, true);
			iconSelector.imageProperties =
				{
					scaleX: this.scale,
						scaleY: this.scale
				};
			
			check.stateToIconFunction = iconSelector.updateValue;
			
			check.labelOffsetY = 8;
			check.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light18, this.fontSize45Light18, COLOR_ACCENT_BUTTON);
			check.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light18, this.fontSize45Light18, COLOR_ACCENT_BUTTON);
			
			//			check.minTouchWidth = check.minTouchHeight = 88 * this.scale;
			check.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			check.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
		}
		
		/********************************************************************
		 * 
		 *  ScrollBar 
		 * 
		 ********************************************************************/
		protected function verticalScrollBarFactory():SimpleScrollBar
		{
			const scrollBar:SimpleScrollBar = new SimpleScrollBar();
			scrollBar.direction = SimpleScrollBar.DIRECTION_VERTICAL;
			const defaultSkin:Scale3Image = new Scale3Image(this._scrollBarVertical);
			defaultSkin.height = 10
			scrollBar.thumbProperties.defaultSkin = defaultSkin;
			scrollBar.paddingTop = scrollBar.paddingRight = scrollBar.paddingBottom = 4;
			return scrollBar;
		}
		
		protected function horizontalScrollBarFactory():SimpleScrollBar
		{
			const scrollBar:SimpleScrollBar = new SimpleScrollBar();
			scrollBar.direction = SimpleScrollBar.DIRECTION_HORIZONTAL;
			const defaultSkin:Scale3Image = new Scale3Image(this._scrollBarHorizontal);
			defaultSkin.width = 10
			scrollBar.thumbProperties.defaultSkin = defaultSkin;
			scrollBar.paddingRight = scrollBar.paddingBottom = scrollBar.paddingLeft = 4;
			return scrollBar;
		}
		
		protected function scrollerInitializer(scroller:Scroller):void
		{
			scroller.verticalScrollBarFactory = this.verticalScrollBarFactory;
			scroller.horizontalScrollBarFactory = this.horizontalScrollBarFactory;
		}
		
		/********************************************************************
		 * 
		 *  Scroll Text
		 * 
		 ********************************************************************/
/*		protected function scrollTextInitializer(text:ScrollText):void
		{
//			text.render(
			text.textFormat = new BitmapFontTextFormat(bitmapFont55Roman15, this.fontSize55Roman15, COLOR_TEXT_TITLE);
			text.paddingTop = text.paddingBottom = text.paddingLeft = 32 * this.scale;
			text.paddingRight = 36 * this.scale;
		}*/
		
		/********************************************************************
		 * 
		 *  Offers Grouped List
		 * 
		 ********************************************************************/
//		protected function offersGroupedListInitializer(list:GroupListMultiSelect):void
		protected function offersGroupedListInitializer(list:GroupedList):void
		{
			list.itemRendererName = "offersGroupedListItem";
			list.headerRendererName = "offersGroupedListHeader";
			
			const layout:VerticalLayout = new VerticalLayout();
			layout.useVirtualLayout = true;
			layout.paddingBottom = 50;
			layout.gap = 3;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			list.layout = layout;
			
		}
		
//		protected function offersItemRendererInitializer(renderer:DefaultGroupMultiSelectItemRenderer):void
		protected function offersItemRendererInitializer(renderer:DefaultGroupedListItemRenderer):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = _buttonRound6Dark;
			skinSelector.setValueForState(_buttonRound6Light, Button.STATE_DOWN, false);
			skinSelector.defaultSelectedValue = _buttonRound6Light;
			skinSelector.imageProperties =
				{
					width: 30,
					height: 30,
					textureScale: 1
				};
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
//			renderer.acces
//			renderer.accessorySourceFunction = getCheck();
//			renderer.defaultSkin = new Scale9Image(_buttonUnderline);
			
//			const upIcon:Image = new Image(_checkUpIconTexture);
//			renderer.defaultIcon = upIcon
//			renderer.defaultSelectedIcon = new Image(_checkDownIconTexture);
//			renderer.upIcon = upIcon
//			renderer.hoverIcon = upIcon
//			renderer.iconOffsetY = -5;
			
			renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont57Cond18, this.fontSize57Cond18, COLOR_TEXT_PRIMARY);
			renderer.defaultLabelProperties.wordWrap = true;
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.paddingTop = renderer.paddingBottom = 12;
			renderer.paddingRight = 80;
			
			renderer.labelOffsetX = 17;
			renderer.labelOffsetY = 5;

		} 
		
//		protected function offersHeaderRendererInitializer(renderer:DefaultGroupMultiSelectHeaderRenderer):void
		protected function offersHeaderRendererInitializer(renderer:DefaultGroupedListHeaderOrFooterRenderer):void
		{
			
//			renderer.backgroundSkin = new Scale9Image(_buttonUnderline);
			renderer.contentLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond20, this.fontSize67MediumCond20, COLOR_SIDENAV_SUBTEXT); 
			renderer.paddingTop = 28;
			renderer.paddingBottom = 10
			
		}
		
		/********************************************************************
		 * 
		 *  Checklist Grouped List
		 * 
		 ********************************************************************/
		protected function checklistGroupedListInitializer(list:GroupListMultiSelect):void
		{
			list.itemRendererName = "checklistGroupedListItem";
			list.headerRendererName = "checklistGroupedListHeader";
			
			const layout:VerticalLayout = new VerticalLayout();
			layout.useVirtualLayout = true;
			layout.paddingBottom = 50;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			
			list.layout = layout;
			
		}
		
		protected function checklistItemRendererInitializer(renderer:DefaultGroupMultiSelectItemRenderer):void
		{
//			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
//			skinSelector.defaultValue = _buttonRound6Dark;
//			skinSelector.setValueForState(_buttonRound6Light, Button.STATE_DOWN, false);
//			skinSelector.defaultSelectedValue = this.itemRendererSelectedSkinTextures;
//			skinSelector.setValueForState(this.itemRendererSelectedSkinTextures, Button.STATE_DOWN, false);
//			skinSelector.imageProperties =
//				{
//					width: 30,
//					height: 30,
//					textureScale: 1
//				};
//			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			renderer.defaultSkin = new Scale9Image(_buttonUnderline);
			
			const upIcon:Image = new Image(_checkUpIconTexture);
			const downIcon:Image = new Image(_checkDownIconTexture);

			renderer.defaultIcon = upIcon;
			renderer.defaultSelectedIcon = downIcon;
			renderer.upIcon = upIcon;
			renderer.hoverIcon = upIcon;
//			renderer.iconOffsetY = -5;
			
			renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont45Light20, this.fontSize45Light20, COLOR_ACCENT_BUTTON);
			renderer.defaultLabelProperties.wordWrap = true;
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.paddingTop = renderer.paddingBottom = 12;
			renderer.paddingRight = 80;
			
			renderer.labelOffsetX = 17;
			renderer.labelOffsetY = 5;
			renderer.iconPosition = Button.ICON_POSITION_LEFT;
			
//			renderer.accessoryGap = Number.POSITIVE_INFINITY;
//			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
//			renderer.minWidth = 675;
//			renderer.minHeight = 50;
//			renderer.minTouchWidth = 675;
//			renderer.minTouchHeight = 50;
//			renderer.accessoryLoaderFactory = this.imageLoaderFactory;
//			renderer.iconLoaderFactory = this.imageLoaderFactory;
		}
		
		protected function checklistHeaderRendererInitializer(renderer:DefaultGroupMultiSelectHeaderRenderer):void
		{
//			const defaultSkin:Quad = new Quad(44 * this.scale, 44 * this.scale, 0x242424);
//			renderer.backgroundSkin = defaultSkin;
						
			renderer.backgroundSkin = new Scale9Image(_buttonUnderline);
			renderer.contentLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont67MediumCond20, this.fontSize67MediumCond20, COLOR_SIDENAV_SUBTEXT);
//			renderer.height = 68;
//			renderer.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE
				
			renderer.paddingTop = 27;
			renderer.paddingBottom = 14;
//			renderer.paddingBottom = 16
//			renderer.paddingLeft = renderer.paddingRight = 1;
//			renderer.minWidth = renderer.minHeight = 48
//			renderer.minTouchWidth = renderer.minTouchHeight = 48;
			
		}
		
		/****************************************************************************
		 * 
		 *    TextInput
		 * 
		 ****************************************************************************/
		protected function textInputInitializer(input:TextInput):void
		{			
//			input.minWidth = input.minHeight = 60 * this.scale;
//			input.minTouchWidth = input.minTouchHeight = 88 * this.scale;
			input.paddingTop = input.paddingBottom = 5;
			input.paddingLeft = input.paddingRight = 14;
				
			input.textEditorProperties.fontFamily = "Helvetica Neue,Helvetica,Arial,_sans";
			input.textEditorProperties.fontSize = 24;
			input.textEditorProperties.color = 0x999999;
			input.textEditorProperties.textAlign = "center";

		}
		
		protected function userInputInitializer(input:TextInput):void
		{
			input.paddingTop = 10; 
			input.paddingBottom = 5;
			input.paddingLeft = input.paddingRight = 14;
			
			input.textEditorProperties.fontFamily = "Helvetica Neue,Helvetica,Arial,_sans";
			input.textEditorProperties.fontSize = 20;
			input.textEditorProperties.color = 0x999999;
			input.textEditorProperties.textAlign = "left";
		}
		
		protected function root_addedToStageHandler(event:Event):void
		{
			DisplayObject(event.currentTarget).stage.color = BACKGROUND_COLOR;
		}
	}
}