package com.sajakfarki.trico
{
	import com.sajakfarki.trico.model.vo.AppModel;
	import com.sajakfarki.trico.service.ILoginService;
	import com.sajakfarki.trico.service.NetworkService;
	import com.sajakfarki.trico.signal.events.TricoEvent;
	import com.sajakfarki.trico.view.components.AdFullPageView;
	import com.sajakfarki.trico.view.components.AlbumView;
	import com.sajakfarki.trico.view.components.ChecklistView;
	import com.sajakfarki.trico.view.components.CommunityView;
	import com.sajakfarki.trico.view.components.HomeView;
	import com.sajakfarki.trico.view.components.OffersView;
	import com.sajakfarki.trico.view.components.ReferralView;
	import com.sajakfarki.trico.view.components.WelcomeView;
	import com.sajakfarki.trico.view.components.display.HouseBuildSprite;
	import com.sajakfarki.trico.view.components.display.LoadingSprite;
	import com.sajakfarki.trico.view.components.display.WifiScreen;
	import com.sajakfarki.trico.view.components.nav.SideNav;
	import com.sajakfarki.trico.view.components.nav.TopNav;
	import com.sajakfarki.trico.view.components.pdfs.BuildingExperienceView;
	import com.sajakfarki.trico.view.components.pdfs.CustomerPortalView;
	import com.sajakfarki.trico.view.components.pdfs.InteriorSelectionView;
	import com.sajakfarki.trico.view.components.pdfs.ServiceView;
	import com.sajakfarki.trico.view.components.promotions.EquitySaverView;
	import com.sajakfarki.trico.view.components.promotions.TricoCentreView;
	
	import flash.utils.setTimeout;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
		
	public class TricoNavigator
	{
		[Inject]
		public var _loginService:ILoginService;
		[Inject]
		public var _appModel:AppModel;
		
		public static var HEIGHT:int = 752;
		public static var WIDTH:int = 1280;
		
		public static var DURATION_FULLPAGE:Number = 100;
		
		public static const DURATION_SCREEN:Number = 0.9;
		public static const DURATION_MENU:Number = 0.4;
		
		//Navigation
		private var _sideNav:SideNav;
		private var _topNav:TopNav;
		private var _disabler:Button;
		
		//Transitions
		private var _topMenuTransition:Tween;
		private var _sideMenuTransition:Tween;
		private var _screenOutTransition:Tween;
		private var _screenInTransition:Tween;
		
//		private var _isWebView:Boolean = false;
		private var _isInTransition:Boolean = false;
		private var _logout:Boolean = false;
		
		//Transition Options
		private var _ease:String = Transitions.EASE_IN_OUT;
		
		//Screen Holders
		private var _currentScreen:FeathersControl;
		private var _nextScreen:FeathersControl;
		private var _isGoingHome:Boolean = true;
		private var _isNewScreen:Boolean;
		
		//Screens
		private var _homeScreen:HomeView;
		private var _welcome:WelcomeView;
		private var _checklist:ChecklistView;
		private var _offers:OffersView;
		private var _album:AlbumView;
		private var _service:ServiceView;
		private var _communityScreen:CommunityView;
		private var _referalScreen:ReferralView;
		private var _tricoCentreView:TricoCentreView;
		private var _equitySaverView:EquitySaverView;
//		private var _globalFestScreen:GlobalFestView;
		private var _buildingExperienceView:BuildingExperienceView;
		private var _customerPortal:CustomerPortalView;
		private var _interiorSelection:InteriorSelectionView;
		
		private var _currentEvent:String;
		private var _contextView:DisplayObjectContainer;
		private var _loadingSprite:LoadingSprite;
//		private var _bg:Quad;
				
		//Loader
		private var _houseBuild:HouseBuildSprite;
		
		//WIFI
		private var _networkService:NetworkService;
		private var _wifiScreen:WifiScreen;
		
		//FULL PAGE AD
		private var _fullPageAd:AdFullPageView;
		private var _fullPageReady:Boolean;
		private var _servicesConnected:Boolean = false;
		
		private var _startUp:Boolean = true;
		
		public function TricoNavigator(contextView:DisplayObjectContainer)
		{
			_contextView = contextView;
		}
		
		/***************************************************************************
		 * 
		 *  Start App
		 * 
		 ***************************************************************************/
		public function startApp():void
		{
//			_bg = new Quad(WIDTH, HEIGHT, 0x000000);
//			_contextView.addChild(_bg);
			
			_disabler = new Button();
			_disabler.width = WIDTH-SideNav.WIDTH;
			_disabler.height = HEIGHT;
			_disabler.x = WIDTH;
			_contextView.addChild(_disabler);
			
			_wifiScreen = new WifiScreen();
			_wifiScreen.visible = false;
			_wifiScreen.x = WIDTH/2;
			_wifiScreen.y = HEIGHT/2;
			_contextView.addChild(_wifiScreen);
			
			_networkService = new NetworkService();
			_networkService.networkChange.add(networkListener);
		}
		
		private function networkListener(isConnected:Boolean):void
		{
			if(isConnected){
				
				_wifiScreen.visible = false;
				
				if(_startUp){
					
					_welcome = new WelcomeView();
					_welcome.addEventListener(Event.OPEN, signinListener);
					_welcome.height = HEIGHT;
					_welcome.width = WIDTH;
					_contextView.addChildAt(_welcome, 1);
					
					_startUp = false;
				}
				
			}else{
				
				_wifiScreen.visible = true;
				
			}
			
		}
		
		private function fullPageReady():void
		{
			_fullPageReady = true;
			checkAppLoad();
		}
		
		/***************************************************************************
		 * 
		 *  Navigation
		 * 
		 ***************************************************************************/
		private function homeScreenListeners(add:Boolean):void
		{
			if(add){
				_homeScreen.addEventListener(TricoEvent.START_APP, navListener);
				
				_homeScreen.addEventListener(TricoEvent.ALBUM, navListener);
				_homeScreen.addEventListener(TricoEvent.COMMUNITY, navListener);
				_homeScreen.addEventListener(TricoEvent.SERVICE, navListener);
				_homeScreen.addEventListener(TricoEvent.CHECKLIST, navListener);
				_homeScreen.addEventListener(TricoEvent.OFFERS, navListener);
				_homeScreen.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
				_homeScreen.addEventListener(TricoEvent.REFERRAL, navListener);
				_homeScreen.addEventListener(TricoEvent.PROMOTIONS, navListener);
				_homeScreen.addEventListener(TricoEvent.TRICO_CENTRE, navListener);
				_homeScreen.addEventListener(TricoEvent.EQUITY_SAVER, navListener);
//				_homeScreen.addEventListener(TricoEvent.GLOBAL_FEST, navListener);
				_homeScreen.addEventListener(TricoEvent.BUILDING_EXPERIENCE, navListener);
				_homeScreen.addEventListener(TricoEvent.INTERIOR_SELECTION, navListener);
				_homeScreen.addEventListener(TricoEvent.CUSTOMER_PORTAL, navListener);
				
			}else{
				_homeScreen.removeEventListener(TricoEvent.START_APP, navListener);
				
				_homeScreen.removeEventListener(TricoEvent.ALBUM, navListener);
				_homeScreen.removeEventListener(TricoEvent.COMMUNITY, navListener);
				_homeScreen.removeEventListener(TricoEvent.SERVICE, navListener);
				_homeScreen.removeEventListener(TricoEvent.CHECKLIST, navListener);
				_homeScreen.removeEventListener(TricoEvent.OFFERS, navListener);
				_homeScreen.removeEventListener(TricoEvent.OFFER_CLICKED, navListener);
				_homeScreen.removeEventListener(TricoEvent.REFERRAL, navListener);
				_homeScreen.removeEventListener(TricoEvent.PROMOTIONS, navListener);
				_homeScreen.removeEventListener(TricoEvent.TRICO_CENTRE, navListener);
				_homeScreen.removeEventListener(TricoEvent.EQUITY_SAVER, navListener);
//				_homeScreen.removeEventListener(TricoEvent.GLOBAL_FEST, navListener);
				_homeScreen.removeEventListener(TricoEvent.BUILDING_EXPERIENCE, navListener);
				_homeScreen.removeEventListener(TricoEvent.INTERIOR_SELECTION, navListener);
				_homeScreen.removeEventListener(TricoEvent.CUSTOMER_PORTAL, navListener);
			}
		}
		
		private function navScreenListeners(add:Boolean):void
		{
			if(add){
				_sideNav.addEventListener(TricoEvent.HOME, navListener);
				_sideNav.addEventListener(TricoEvent.CHECKLIST, navListener);
				_sideNav.addEventListener(TricoEvent.COMMUNITY, navListener);
				_sideNav.addEventListener(TricoEvent.SERVICE, navListener);
				_sideNav.addEventListener(TricoEvent.OFFERS, navListener);
				_sideNav.addEventListener(TricoEvent.ALBUM, navListener);
				_sideNav.addEventListener(TricoEvent.LOGOUT, logoutListener);
				_sideNav.addEventListener(TricoEvent.REFERRAL, navListener);
				_sideNav.addEventListener(TricoEvent.TRICO_CENTRE, navListener);
				_sideNav.addEventListener(TricoEvent.EQUITY_SAVER, navListener);
//				_sideNav.addEventListener(TricoEvent.GLOBAL_FEST, navListener);
				_sideNav.addEventListener(TricoEvent.BUILDING_EXPERIENCE, navListener);
				_sideNav.addEventListener(TricoEvent.INTERIOR_SELECTION, navListener);
				_sideNav.addEventListener(TricoEvent.CUSTOMER_PORTAL, navListener);
			}else{
				_sideNav.removeEventListener(TricoEvent.HOME, navListener);
				_sideNav.removeEventListener(TricoEvent.CHECKLIST, navListener);
				_sideNav.removeEventListener(TricoEvent.COMMUNITY, navListener);
				_sideNav.removeEventListener(TricoEvent.SERVICE, navListener);
				_sideNav.removeEventListener(TricoEvent.OFFERS, navListener);
				_sideNav.removeEventListener(TricoEvent.ALBUM, navListener);
				_sideNav.removeEventListener(TricoEvent.LOGOUT, logoutListener);
				_sideNav.removeEventListener(TricoEvent.REFERRAL, navListener);
				_sideNav.removeEventListener(TricoEvent.TRICO_CENTRE, navListener);
				_sideNav.removeEventListener(TricoEvent.EQUITY_SAVER, navListener);
//				_sideNav.removeEventListener(TricoEvent.GLOBAL_FEST, navListener);
				_sideNav.removeEventListener(TricoEvent.BUILDING_EXPERIENCE, navListener);
				_sideNav.removeEventListener(TricoEvent.INTERIOR_SELECTION, navListener);
				_sideNav.removeEventListener(TricoEvent.CUSTOMER_PORTAL, navListener);
			}
		}
		
		private function navListener(e:Event):void
		{
			if(!_isInTransition){
				
				_currentEvent = e.type;
				
				trace(">>>>>>>>>>>>>>>>>> " + e.type);
				
				_appModel.offerSelected = 0;
				_isNewScreen = true;
				
				switch(e.type){
					
					case TricoEvent.HOME:
						
						if(!(_currentScreen is HomeView)){
							
							_homeScreen = new HomeView();
							_homeScreen.isStartUp = false;
							_homeScreen.height = HEIGHT - TopNav.HEIGHT;
							_homeScreen.width = WIDTH;
							_nextScreen = _homeScreen;
							
							homeScreenListeners(true);
						}else{
							_isNewScreen = false;
						}
						
						break;
										
					case TricoEvent.REFERRAL:
						
						if(!(_currentScreen is ReferralView)){
							_referalScreen = new ReferralView();
							_referalScreen.width = WIDTH;
							_referalScreen.height = HEIGHT - TopNav.HEIGHT;
//							_referalScreen.y = HEIGHT;
							_nextScreen = _referalScreen;
						}else{
							_isNewScreen = false;
						}
						
						break;
					
					case TricoEvent.COMMUNITY:
						if(!(_currentScreen is CommunityView)){
							_communityScreen = new CommunityView();
							_communityScreen.width = WIDTH;
							_communityScreen.height = HEIGHT - TopNav.HEIGHT;
							_communityScreen.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
//							_communityScreen.y = HEIGHT;
							_nextScreen = _communityScreen;
						}else{
							_isNewScreen = false;
						}
						break;
					
					case TricoEvent.TRICO_CENTRE:
						if(!(_currentScreen is TricoCentreView)){
							_tricoCentreView = new TricoCentreView();
							_tricoCentreView.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_tricoCentreView.width = WIDTH;
							_tricoCentreView.height = HEIGHT - TopNav.HEIGHT;
//							_tricoCentreView.y = HEIGHT;
							_nextScreen = _tricoCentreView;
						}else{
							_isNewScreen = false;
						}
						break;
					
					case TricoEvent.BUILDING_EXPERIENCE:
						if(!(_currentScreen is BuildingExperienceView)){
							_buildingExperienceView = new BuildingExperienceView();
							_buildingExperienceView.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_buildingExperienceView.width = WIDTH;
							_buildingExperienceView.height = HEIGHT - TopNav.HEIGHT;
//							_buildingExperienceView.y = HEIGHT;
							_nextScreen = _buildingExperienceView;
						}else{
							_isNewScreen = false;
						}
						break;
					
					case TricoEvent.CUSTOMER_PORTAL:
						if(!(_currentScreen is CustomerPortalView)){
							_customerPortal = new CustomerPortalView();
							_customerPortal.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_customerPortal.width = WIDTH;
							_customerPortal.height = HEIGHT - TopNav.HEIGHT;
//							_customerPortal.y = HEIGHT;
							_nextScreen = _customerPortal;
						}else{
							_isNewScreen = false;
						}
						break;
					
					case TricoEvent.INTERIOR_SELECTION:
						if(!(_currentScreen is InteriorSelectionView)){
							_interiorSelection = new InteriorSelectionView();
							_interiorSelection.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_interiorSelection.width = WIDTH;
							_interiorSelection.height = HEIGHT - TopNav.HEIGHT;
//							_interiorSelection.y = HEIGHT;
							_nextScreen = _interiorSelection;
						}else{
							_isNewScreen = false;
						}
						break;
					
					/*case TricoEvent.GLOBAL_FEST:
						if(!(_currentScreen is GlobalFestView)){
							_globalFestScreen = new GlobalFestView();
							_globalFestScreen.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_globalFestScreen.width = WIDTH;
							_globalFestScreen.height = HEIGHT - TopNav.HEIGHT;
							_globalFestScreen.y = -HEIGHT;
							_nextScreen = _globalFestScreen;
						}
						break;*/
					
					case TricoEvent.EQUITY_SAVER:
						
						if(!(_currentScreen is EquitySaverView)){
							_equitySaverView = new EquitySaverView();
							_equitySaverView.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_equitySaverView.width = WIDTH;
							_equitySaverView.height = HEIGHT - TopNav.HEIGHT;
//							_equitySaverView.y = HEIGHT;
							_nextScreen = _equitySaverView;
						}else{
							_isNewScreen = false;
						}
						break;
					
					case TricoEvent.SERVICE:
						
						if(!(_currentScreen is ServiceView)){
							_service = new ServiceView();
							_service.width = WIDTH;
							_service.height = HEIGHT - TopNav.HEIGHT;
//							_service.y = HEIGHT;
							
							_nextScreen = _service;
						}else{
							_isNewScreen = false;
						}
						break;
						
					case TricoEvent.OFFER_CLICKED:
						
						trace(" TricoEvent.OFFER_CLICKED = " + e.data);
						_appModel.offerSelected = int(e.data);
						
					case TricoEvent.OFFERS:
						
						if(!(_currentScreen is OffersView)){
							_offers = new OffersView();
							_offers.width = WIDTH;
							_offers.height = HEIGHT - TopNav.HEIGHT;
//							_offers.y = HEIGHT;
							_nextScreen = _offers;
						}else{
							_isNewScreen = false;
						}
						
						break;
					
					case TricoEvent.CHECKLIST:
						
						if(!(_currentScreen is ChecklistView)){
							_checklist = new ChecklistView();
							_checklist.addEventListener(TricoEvent.OFFER_CLICKED, navListener);
							_checklist.width = WIDTH;
							_checklist.height = HEIGHT - TopNav.HEIGHT;
//							_checklist.y = HEIGHT;
							_nextScreen = _checklist;
						}else{
							_isNewScreen = false;
						}
						
						break;
					
					case TricoEvent.ALBUM:
											
						if(!(_currentScreen is AlbumView)){
							_album = new AlbumView();
							_album.width = WIDTH;
							_album.height = HEIGHT - TopNav.HEIGHT;
							_album.addEventListener(TricoEvent.SHOW_MENU, navListener);
							_album.addEventListener(TricoEvent.HIDE_MENU, navListener);
//							_album.y = HEIGHT;
							_nextScreen = _album;
						}else{
							_isNewScreen = false;
						}
						
						break;
					
					case TricoEvent.HIDE_MENU:
						
						_isNewScreen = false;
						hideTopMenu();
						
						break;
					
					case TricoEvent.SHOW_MENU:
					
						_isNewScreen = false;
						showTopMenu();
						
						break;
					
					case TricoEvent.START_APP:
						
						_servicesConnected = true;
						checkAppLoad();
						
						return;
					
					default:
						
						break;
				}
				
				
				if(_isNewScreen){
					if(_nextScreen is HomeView){
						_isGoingHome = true;
						_nextScreen.y = -HEIGHT;
					}else{
						_isGoingHome = false;
						_nextScreen.y = HEIGHT;
					}
				}
				

				trace("TRANSITION!");
				
				if(_topNav.isOpen){
					closeMenu();
				}else{
					topNavTransitionClean();
					
					/*if(e.type ==TricoEvent.HOME){
						_topNav.hideMenuButton();
					}else{
						_topNav.showMenuButton();
					}*/
					
				}
				
			}
			
		}
		
		private function checkAppLoad():void
		{
			if(_fullPageReady && _servicesConnected){
				
				_houseBuild.load(false);
				_houseBuild.addEventListener(FeathersEventType.TRANSITION_COMPLETE, showComplete);
				
			}
		}
		
		private function showComplete(e:Event):void
		{
			trace("show complete");
			//loading sprite animation
			_loadingSprite = new LoadingSprite();
			_loadingSprite.x = WIDTH/2;
			_loadingSprite.y = HEIGHT/2;
			_contextView.addChild(_loadingSprite);
			_loadingSprite.addEventListener(FeathersEventType.TRANSITION_COMPLETE, loadFullPage);
			_loadingSprite.load(true);
		}
		
		private function loadFullPage(e:Event):void
		{
			_fullPageAd.showAd();
			
			setTimeout(goHome, DURATION_FULLPAGE);
		}
		
		private function goHome():void
		{
			_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
			_topMenuTransition.animate("y", 0);
			_topMenuTransition.onComplete = transitionClean;
			_topMenuTransition.onCompleteArgs = [_topMenuTransition];
			Starling.juggler.add(_topMenuTransition);
			
			setTimeout(tweenHome, 400);
		}
		
		private function tweenHome():void
		{
			_screenInTransition = new Tween(_currentScreen, DURATION_SCREEN, _ease);
			_screenInTransition.animate("y", TopNav.HEIGHT);
			_screenInTransition.onComplete = fullPageClean;
			_screenInTransition.onCompleteArgs = [_screenInTransition];
			Starling.juggler.add(_screenInTransition);
			
		}
		
		private function fullPageClean(t:Tween):void
		{
			Starling.juggler.remove(t);
			t = null;	
			
			_isInTransition = false;
			
			_contextView.removeChild(_fullPageAd);
		}
		
		/***************************************************************************
		 * 
		 *   Screen Transition
		 * 
		 ***************************************************************************/
		private function switchScreens():void
		{
			_isInTransition = true;

			if(_currentScreen != null)
			{
				_screenOutTransition = new Tween(_currentScreen, DURATION_SCREEN, _ease);
				if(_isGoingHome){
					_screenOutTransition.animate("y", HEIGHT);
				}else{
					_screenOutTransition.animate("y", -HEIGHT);
				}
//				_screenOutTransition.delay = _menuDuration;
				_screenOutTransition.onComplete = screenTransitionClean;
				_screenOutTransition.onCompleteArgs = [_screenOutTransition];
					
				Starling.juggler.add(_screenOutTransition);
			}

			_screenInTransition = new Tween(_nextScreen, DURATION_SCREEN, _ease);
			_screenInTransition.animate("y", TopNav.HEIGHT);
//			_screenInTransition.delay = _menuDuration;
			
			if(_logout){
				_screenInTransition.onComplete = transitionCleanAndLogout
			}else{
				_screenInTransition.onComplete = transitionClean;
			}
			_screenInTransition.onCompleteArgs = [_screenInTransition];
			Starling.juggler.add(_screenInTransition);
						
		}
		
		/*******************************************************************
		 * 
		 *   Transition onComplete
		 * 
		 ******************************************************************/
		private function transitionCleanAndLogout(t:Tween):void
		{
			Starling.juggler.remove(t);
			t = null;
			
			_logout = false;
			logoutTransition();
			
			_isInTransition = false;
			
		}
		
		private function transitionClean(t:Tween):void
		{
			Starling.juggler.remove(t);
			t = null;	
			
			_isInTransition = false;
		}
		
		private function transitionAndSpriteClean(t:Tween):void
		{
			Starling.juggler.remove(t);
			_contextView.removeChild(t.target as FeathersControl, true);
			t = null;
			
			_isInTransition = false;
			
		}
		
		private function screenTransitionClean(t:Tween):void
		{
			Starling.juggler.remove(t);			
			_contextView.removeChild(t.target as FeathersControl, true);
			
			t = null;
			
			_currentScreen = _nextScreen;
			_nextScreen = null;
			
			_isInTransition = false;
			
		}
		
		/***************************************************************************
		 * 
		 *   Show/Hide Top Menu Transition
		 * 
		 ***************************************************************************/
		private function hideTopMenu():void
		{
			_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
			_topMenuTransition.animate("y", -50);
			_topMenuTransition.onComplete = topNavTransitionClean;
			Starling.juggler.add(_topMenuTransition);
		}
		
		private function showTopMenu():void
		{
			_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
			_topMenuTransition.animate("y", 0);
			_topMenuTransition.onComplete = topNavTransitionClean;
			Starling.juggler.add(_topMenuTransition);
		}
		
		/***************************************************************************
		 * 
		 *   Close Menu Transition
		 * 
		 ***************************************************************************/
		private function closeMenu(e:Event = null):void
		{
			_disabler.x = WIDTH;
			_disabler.removeEventListener(Event.TRIGGERED, closeMenu);
			
			_isInTransition = true;
			_topNav.isOpen = false;
					
			_sideMenuTransition = new Tween(_sideNav, DURATION_MENU, _ease);
			_sideMenuTransition.fadeTo(0);
			_sideMenuTransition.moveTo(-SideNav.WIDTH/4, 0);
			Starling.juggler.add(_sideMenuTransition);

			_screenInTransition = new Tween(_currentScreen, DURATION_MENU, _ease);
			_screenInTransition.animate("x", 0);
			_screenInTransition.onComplete = transitionClean;
			_screenInTransition.onCompleteArgs = [_screenInTransition];
			Starling.juggler.add(_screenInTransition);
			
			_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
			_topMenuTransition.animate("x", 0);
			_topMenuTransition.onComplete = topNavTransitionClean;
			Starling.juggler.add(_topMenuTransition);
		}
		
		private function closeMenuDispatch(e:Event):void
		{
			trace("dispatch");
			this.navListener(new TricoEvent(TricoEvent.HIDE_MENU));
		}
		
		/***************************************************************************
		 * 
		 *   Open Menu Transition
		 * 
		 ***************************************************************************/
		private function openMenu(e:Event):void
		{
			if(!_isInTransition){
				
				if(_topNav.isOpen){
					
					closeMenu();
					
				}else{
					
					_disabler.addEventListener(Event.TRIGGERED, closeMenu);
					_disabler.x = SideNav.WIDTH;
					
					_contextView.setChildIndex(_disabler, _contextView.numChildren);
					
					_isInTransition = true;
					_topNav.isOpen = true;
						
					_sideNav = new SideNav(_currentEvent);
					_sideNav.alpha = 0;
					_sideNav.width = SideNav.WIDTH;
					_sideNav.height = HEIGHT;
					_sideNav.x = -SideNav.WIDTH/4;
					
					navScreenListeners(true);
					
					_contextView.addChildAt(_sideNav, 0);

					_sideMenuTransition = new Tween(_sideNav, DURATION_MENU, _ease);
					_sideMenuTransition.fadeTo(1);
					_sideMenuTransition.moveTo(0, 0);
					Starling.juggler.add(_sideMenuTransition);
					
					_screenOutTransition = new Tween(_currentScreen, DURATION_MENU, _ease);
					_screenOutTransition.animate("x", SideNav.WIDTH);
					_screenOutTransition.onComplete = transitionClean;
					_screenOutTransition.onCompleteArgs = [_screenOutTransition];
					Starling.juggler.add(_screenOutTransition);
					
					_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
					_topMenuTransition.animate("x", SideNav.WIDTH);
					_topMenuTransition.onComplete = topNavTransitionClean;
					Starling.juggler.add(_topMenuTransition);
					
				}
				
			}
			
		}
		
		private function logoutListener():void
		{
			_loginService.expireToken();
			
			_logout = true;
			
			closeMenu();
		}
		
		private function topNavTransitionClean():void
		{
//			trace("topNavTransitionClean()");
			_topMenuTransition = null;
			
			if(!_topNav.isOpen){
				
				if(_sideNav){
					
					navScreenListeners(false);
					
					_contextView.removeChild(_sideNav, true);
					_sideNav = null;
				}
				
				if(_nextScreen != null){
					_contextView.addChildAt(_nextScreen as FeathersControl, 1);
					switchScreens();
				}else{
					checkLogout(); 
				}
				
			}else{
				checkLogout();
			}
			
		}
		
		private function checkLogout():void
		{
			if(_logout){
				_logout = false;
				logoutTransition();
			}else{
				_isInTransition = false;
			}
		}
		
		/***************************************************************************
		 * 
		 *   Logout Transition
		 * 
		 ***************************************************************************/
		private function logoutTransition():void
		{
			_isInTransition = true;
			
			_welcome = new WelcomeView();
			_welcome.addEventListener(Event.OPEN, signinListener);
			_welcome.height = HEIGHT;
			_welcome.width = WIDTH;
			_welcome.y = HEIGHT;
			_contextView.addChildAt(_welcome, 1);
						
			_topMenuTransition = new Tween(_topNav, DURATION_MENU, _ease);
			_topMenuTransition.animate("y", -TopNav.HEIGHT);
//			_topMenuTransition.delay = 1;
			_topMenuTransition.onComplete = transitionAndSpriteClean;
			_topMenuTransition.onCompleteArgs = [_topMenuTransition];
			Starling.juggler.add(_topMenuTransition);
			
			_screenOutTransition = new Tween(_currentScreen, DURATION_SCREEN, _ease);
			_screenOutTransition.animate("y", -HEIGHT);
//			_screenOutTransition.delay = 1;
			_screenOutTransition.onComplete = screenTransitionClean;
			_screenOutTransition.onCompleteArgs = [_screenOutTransition];
			
			Starling.juggler.add(_screenOutTransition)
			
			_screenInTransition = new Tween(_welcome, DURATION_SCREEN, _ease);
			_screenInTransition.animate("y", 0);
//			_screenInTransition.animate("scaleY", 1);
//			_screenInTransition.animate("skewX", 0);
//			_screenInTransition.animate("skewY", 0);
//			_screenInTransition.delay = DURATION_SCREEN;
			
//			_activeTransition.delay = 0.5;
			_screenInTransition.onComplete = transitionClean;
			_screenInTransition.onCompleteArgs = [_screenInTransition];
			
			Starling.juggler.add(_screenInTransition);
			
		}
		
		/***************************************************************************
		 * 
		 *   Signin Transition
		 * 
		 ***************************************************************************/
		private function signinListener(event:Event):void
		{
			_welcome.removeEventListener(Event.OPEN, signinListener);
			
			_isInTransition = false;
			
			_topNav = new TopNav();
			_topNav.y =  -TopNav.HEIGHT;
			_topNav.width = WIDTH;
			_topNav.addEventListener(TricoEvent.MENU, openMenu);
			_topNav.addEventListener(TricoEvent.HOME, navListener);
			_topNav.addEventListener(TricoEvent.SERVICE, navListener);
			
			_contextView.addChildAt(_topNav, 2);			
			
			_houseBuild = new HouseBuildSprite();
			_houseBuild.x = WIDTH/2;
			_houseBuild.y = HEIGHT/2;
			_contextView.addChildAt(_houseBuild, 0);
			_houseBuild.load(true);
			
			_homeScreen = new HomeView();
			_homeScreen.isStartUp = true;
			_homeScreen.height = HEIGHT - TopNav.HEIGHT;
			_homeScreen.width = WIDTH;
			_homeScreen.y = -HEIGHT;
			_contextView.addChildAt(_homeScreen, 0);
			
			homeScreenListeners(true);
			
			
			/** Setup full page ad **/
			_fullPageReady = false;
			_fullPageAd = new AdFullPageView(WIDTH, HEIGHT);
			_fullPageAd.addEventListener(TricoEvent.FULLPAGE, fullPageReady);
			_contextView.addChildAt(_fullPageAd, 0);
			/******************/
			
			
			/** CURRENT SCREEN INIT */
			_currentScreen = _homeScreen;
			_nextScreen = null;
			
			
			_screenOutTransition = new Tween(_welcome, DURATION_SCREEN, _ease);
			_screenOutTransition.animate("y", HEIGHT);
//			_screenOutTransition.animate("scaleY", 0.01);
//			_screenOutTransition.animate("skewX", 3.1);
//			_screenOutTransition.animate("skewY", 0.01);
			
			
//			_activeTransition.delay = 0.5;
			_screenOutTransition.onComplete = transitionAndSpriteClean;
			_screenOutTransition.onCompleteArgs = [_screenOutTransition];
			
			
			Starling.juggler.add(_screenOutTransition);
									
		}
	}
}