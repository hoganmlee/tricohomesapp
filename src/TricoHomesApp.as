package
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.InvokeEvent;
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import org.gestouch.core.Gestouch;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.extensions.starling.StarlingTouchHitTester;
	import org.gestouch.input.NativeInputAdapter;
	import org.robotlegs.mvcs.StarlingContext;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	
	[SWF(backgroundColor="#000000")]
	public class TricoHomesApp extends Sprite
	{
		public static const FRAME_RATE:int = 60;
		
		
		public static const BASE_URL:String = "http://trico.sajakfarki.com/";
		
		
		public static const HEIGHT:int = 752;
		public static const WIDTH:int = 1280;
		
//		public static const DEBUG:Boolean = true;
		public static const DEBUG:Boolean = false;
		
		private var _starling:Starling;
		private var _starlingContext:StarlingContext;
		private var _starlingTouchHitTester:StarlingTouchHitTester;
		
		public function TricoHomesApp()
		{
			super();
			
			if(this.stage)
			{
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
			}
			
			this.mouseEnabled = this.mouseChildren = false;
			this.loaderInfo.addEventListener(flash.events.Event.COMPLETE, loaderInfo_completeHandler);
		}
		
		private function loaderInfo_completeHandler(event:flash.events.Event):void
		{
			// Init stage.
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.frameRate = FRAME_RATE;
			
			Gestouch.addDisplayListAdapter(starling.display.DisplayObject, new StarlingDisplayListAdapter());
						
			// Init Starling.
			Starling.handleLostContext = true;
			Starling.multitouchEnabled = true;
			
			
			_starling = new Starling(Main, stage);
			this._starling.enableErrorChecking = false;
//			this._starling.showStats = true;
			_starling.start();
			
			Gestouch.inputAdapter = new NativeInputAdapter(stage);
			_starlingTouchHitTester = new StarlingTouchHitTester(this._starling);
			Gestouch.addTouchHitTester(_starlingTouchHitTester, -1);
			
			// Update Starling view port on stage resizes.
			stage.addEventListener( Event.RESIZE, onStageResize, false, int.MAX_VALUE, true );
			stage.addEventListener( Event.DEACTIVATE, onStageDeactivate, false, 0, true );
			
			
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);
			
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, addInvokeListener);
						
		}
		
		protected function addInvokeListener(event:InvokeEvent):void
		{
			trace(">>>>>>>>>>>> InvokeEvent :: " + event.reason);
			trace(">>>>>>>>>>>> InvokeEvent :: arguments = " + event.arguments);
		}
		
		protected function onKeyDown(ev:KeyboardEvent):void
		{
			switch(ev.keyCode)
			{
				case Keyboard.BACK: 
					// user hit the back button on Android device
					// case 94: // was hard-coded for older build of SDK supporting eclair
					// don't let the OS kill focus to our app
					ev.preventDefault();
					ev.stopImmediatePropagation();
					break;
				}
		}
		
		private function onStageResize( event:Event ):void 
		{
			if( stage.stageWidth < 256 || stage.stageHeight < 256 ) return;
			
			_starling.stage.stageWidth = stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
			
			const viewPort:Rectangle = _starling.viewPort;
			viewPort.width = stage.stageWidth;
			viewPort.height = stage.stageHeight;
			
			try {
				_starling.viewPort = viewPort;
			}
			catch( error:Error ) {
				//ERRRRRORRRRR!
			}
		}
		
		private function onStageDeactivate( event:Event ):void {
			_starling.stop();
			stage.addEventListener( Event.ACTIVATE, onStageActivate, false, 0, true );
		}
		
		private function onStageActivate( event:Event ):void {
			stage.removeEventListener( Event.ACTIVATE, onStageActivate );
			_starling.start();
		}
		
	}
	
}