# [Trico Homes Mobile App](http://www.tricohomes.com/)

## Quick start

Use SourceTree

## Features

* Cross platform (iOS/Android)

### Major components:

* [Starling](https://github.com/PrimaryFeather/Starling-Framework)
* [Feathers](https://github.com/joshtynjala/feathers)
* [Robot Legs](https://github.com/s9tpepper/robotlegs-starling-plugin)